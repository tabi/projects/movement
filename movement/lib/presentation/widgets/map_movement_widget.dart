import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';
import 'package:movement/infrastructure/repositories/dtos/cluster_dto.dart';
import 'package:movement/infrastructure/repositories/dtos/movement_dto.dart';
import 'package:movement/providers.dart';

import '../../infrastructure/notifiers/responsive_ui.dart';
import '../../infrastructure/repositories/database/database.dart';
import '../../infrastructure/repositories/dtos/enums/transport_enum.dart';
import '../theme/icon_mapper.dart';

class MapMovementWidget extends ConsumerStatefulWidget {
  const MapMovementWidget();

  @override
  ConsumerState<MapMovementWidget> createState() => _MapMovementWidgetState();
}

class _MapMovementWidgetState extends ConsumerState<MapMovementWidget> {
  var pressed = false;
  var markers = <Marker>[];
  var polylines = <Polyline>[];
  Timer? timer = null;

  final mapController = MapController();
  final streamSensorGeolocationController = StreamController<List<SensorGeolocation>>();
  final streamClusterController = StreamController<List<ClusterDto>>();
  final streamMovementDtoController = StreamController<List<MovementDto>>();

  StreamSubscription<List<SensorGeolocation>>? sensorGeolocationSubscription;
  StreamSubscription<List<ClusterDto>>? clusterSubscription;
  StreamSubscription<List<MovementDto>>? movementSubscription;
  var isDisposed = false;

  @override
  void dispose() {
    isDisposed = true;
    mapController.dispose();

    sensorGeolocationSubscription?.cancel();
    clusterSubscription?.cancel();
    movementSubscription?.cancel();

    streamSensorGeolocationController.close();
    streamClusterController.close();
    streamMovementDtoController.close();

    timer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) {
        final dayOverviewNotifier = ref.watch(dayOverviewNotifierProvider);

        streamSensorGeolocationController.addStream(dayOverviewNotifier.streamSensorGeolocations());
        streamClusterController.addStream(dayOverviewNotifier.streamClusters());
        streamMovementDtoController.addStream(dayOverviewNotifier.streamMovements());

        sensorGeolocationSubscription = streamSensorGeolocationController.stream.listen(updateSensorGeolocationsPolylines);
        clusterSubscription = streamClusterController.stream.listen(updateClustersPolylines);
        movementSubscription = streamMovementDtoController.stream.listen(updateMovementsPolylines);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200 * y,
      width: MediaQuery.of(context).size.width,
      child: FlutterMap(
        mapController: mapController,
        children: [
          TileLayer(urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'),
          MarkerLayer(markers: markers),
          PolylineLayer(
            polylines: polylines,
          ),
        ],
        options: MapOptions(
          initialCenter: LatLng(52.0660149, 4.3987896),
          initialZoom: 12,
          onPointerDown: (event, point) {
            pressed = true;
            timer?.cancel();
          },
          onPointerUp: (event, point) {
            timer = Timer(Duration(seconds: 60), () {
              pressed = false;
            });
          },
        ),
      ),
    );
  }

  void updateSensorGeolocationsPolylines(List<SensorGeolocation> sensorGeoLocations) {
    var polyline = Polyline(
      strokeWidth: 5 * f,
      color: const Color(0xFF00589C),
      borderStrokeWidth: 2 * f,
      borderColor: const Color(0xFF073C6A),
      points: smoothing(sensorGeoLocations.map((e) => LatLng(e.latitude, e.longitude)).toList()),
    );

    if (!pressed && sensorGeoLocations.isNotEmpty) {
      mapController.move(LatLng(sensorGeoLocations.last.latitude, sensorGeoLocations.last.longitude), 12);

      setState(() {
        polylines = [polyline];
      });
    }
  }

  void updateClustersPolylines(List<ClusterDto> clusters) {
    for (var cluster in clusters) {
      var polyline = Polyline(
        strokeWidth: 5 * f,
        color: getMovementColor(cluster.transport),
        borderStrokeWidth: 2 * f,
        isDotted: true,
        borderColor: getMovementColor(cluster.transport),
        points: smoothing(cluster.sensorGeolocations.map((e) => LatLng(e.latitude, e.longitude)).toList()),
      );

      if (!pressed && cluster.sensorGeolocations.isNotEmpty) {
        mapController.move(LatLng(cluster.sensorGeolocations.last.latitude, cluster.sensorGeolocations.last.longitude), 12);

        setState(() {
          polylines = [polyline];
        });
      }
    }
  }

  void updateMovementsPolylines(List<MovementDto> movements) {
    for (var movement in movements) {
      var polyline = Polyline(
        strokeWidth: 5 * f,
        color: getMovementColor(movement.transport),
        borderStrokeWidth: 2 * f,
        borderColor: getMovementColor(movement.transport),
        points: smoothing(movement.sensorGeolocations.map((e) => LatLng(e.latitude, e.longitude)).toList()),
      );

      if (!pressed && movement.sensorGeolocations.isNotEmpty) {
        mapController.move(LatLng(movement.sensorGeolocations.last.latitude, movement.sensorGeolocations.last.longitude), 12);

        setState(() {
          polylines = [polyline];
        });
      }
    }
  }

  void updateMarkers() {
    setState(() {});
  }

  Marker _getMarker(LatLng latLng, String? icon) {
    return Marker(
      width: 40 * x,
      height: 40 * y,
      point: latLng,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: Colors.white,
        ),
        alignment: Alignment.center,
        child: FaIconMapper.getFaIcon(icon),
      ),
    );
  }

  double medianOf3(List<double> l) {
    l.sort();
    return l[1];
  }

  List<LatLng> saveEqualize(List<LatLng> unsmooth) {
    try {
      return Path.from(unsmooth).equalize(30, smoothPath: true).coordinates;
    } catch (e) {
      return unsmooth;
    }
  }

  List<LatLng> smoothing(List<LatLng> points) {
    var smoothPoints = <LatLng>[];
    final medianFilteredPoints = <LatLng>[];

    if (points.length > 7) {
      for (int i = 0; i < points.length; i++) {
        if (points.asMap().containsKey(i - 1) && points.asMap().containsKey(i + 1)) {
          final medianLatitude = medianOf3([points[i - 1].latitude, points[i].latitude, points[i + 1].latitude]);
          final medianLongitude = medianOf3([points[i - 1].longitude, points[i].longitude, points[i + 1].longitude]);
          medianFilteredPoints.add(LatLng(medianLatitude, medianLongitude));
        }
      }

      smoothPoints = saveEqualize(medianFilteredPoints);
    } else {
      smoothPoints = saveEqualize(points);
    }

    return smoothPoints;
  }

  Color getMovementColor(Transport? transport) {
    switch (transport) {
      case Transport.Walking:
        return Colors.green;
      case Transport.Bicycle:
        return Colors.grey;
      case Transport.BicycleElectric:
        return Colors.grey;
      case Transport.Bus:
        return Colors.blueAccent;
      case Transport.Tram:
        return Colors.orange;
      case Transport.Subway:
        return Colors.purple;
      case Transport.Train:
        return Colors.yellow;
      case Transport.Car:
        return Colors.blue;
      case Transport.CarPassenger:
        return Colors.blue;
      case Transport.Scooter:
        return Colors.red;
      case Transport.Motor:
        return Colors.red;
      case Transport.PickupTruck:
        return Colors.red;
      case Transport.Onderway:
        return Colors.black;
      case Transport.Unknown:
        return Colors.black;
      case null:
        return Colors.black;
    }
  }
}
