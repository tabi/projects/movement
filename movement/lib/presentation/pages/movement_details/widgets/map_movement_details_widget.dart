import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';
import 'package:movement/providers.dart';

import '../../../../infrastructure/notifiers/responsive_ui.dart';
import '../../../../infrastructure/repositories/database/database.dart';
import '../../../../infrastructure/repositories/dtos/movement_dto.dart';
import '../../../theme/icon_mapper.dart';

class MapMovementDetailsWidget extends ConsumerStatefulWidget {
  final MovementDto _movementDto;

  const MapMovementDetailsWidget(this._movementDto);

  @override
  ConsumerState<MapMovementDetailsWidget> createState() => _MapMovementWidgetState();
}

class _MapMovementWidgetState extends ConsumerState<MapMovementDetailsWidget> {
  var pressed = false;
  var markers = <Marker>[];
  var polylines = <Polyline>[];

  final mapController = MapController();
  var isDisposed = false;

  @override
  void dispose() {
    isDisposed = true;
    mapController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) {
        updatePolylines(widget._movementDto.sensorGeolocations);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200 * y,
      width: MediaQuery.of(context).size.width,
      child: FlutterMap(
        mapController: mapController,
        children: [
          TileLayer(urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'),
          MarkerLayer(markers: markers),
          PolylineLayer(
            polylines: polylines,
          ),
        ],
        options: MapOptions(
          initialCenter: LatLng(52.0660149, 4.3987896),
          initialZoom: 12,
        ),
      ),
    );
  }

  void updatePolylines(List<SensorGeolocation> sensorGeoLocations) {
    var polyline = Polyline(
      strokeWidth: 5 * f,
      color: const Color(0xFF00589C),
      borderStrokeWidth: 2 * f,
      borderColor: const Color(0xFF073C6A),
      points: smoothing(sensorGeoLocations.map((e) => LatLng(e.latitude, e.longitude)).toList()),
    );

    if (!pressed && sensorGeoLocations.isNotEmpty) {
      mapController.move(LatLng(sensorGeoLocations.last.latitude, sensorGeoLocations.last.longitude), 12);

      setState(() {
        polylines = [polyline];
      });
    }
  }

  void updateMarkers() {
    setState(() {});
  }

  Marker _getMarker(LatLng latLng, String? icon) {
    return Marker(
      width: 40 * x,
      height: 40 * y,
      point: latLng,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: Colors.white,
        ),
        alignment: Alignment.center,
        child: FaIconMapper.getFaIcon(icon),
      ),
    );
  }

  double medianOf3(List<double> l) {
    l.sort();
    return l[1];
  }

  List<LatLng> saveEqualize(List<LatLng> unsmooth) {
    try {
      return Path.from(unsmooth).equalize(30, smoothPath: true).coordinates;
    } catch (e) {
      return unsmooth;
    }
  }

  List<LatLng> smoothing(List<LatLng> points) {
    var smoothPoints = <LatLng>[];
    final medianFilteredPoints = <LatLng>[];

    if (points.length > 7) {
      for (int i = 0; i < points.length; i++) {
        if (points.asMap().containsKey(i - 1) && points.asMap().containsKey(i + 1)) {
          final medianLatitude = medianOf3([points[i - 1].latitude, points[i].latitude, points[i + 1].latitude]);
          final medianLongitude = medianOf3([points[i - 1].longitude, points[i].longitude, points[i + 1].longitude]);
          medianFilteredPoints.add(LatLng(medianLatitude, medianLongitude));
        }
      }

      smoothPoints = saveEqualize(medianFilteredPoints);
    } else {
      smoothPoints = saveEqualize(points);
    }

    return smoothPoints;
  }
}
