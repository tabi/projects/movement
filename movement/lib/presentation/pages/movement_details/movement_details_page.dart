import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movement/infrastructure/notifiers/responsive_ui.dart';
import 'package:movement/presentation/pages/movement_details/widgets/map_movement_details_widget.dart';

import '../../../app_fonts.dart';
import '../../../infrastructure/notifiers/movement_notifier.dart';
import '../../../infrastructure/repositories/dtos/enums/log_type.dart';
import '../../../infrastructure/repositories/dtos/movement_dto.dart';
import '../../../infrastructure/repositories/log_repository.dart';
import '../../../infrastructure/services/localization_service.dart';
import '../../../providers.dart';
import '../../widgets/time_display_widget.dart';
import 'widgets/confirm_button_widget.dart';
import 'widgets/delete_button_widget.dart';
import 'widgets/vehicle_form.dart';

class MovementDetailsPage extends ConsumerWidget {
  final MovementDto _movementDto;

  const MovementDetailsPage(this._movementDto);

  void closeAndCancel(BuildContext context, MovementNotifier movementNotifier) {
    //Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    log('MovementDetailsPage::build', '', LogType.Flow);
    final movementNotifier = ref.watch(movementNotifierProvider(_movementDto));
    final movementDto = movementNotifier.movementDto;

    return PopScope(
      onPopInvoked: (bool popped) async {
        print(popped);
        closeAndCancel(context, movementNotifier);
      },
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            AppLocalizations.of(context).translate('movementdetailspage_title'),
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          actions: [ConfirmButtonWidget(movementDto)],
        ),
        body: Stack(
          children: [
            Column(
              children: [
                MapMovementDetailsWidget(_movementDto),
                SizedBox(height: 10 * y),
                TimeDisplayWidget(movementNotifier),
                SizedBox(height: 30 * y),
                Text(AppLocalizations.of(context).translate('movementdetailspage_whichmovement'), style: AppFonts.xxlBoldSohoPro, textAlign: TextAlign.center),
                VehicleForm(movementDto),
              ],
            ),
            Positioned(bottom: 30 * y, right: 30 * x, child: DeleteButton(movementDto)),
          ],
        ),
      ),
    );
  }
}
