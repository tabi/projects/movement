import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class IntroNotificationsNotifier extends ChangeNotifier {
  bool isGranted = false;
  bool isPressed = false;

  Future<void> configureLocalNotifcations() async {
    final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    isPressed = true;
    isGranted = true;
    notifyListeners();
  }

  static void onDidReceiveLocalNotification(int id, String? title, String? body, String? payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    print('hello');
  }

  void selectNotification(String? payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }
}
