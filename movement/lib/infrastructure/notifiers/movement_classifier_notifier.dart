import 'package:movement/infrastructure/services/movement_classifier/movement_classifier.dart';

class MovementClassifierNotifier {
  final MovementClassifier _movementClassifier;

  MovementClassifierNotifier(this._movementClassifier);
}
