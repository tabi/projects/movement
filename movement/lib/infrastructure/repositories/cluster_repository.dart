import 'package:movement/infrastructure/repositories/dtos/cluster_dto.dart';

import 'database/database.dart';
import 'network/cluster_api.dart';

class ClusterRepository {
  final Database _database;
  final ClusterApi _clusterApi;

  ClusterRepository(this._database, this._clusterApi);

  Future<void> syncClusters({int limit = 5000}) async {
    final unsycnedClusters = await _database.clusterDao.getUnsycnedClusters(limit);
    if (unsycnedClusters.isNotEmpty) {
      final _response = await _clusterApi.syncClusters(unsycnedClusters);
      if (_response.isOk) {
        final synced = unsycnedClusters.map((e) => e.copyWith(synced: true));
        _database.clusterDao.replaceBulkCluster(synced);
      }
    }
  }

  Stream<List<ClusterDto>> streamClusters(DateTime dateTime) async* {
    yield* _database.clusterDao.streamClusters(dateTime);
  }
}
