import 'package:json_annotation/json_annotation.dart';
import 'package:movement/infrastructure/repositories/dtos/enums/transport_enum.dart';

part 'probable_transport_dto.g.dart';

@JsonSerializable()
class ProbableTransportDTO {
  final Transport transport;
  final double probability;

  ProbableTransportDTO({
    required this.transport,
    required this.probability,
  });

  factory ProbableTransportDTO.fromMap(Map<String, dynamic> json) =>
      ProbableTransportDTO(transport: Transport.values[json["transport"]], probability: json["probability"] / 1.0);

  Map<String, dynamic> toMap() => _$ProbableTransportDTOToJson(this);
}
