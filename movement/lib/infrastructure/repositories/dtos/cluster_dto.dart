import 'package:movement/infrastructure/repositories/dtos/enums/transport_enum.dart';

import '../database/database.dart';

class ClusterDto {
  String uuid;
  DateTime startTime, endTime, createdOn;
  Transport? transport;
  int averageSpeed;
  double averageAccuracy;
  double? trainProbability, tramProbability, subwayProbability, walkingProbability, carProbability, bicycleProbability;
  bool synced;
  List<SensorGeolocation> sensorGeolocations;

  ClusterDto({
    required this.uuid,
    required this.startTime,
    required this.endTime,
    required this.createdOn,
    required this.transport,
    required this.averageSpeed,
    required this.averageAccuracy,
    required this.trainProbability,
    required this.tramProbability,
    required this.subwayProbability,
    required this.walkingProbability,
    required this.carProbability,
    required this.bicycleProbability,
    required this.synced,
    required this.sensorGeolocations,
  });
}
