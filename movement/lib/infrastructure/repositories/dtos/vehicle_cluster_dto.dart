import '../database/database.dart';

class VehicleClusterDto {
  int averageSpeed, amountOfTime, amountOfPoints, maxSpeed;
  List<SensorGeolocation> locations;

  VehicleClusterDto({required this.locations, required this.averageSpeed, required this.amountOfTime, required this.amountOfPoints, required this.maxSpeed});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['AverageSpeed'] = averageSpeed;
    data['AmountOfTime'] = amountOfTime;
    data['AmountOfPoints'] = amountOfPoints;
    data['MaxSpeed'] = maxSpeed;
    data['Locations'] = locations;

    return data;
  }

  factory VehicleClusterDto.fromMap(Map<String, dynamic> json) => VehicleClusterDto(
      averageSpeed: json["AverageSpeed"] / 1.0,
      amountOfTime: json["AmountOfTime"] / 1.0,
      amountOfPoints: json["AmountOfPoints"] / 1.0,
      maxSpeed: json["MaxSpeed"] / 1.0,
      locations: fromList(json["Locations"]));

  static List<SensorGeolocation> fromList(List<dynamic> list) {
    List<SensorGeolocation> mappedList = [];
    for (var item in list) {
      mappedList.add(SensorGeolocation.fromJson(item));
    }

    return mappedList;
  }
}
