import 'dart:ffi';

import 'package:drift/drift.dart';
import 'package:movement/infrastructure/repositories/database/tables/cluster_table.dart';
import 'package:movement/infrastructure/repositories/database/tables/sensor_geolocation_table.dart';
import 'package:movement/infrastructure/repositories/dtos/cluster_dto.dart';
import 'package:movement/presentation/widgets/date_picker_widget/util/date_extensions.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../dtos/enums/transport_enum.dart';
import '../database.dart';

part 'cluster_dao.g.dart';

@DriftAccessor(tables: [Clusters, SensorGeolocations])
class ClusterDao extends DatabaseAccessor<Database> with _$ClusterDaoMixin {
  ClusterDao(Database db) : super(db);

  Future<int> insertCluster(Cluster cluster) async => into(clusters).insert(cluster);

  Future<List<Cluster>> getClustersWithNoMovement() async {
    return (select(clusters)
          ..where((c) => c.classifiedPeriodUuid.isNull())
          ..orderBy([
            (c) => OrderingTerm(expression: c.createdOn),
          ]))
        .get();
  }

  Future<List<Cluster>> getUnsycnedClusters(int limit) {
    return (select(clusters)
          ..where((m) => m.synced.equals(false))
          ..limit(limit))
        .get();
  }

  Future<void> replaceBulkCluster(Iterable<Cluster> clusterList) async {
    await batch((batch) {
      batch.replaceAll(clusters, clusterList);
    });
  }

  Stream<List<ClusterDto>> streamClusters(DateTime dateTime) async* {
    final _clustersStream = (select(clusters)
          ..where((c) => (c.createdOn.isBiggerOrEqual(Variable(dateTime.startOfDay())) & c.createdOn.isSmallerOrEqual(Variable(dateTime.endOfDay()))))
          ..where((c) => c.deletedOn.isNull())
          ..where((c) => c.classifiedPeriodUuid.isNull())
          ..orderBy([
            (c) => OrderingTerm(expression: c.createdOn),
          ]))
        .asyncMap((cluster) async {
          final sensorlocations = await (select(sensorGeolocations)..where((s) => s.classifiedPeriodUuid.equals(cluster.uuid))).get();

          return ClusterDto(
              uuid: cluster.uuid,
              startTime: cluster.startTime,
              endTime: cluster.endTime,
              createdOn: cluster.createdOn,
              transport: cluster.transport != null ? Transport.values[cluster.transport!] : null,
              averageSpeed: cluster.averageSpeed,
              averageAccuracy: cluster.averageAccuracy,
              trainProbability: cluster.trainProbability,
              tramProbability: cluster.tramProbability,
              subwayProbability: cluster.subwayProbability,
              walkingProbability: cluster.walkingProbability,
              carProbability: cluster.carProbability,
              bicycleProbability: cluster.bicycleProbability,
              synced: cluster.synced,
              sensorGeolocations: sensorlocations);
        })
        .watch()
        .debounce(const Duration(milliseconds: 100));

    yield* _clustersStream;
  }
}
