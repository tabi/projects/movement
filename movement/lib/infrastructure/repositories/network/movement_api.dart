import '../database/database.dart';
import '../dtos/parsed_response.dart';
import 'base_api.dart';

class MovementApi extends BaseApi {
  MovementApi(Database database) : super('movement/', database);

  Future<ParsedResponse<Movement?>> sync(Movement movement) async => this.getParsedResponse<Movement, Movement>(
        'upsert',
        Movement.fromJson,
        payload: movement,
      );

  Future<ParsedResponse<List<Movement>>> syncClusters(List<Movement> movements) async {
    final payload = movements.map((movement) => movement.toJson()).toList();
    return this.getParsedResponse<List<Movement>, Movement>(
      'bulkInsert',
      Movement.fromJson,
      payload: payload,
    );
  }
}
