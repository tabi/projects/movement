import '../database/database.dart';
import '../dtos/parsed_response.dart';
import 'base_api.dart';

class ClusterApi extends BaseApi {
  ClusterApi(Database database) : super('cluster/', database);

  Future<ParsedResponse<List<Cluster>>> syncClusters(List<Cluster> clusters) async {
    final payload = clusters.map((cluster) => cluster.toJson()).toList();
    return this.getParsedResponse<List<Cluster>, Cluster>(
      'bulkInsert',
      Cluster.fromJson,
      payload: payload,
    );
  }
}
