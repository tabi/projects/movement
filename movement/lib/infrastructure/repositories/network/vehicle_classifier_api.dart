import '../database/database.dart';
import '../dtos/parsed_response.dart';
import '../dtos/probable_transport_dto.dart';
import '../dtos/vehicle_cluster_dto.dart';
import 'base_api.dart';

class VehicleClassifierApi extends BaseApi {
  VehicleClassifierApi(Database database) : super('vehicleClassifier/', database);

  Future<ParsedResponse<ProbableTransportDTO>> classifyWalking(VehicleClusterDto cluster) async =>
      this.getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyWalking', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyBicycle(VehicleClusterDto cluster) async =>
      this.getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyBicycle', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyCar(VehicleClusterDto cluster) async =>
      this.getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyCar', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyTram(VehicleClusterDto cluster) async =>
      this.getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyTram', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyTrain(VehicleClusterDto cluster) async =>
      this.getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyTrain', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifySubway(VehicleClusterDto cluster) async =>
      this.getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifySubway', ProbableTransportDTO.fromMap, payload: cluster);
}
