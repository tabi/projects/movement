import 'dart:math';
import 'dart:ui';

import 'package:drift/drift.dart';
import 'package:movement/infrastructure/repositories/dtos/enums/transport_enum.dart';
import 'package:movement/infrastructure/repositories/dtos/probable_transport_dto.dart';
import 'package:movement/infrastructure/repositories/network/vehicle_classifier_api.dart';
import 'package:uuid/uuid.dart';
import '../../repositories/database/database.dart';
import '../../repositories/dtos/vehicle_cluster_dto.dart';

class ClusterClassifier {
  final Database _database;
  final VehicleClassifierApi _vehicleClassifierApi;

  ClusterClassifier(this._database, this._vehicleClassifierApi) {}

  Future processGeolocation() async {
    //get last 5 points
    var sensorgeolocations = await _database.sensorGeolocationDao.getLastBackgroundSensorGeolocations(5);

    //check if atleast 4 points have been stored
    if (sensorgeolocations.length < 4) {
      return;
    }

    var medianSpeed = getMedian(sensorgeolocations);
    var isMoving = this.isMoving(medianSpeed);

    if (!isMoving) {
      //get all points that were moving
      var sensorgeolocations = await _database.sensorGeolocationDao.getGeolocationsWithNoCluster();
      var cluster = await distillCluster(sensorgeolocations);

      await _database.clusterDao.insertCluster(cluster);
    }
  }

  Future<List<Cluster>> createCalculatedSpeedCluster(List<SensorGeolocation> data) async {
    final clusters = List<Cluster>.empty(growable: true);
    var counter = 0;
    var moving = false;

    //data.removeWhere((element) => element.accuracy > 30);

    for (var i = 0; i < data.length - 5; i += 5) {
      var medianSpeed = getMedian(data.sublist(i, i + 5));
      for (var j = 0; j < 5; j++) {
        var isMoving = this.isMoving(medianSpeed);
        var stoppedMoving = this.stoppedMoving(isMoving, moving);
        var counterIsBigEnough = i + j - counter > 30;

        if (stoppedMoving) {
          var cluster = await distillCluster(data.sublist(counter, i + j));

          clusters.add(cluster);

          //just for fast testing
          // if (cluster.probableTransports.isNotEmpty) {
          //   return clusters;
          // }
        }

        if (startedMoving(isMoving, moving)) {
          counter = i + j;
        }

        moving = isMoving;
      }
    }

    return clusters;
  }

  bool stoppedMoving(bool isMoving, bool moving) {
    return !isMoving && moving;
  }

  bool startedMoving(bool isMoving, bool moving) {
    return isMoving && !moving;
  }

  bool isMoving(int calculatedSpeed) {
    return calculatedSpeed > 3;
  }

  int getMedian(List<SensorGeolocation> locations) {
    locations.sort((a, b) => a.calculatedSpeed.compareTo(b.calculatedSpeed));
    var median = locations[2].calculatedSpeed;

    return median.isNaN || median.isInfinite ? 0 : median.toInt();
  }

  Future distillCluster(List<SensorGeolocation> data) async {
    var aSpeed = data.map((m) => m.calculatedSpeed).reduce((a, b) => a + b) / data.length;
    var aAccuracy = data.map((m) => m.accuracy).reduce((a, b) => a + b) / data.length;
    var averageAccuracy = data.map((m) => m.accuracy).reduce((a, b) => a + b) / data.length;
    var averageSpeed = aSpeed.isInfinite ? 0 : aSpeed.toInt();
    var mSpeed = data.map((d) => d.calculatedSpeed).reduce(max);
    var maxSpeed = mSpeed.isInfinite ? 0 : mSpeed.toInt();
    var amountOfTime = (data.last.createdOn.millisecondsSinceEpoch - data.first.createdOn.millisecondsSinceEpoch) / 1000;

    var cluster = Cluster(
        uuid: Uuid().v4(),
        classifiedPeriodUuid: null,
        createdOn: DateTime.now(),
        startTime: data.first.createdOn,
        endTime: data.last.createdOn,
        averageAccuracy: averageAccuracy,
        averageSpeed: averageSpeed,
        deletedOn: null,
        transport: null,
        synced: false);

    if (cluster.averageSpeed > 9 || cluster.averageAccuracy > 400) {
      var list = await getVehicleClassificationFromServer(cluster, data);

      var car = list.firstWhere((element) => element.transport == Transport.Car).probability;
      var train = list.firstWhere((element) => element.transport == Transport.Train).probability;
      var tram = list.firstWhere((element) => element.transport == Transport.Tram).probability;
      //var bicycle = list.firstWhere((element) => element.transport == Transport.Bicycle).probability;
      var subway = list.firstWhere((element) => element.transport == Transport.Subway).probability;
      //var walking = list.firstWhere((element) => element.transport == Transport.Walking).probability;

      cluster = cluster.copyWith(
          carProbability: Value(car),
          trainProbability: Value(train),
          subwayProbability: Value(subway),
          bicycleProbability: Value(0),
          tramProbability: Value(tram),
          walkingProbability: Value(0));
    }

    return cluster;
  }

  Future<List<ProbableTransportDTO>> getVehicleClassificationFromServer(Cluster cluster, List<SensorGeolocation> locations) async {
    var list = List<ProbableTransportDTO>.from([], growable: true);
    var vehicleCluster = VehicleClusterDto(locations: locations, averageSpeed: cluster.averageSpeed, amountOfTime: 0, amountOfPoints: 0, maxSpeed: 0);

    await Future.wait([
      //_vehicleClassifierApi.classifyBicycle(vehicleCluster),
      _vehicleClassifierApi.classifyCar(vehicleCluster),
      _vehicleClassifierApi.classifySubway(vehicleCluster),
      _vehicleClassifierApi.classifyTram(vehicleCluster),
      _vehicleClassifierApi.classifyTrain(vehicleCluster),
      //_vehicleClassifierApi.classifyWalking(vehicleCluster),
    ]).then((value) => {list.addAll(value.map((e) => e.payload!))});

    return list;
  }

  //create movements from cluster
  double _calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * asin(sqrt(a)) * 1000;
  }

  int _calculateSpeed(DateTime startTime, DateTime endTime, double distance) {
    var timeDifferenceInSeconds = (endTime.millisecondsSinceEpoch - startTime.millisecondsSinceEpoch) * 1000;
    var speed = distance / timeDifferenceInSeconds;

    return speed.toInt();
  }
}
