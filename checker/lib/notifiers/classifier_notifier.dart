import 'dart:math';

import 'package:checker/repositories/database/database.dart';
import 'package:checker/repositories/network/dtos/cluster_dto.dart';
import 'package:checker/repositories/network/dtos/movement_dto.dart';
import 'package:checker/repositories/network/sensor_geolocation_api.dart';
import 'package:checker/services/movement_classifier/movement_classifier.dart';
import 'package:flutter/material.dart';

import '../repositories/network/cluster_api.dart';

class ClassifierNotifier extends ChangeNotifier {
  final MovementClassifier _movementClassifier;
  final SensorGeolocationApi _sensorGeolocationApi;
  final ClusterApi _clusterApi;
  final Database _database;

  List<ClusterDto> clustersDto = <ClusterDto>[];
  List<MovementDto> movementsDto = <MovementDto>[];
  List<SensorGeolocation> locations = <SensorGeolocation>[];

  ClassifierNotifier(this._movementClassifier, this._sensorGeolocationApi, this._clusterApi, this._database) {
    classifyLocations();
  }

  classifyLocations() async {
    var payload = await _sensorGeolocationApi.getLocations();
    locations = payload.payload!;
    notifyListeners();

    for (var location in locations) {
      await _movementClassifier.processLocation(location);
    }

    var clusterLocations = await _database.sensorGeolocationDao.getAllSensorGeolocations();
    var clusters = await _database.clusterDao.getClusters();

    await _sensorGeolocationApi.updateSensorLocations(clusterLocations);
    await _clusterApi.syncAnonymousClusters(clusters);
    // movements = await _database.movementDao.getMovements();
    notifyListeners();
  }

  calculateAndSaveLocations() async {
    var payload = await _sensorGeolocationApi.getLocations();
    var locations = payload.payload!;

    for (var i = 1; i < locations.length; i++) {
      var calculatedSpeed = 0.0;
      var lastSensorLocation = locations[i - 1];
      var distance = _calculateDistance(lastSensorLocation.latitude, lastSensorLocation.longitude, locations[i].latitude, locations[i].longitude);
      calculatedSpeed = _calculateSpeed(lastSensorLocation.createdOn, locations[i].createdOn, distance);

      locations[i] = locations[i].copyWith(calculatedSpeed: calculatedSpeed, distance: distance);
    }

    await _sensorGeolocationApi.updateSensorLocations(locations);
  }

  calculateAndSaveMedianLocations() async {
    var payload = await _sensorGeolocationApi.getLocations();
    var locations = payload.payload!;

    for (var i = 5; i < locations.length; i++) {
      var medianSpeed = getMedian(locations.sublist(i - 5, i));

      locations[i] = locations[i].copyWith(medianSpeed: medianSpeed);
    }

    await _sensorGeolocationApi.updateSensorLocations(locations);
  }

  double getMedian(List<SensorGeolocation> locations) {
    locations.sort((a, b) => a.calculatedSpeed.compareTo(b.calculatedSpeed));
    var median = locations[2].calculatedSpeed;

    return median.isNaN || median.isInfinite ? 0 : median;
  }

  double _calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * asin(sqrt(a)) * 1000;
  }

  double _calculateSpeed(DateTime startTime, DateTime endTime, double distance) {
    double timeDifferenceInSeconds = (endTime.millisecondsSinceEpoch - startTime.millisecondsSinceEpoch) / 1000;
    double speedInMetersPerSecond = distance / timeDifferenceInSeconds;
    double speedInKilometersPerHour = speedInMetersPerSecond.isNaN || speedInMetersPerSecond.isInfinite ? 0 : speedInMetersPerSecond * 3.6;

    return speedInKilometersPerHour;
  }
}
