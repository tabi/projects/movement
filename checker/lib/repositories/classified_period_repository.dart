import 'package:drift/drift.dart';
import 'package:latlong2/latlong.dart';

import 'database/database.dart';
import 'network/classified_period_api.dart';
import 'network/dtos/classified_period_dto.dart';
import 'network/dtos/movement_dto.dart';
import 'network/dtos/reference_latlng_dto.dart';
import 'network/dtos/stop_dto.dart';

class ClassifiedPeriodRepository {
  final Database _database;
  final ClassifiedPeriodApi _classifiedPeriodApi;

  ClassifiedPeriodRepository(this._database, this._classifiedPeriodApi);

  Future<void> syncClassifiedPeriods() async {
    final unsycnedClassifiedPeriods = await _database.classifiedPeriodDao.getUnsyncedClassifiedPeriods();
    for (final unsynced in unsycnedClassifiedPeriods) {
      final _response = await _classifiedPeriodApi.sync(unsynced);
      if (_response.isOk) await _database.classifiedPeriodDao.replaceClassifiedPeriod(unsynced.copyWith(synced: true));
    }
  }

  Future<List<SensorGeolocation>> getSensorlocationsForDetails(DateTime startTime, DateTime endTime) async {
    return await _database.sensorGeolocationDao.getSensorGeolocations(startTime, endTime);
  }

  Stream<List<ClassifiedPeriodDto?>> streamClassifiedPeriodDtos(DateTime dateTime) async* {
    yield* _database.classifiedPeriodDtoDao.streamClassifiedPeriodDtos(dateTime);
  }

  Stream<ClassifiedPeriodDto?> streamClassifiedPeriodDto(String uuid) async* {
    yield* _database.classifiedPeriodDtoDao.streamClassifiedPeriodDto(uuid);
  }

  Future<SensorGeolocation?> getLastValidSensorGeolocation() async => _database.sensorGeolocationDao.getLastValidSensorGeolocation();

  Future<bool> _isBadPeriod(ClassifiedPeriod classifiedPeriodDao) async {
    final stopDurationInSeconds = classifiedPeriodDao.endDate.difference(classifiedPeriodDao.startDate).inSeconds;
    if (stopDurationInSeconds > 150) return false;
    if (classifiedPeriodDao.confirmed == true) return false;
    if ((await _database.classifiedPeriodDao.getClassifiedPeriods()).length <= 1) return false;
    return true;
  }

  Future<void> _removeBadStop(ClassifiedPeriod classifiedPeriod) async {
    final StopDto stopDto = await _database.classifiedPeriodDtoDao.getClassifiedPeriodDto(classifiedPeriod) as StopDto;
    await _database.stopDao.removeStop(stopDto);
  }

  Future<void> _removeBadMovement(ClassifiedPeriod classifiedPeriod) async {
    final MovementDto movementDto = await _database.classifiedPeriodDtoDao.getClassifiedPeriodDto(classifiedPeriod) as MovementDto;
    await _database.movementDao.removeMovement(movementDto);
  }

  Future<void> upsertClassifiedPeriod(bool isStop, SensorGeolocation sensorGeolocation) async {
    final lastClassifiedPeriod = await _database.classifiedPeriodDao.getLastClassifiedPeriod();
    if (await _shouldAddNewClassifiedPeriod(lastClassifiedPeriod, isStop)) {
      if (isStop) {
        if (lastClassifiedPeriod != null && await _isBadPeriod(lastClassifiedPeriod)) {
          await (_removeBadMovement(lastClassifiedPeriod));
          final lastStop = await _database.classifiedPeriodDao.getLastClassifiedPeriod();
          if (lastStop != null) {
            await _database.classifiedPeriodDao.replaceClassifiedPeriod(lastStop.copyWith(endDate: sensorGeolocation.createdOn, synced: false));
          }
        } else {
          await _database.classifiedPeriodDao.addStop("", sensorGeolocation);
        }
      } else {
        // If movement
        if (lastClassifiedPeriod != null && await _isBadPeriod(lastClassifiedPeriod)) {
          await _removeBadStop(lastClassifiedPeriod);
          final lastMovement = await _database.classifiedPeriodDao.getLastClassifiedPeriod();
          if (lastMovement != null) {
            await _database.classifiedPeriodDao.replaceClassifiedPeriod(lastMovement.copyWith(endDate: sensorGeolocation.createdOn, synced: false));
          }
        } else {
          await _database.classifiedPeriodDao.addMovement("", sensorGeolocation);
        }
      }
      //await addGoogleMapsDataToStops();
    } else {
      await _database.classifiedPeriodDao.replaceClassifiedPeriod(lastClassifiedPeriod!.copyWith(endDate: sensorGeolocation.createdOn, synced: false));
    }
  }

  Future<bool> _shouldAddNewClassifiedPeriod(ClassifiedPeriod? lastClassifiedPeriod, bool newLocationIsStop) async {
    if (lastClassifiedPeriod == null) return true;
    final lastClassifiedPeriodIsStop = await _database.classifiedPeriodDao.isStop(lastClassifiedPeriod);
    return lastClassifiedPeriodIsStop != newLocationIsStop;
  }

  Future<List<ClassifiedPeriod>> getClassifiedPeriodsById(List<String> Ids) async {
    return await _database.classifiedPeriodDao.getClassifiedPeriodsByIds(Ids);
  }

  Future removeClassifiedPeriods(List<ClassifiedPeriod> periods) async {
    await _database.classifiedPeriodDao.removeClassifiedPeriods(periods);
  }

  Future<StopDto?> _getLastPeriodIfStopDto() async {
    final lastClassifiedPeriod = await _database.classifiedPeriodDao.getLastClassifiedPeriod();
    if (lastClassifiedPeriod == null) return null;
    final classifiedPeriodDto = await _database.classifiedPeriodDtoDao.getClassifiedPeriodDto(lastClassifiedPeriod);
    if (classifiedPeriodDto is StopDto) return classifiedPeriodDto;
    return null;
  }
}
