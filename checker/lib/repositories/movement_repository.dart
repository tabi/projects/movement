import 'package:drift/drift.dart';

import 'database/database.dart';
import 'network/dtos/movement_dto.dart';

class MovementRepository {
  final Database _database;

  MovementRepository(this._database);

  Future addDeepMovement(MovementDto movementDto) async {
    await _database.movementDao.addMovementWithClusterCount(movementDto, movementDto.clusters.length);

    //Update new clusters
    for (var i = 0; i < movementDto.clusters.length; i++) {
      movementDto.clusters[i] = movementDto.clusters[i].copyWith(classifiedPeriodUuid: Value(movementDto.classifiedPeriod.uuid));

      var clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(movementDto.clusters[i].uuid);

      for (var i = 0; i < clusterLocations.length; i++) {
        clusterLocations[i] = clusterLocations[i].copyWith(classifiedPeriodUuid: Value(movementDto.classifiedPeriod.uuid));
      }

      //Update new SensorGeolocations
      await _database.sensorGeolocationDao.replaceBulkSensorGeolocation(clusterLocations);
    }

    await _database.clusterDao.replaceBulkCluster(movementDto.clusters);
  }

  Future updateDeepMovement(MovementDto newMovementDto, MovementDto oldMovementDto) async {
    await _database.movementDao.removeMovement(oldMovementDto);
    await _database.movementDao.addMovementWithClusterCount(newMovementDto, newMovementDto.clusters.length + oldMovementDto.clusters.length);

    //Update new clusters
    for (var i = 0; i < newMovementDto.clusters.length; i++) {
      newMovementDto.clusters[i] = newMovementDto.clusters[i].copyWith(classifiedPeriodUuid: Value(newMovementDto.classifiedPeriod.uuid));

      var clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(newMovementDto.clusters[i].uuid);

      for (var i = 0; i < clusterLocations.length; i++) {
        clusterLocations[i] = clusterLocations[i].copyWith(classifiedPeriodUuid: Value(newMovementDto.classifiedPeriod.uuid));
      }

      //Update new SensorGeolocations
      await _database.sensorGeolocationDao.replaceBulkSensorGeolocation(clusterLocations);
    }
    await _database.clusterDao.replaceBulkCluster(newMovementDto.clusters);

    //Update old clusters
    for (var i = 0; i < oldMovementDto.clusters.length; i++) {
      oldMovementDto.clusters[i] = oldMovementDto.clusters[i].copyWith(classifiedPeriodUuid: Value(newMovementDto.classifiedPeriod.uuid));

      var clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(oldMovementDto.clusters[i].uuid);

      for (var i = 0; i < clusterLocations.length; i++) {
        clusterLocations[i] = clusterLocations[i].copyWith(classifiedPeriodUuid: Value(newMovementDto.classifiedPeriod.uuid));
      }

      //Update old SensorGeolocations
      await _database.sensorGeolocationDao.replaceBulkSensorGeolocation(clusterLocations);
    }
    await _database.clusterDao.replaceBulkCluster(oldMovementDto.clusters);
  }
}
