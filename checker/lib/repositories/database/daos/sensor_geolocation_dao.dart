import 'package:checker/services/date_extensions.dart';
import 'package:drift/drift.dart';
import 'package:latlong2/latlong.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../network/dtos/reference_latlng_dto.dart';
import '../../network/dtos/sensor_stats_dto.dart';
import '../database.dart';
import '../tables/classified_period_table.dart';
import '../tables/cluster_table.dart';
import '../tables/sensor_geolocation_table.dart';

part 'sensor_geolocation_dao.g.dart';

@DriftAccessor(tables: [SensorGeolocations, Clusters, ClassifiedPeriods])
class SensorGeolocationDao extends DatabaseAccessor<Database> with _$SensorGeolocationDaoMixin {
  SensorGeolocationDao(Database db) : super(db);

  Future<int> insertSensorGeolocation(SensorGeolocation sensorGeolocation) async => into(sensorGeolocations).insert(sensorGeolocation);

  Future<void> addSensorGeolocation(SensorGeolocationsCompanion sensorGeolocationsCompanion) async {
    return await transaction(
      () async {
        await into(sensorGeolocations).insert(sensorGeolocationsCompanion);
      },
    );
  }

  Future<void> addLocation(SensorGeolocation location) async {
    return await transaction(
      () async {
        await into(sensorGeolocations).insert(location);
      },
    );
  }

  Future<bool> replaceSensorGeolocation(SensorGeolocation sensorGeolocation) async => update(sensorGeolocations).replace(sensorGeolocation);

  Future<void> replaceBulkSensorGeolocation(Iterable<SensorGeolocation> sensorGeolocationList) async {
    await batch((batch) {
      batch.replaceAll(sensorGeolocations, sensorGeolocationList);
    });
  }

  Future<int> getSensorGeolocationCount() async => (await select(sensorGeolocations).get()).length;

  Future<ReferenceLatLngDto?> getReferenceLatLng(DateTime referenceDateMin, DateTime referenceDateMax) async {
    for (final minute in [2, 1, 0]) {
      final query = select(sensorGeolocations)
        ..where((s) => s.isNoise.equals(false))
        ..where((s) => s.deletedOn.isNull())
        ..where(
            (s) => s.createdOn.isBetween(Variable.withDateTime(referenceDateMin), Variable.withDateTime(referenceDateMax.subtract(Duration(minutes: minute)))))
        ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.desc)])
        ..limit(1);
      final sensorGeolocation = await query.getSingleOrNull();
      if (sensorGeolocation != null) return ReferenceLatLngDto(LatLng(sensorGeolocation.latitude, sensorGeolocation.longitude), sensorGeolocation.createdOn);
    }
    return null;
  }

  Future<SensorGeolocation?> getLastValidSensorGeolocation() async {
    return await (select(sensorGeolocations)
          ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.desc)])
          ..limit(1))
        .getSingleOrNull();
  }

  Future<List<SensorGeolocation>> getSensorGeolocations(DateTime startTime, DateTime endTime) async {
    return await (select(sensorGeolocations)
          ..where((m) => m.createdOn.isBetween(Variable.withDateTime(startTime), Variable.withDateTime(endTime)))
          ..where((m) => m.deletedOn.isNull())
          ..where((m) => m.isNoise.equals(false))
          ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.asc)]))
        .get();
  }

  Future<List<SensorGeolocation>> getSensorGeolocationsFromCluster(String clusterId) async {
    return await (select(sensorGeolocations)
          ..where((m) => m.deletedOn.isNull())
          ..where((m) => m.clusterId.equals(clusterId))
          ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.asc)]))
        .get();
  }

  Future<List<SensorGeolocation>> findSensorGeolocations(String sensorType) async {
    return await (select(sensorGeolocations)
          ..where((m) => m.sensoryType.equals(sensorType))
          ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.asc)]))
        .get();
  }

  Future<List<SensorGeolocation>> getAllSensorGeolocations() async {
    return await (select(sensorGeolocations)..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.asc)])).get();
  }

  Future<List<SensorGeolocation>> getUnsycnedSensorGeolocations(int limit) async {
    return await (select(sensorGeolocations)
          ..where((m) => m.synced.equals(false))
          ..limit(limit))
        .get();
  }

  Future<int> getSyncedCount() async => (await (select(sensorGeolocations)..where((m) => m.synced.equals(true))).get()).length;

  Future<int> getUnsyncedCount() async => (await (select(sensorGeolocations)..where((m) => m.synced.equals(false))).get()).length;

  Future<List<String>> _getSeenSensors() async {
    return (selectOnly(sensorGeolocations, distinct: true)..addColumns([sensorGeolocations.sensoryType])).map((row) {
      return row.rawData.data.values.first as String;
    }).get();
  }

  Future<List<SensorGeolocation>> _getGeolocationsPerSensor(String sensorName, Expression<Object> sortedBy) async {
    return (select(sensorGeolocations)
          ..where((s) => s.sensoryType.equals(sensorName))
          ..orderBy([(s) => OrderingTerm(expression: sortedBy, mode: OrderingMode.desc)]))
        .get();
  }

  Future<int> _getClusterCount() async {
    final countResult = await (select(clusters)).get();

    return countResult.length;
  }

  Future<int> _getMovementCount() async {
    final countResult = await (select(classifiedPeriods)).get();

    return countResult.length;
  }

  Future<List<SensorStatsDto>> getSensorStats() async {
    final sensorNames = await _getSeenSensors();
    final sensorStatDtos = <SensorStatsDto>[];
    for (final sensorName in sensorNames) {
      final geolocationsByTime = await _getGeolocationsPerSensor(sensorName, sensorGeolocations.createdOn);
      final geolocationsByAccuracy = await _getGeolocationsPerSensor(sensorName, sensorGeolocations.accuracy);
      final movementCount = await _getMovementCount();
      final clusterCount = await _getClusterCount();
      final lastSeen = geolocationsByTime[0].createdOn;
      final count = geolocationsByTime.length;
      sensorStatDtos.add(
        SensorStatsDto(
          name: sensorName,
          lastSeen: lastSeen,
          count: count,
          movementCount: movementCount,
          clusterCount: clusterCount,
          accuracy25Percentile: geolocationsByAccuracy[(count * 0.25).floor()].accuracy,
          accuracy50Percentile: geolocationsByAccuracy[(count * 0.50).floor()].accuracy,
          accuracy75Percentile: geolocationsByAccuracy[(count * 0.75).floor()].accuracy,
        ),
      );
    }
    return sensorStatDtos;
  }

  Future<SensorGeolocation?> getLastBackgroundSensorGeolocation() async {
    return (select(sensorGeolocations)
          ..where((s) => (s.sensoryType.equals('GeolocatorPlatform')).not())
          ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.desc)])
          ..limit(1))
        .getSingleOrNull();
  }

  Future<List<SensorGeolocation>> getLastBackgroundSensorGeolocations(int amount) async {
    return (select(sensorGeolocations)
          ..where((s) => (s.sensoryType.equals('GeolocatorPlatform')).not())
          ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.desc)])
          ..limit(amount))
        .get();
  }

  Future<List<SensorGeolocation>> getGeolocationsWithNoCluster() async {
    return (select(sensorGeolocations)
          ..where((s) => s.clusterId.isNull())
          ..orderBy([(s) => OrderingTerm(expression: s.createdOn, mode: OrderingMode.desc)]))
        .get();
  }

  Stream<List<SensorGeolocation>> streamSensorGeolocations(DateTime dateTime) async* {
    final _sensorGeolocationsStream = (select(sensorGeolocations)
          ..where((c) => (c.createdOn.isBiggerOrEqual(Variable(dateTime.startOfDay())) & c.createdOn.isSmallerOrEqual(Variable(dateTime.endOfDay()))))
          ..where((c) => c.deletedOn.isNull())
          ..orderBy([
            (c) => OrderingTerm(expression: c.createdOn),
          ]))
        .watch()
        .debounce(const Duration(milliseconds: 100));

    yield* _sensorGeolocationsStream;
  }
}
