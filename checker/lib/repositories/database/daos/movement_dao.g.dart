// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movement_dao.dart';

// ignore_for_file: type=lint
mixin _$MovementDaoMixin on DatabaseAccessor<Database> {
  $TrackedDaysTable get trackedDays => attachedDatabase.trackedDays;
  $ClassifiedPeriodsTable get classifiedPeriods =>
      attachedDatabase.classifiedPeriods;
  $VehiclesTable get vehicles => attachedDatabase.vehicles;
  $MovementsTable get movements => attachedDatabase.movements;
  $ReasonsTable get reasons => attachedDatabase.reasons;
  $GoogleMapsDatasTable get googleMapsDatas => attachedDatabase.googleMapsDatas;
  $StopsTable get stops => attachedDatabase.stops;
  $ManualGeolocationsTable get manualGeolocations =>
      attachedDatabase.manualGeolocations;
  $ClustersTable get clusters => attachedDatabase.clusters;
  $SensorGeolocationsTable get sensorGeolocations =>
      attachedDatabase.sensorGeolocations;
}
