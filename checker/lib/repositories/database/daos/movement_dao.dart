import 'package:checker/services/date_extensions.dart';
import 'package:drift/drift.dart';
import 'package:stream_transform/stream_transform.dart';
import 'package:uuid/uuid.dart';

import '../../network/dtos/enums/transport_enum.dart';
import '../../network/dtos/movement_dto.dart';
import '../database.dart';
import '../tables/classified_period_table.dart';
import '../tables/google_maps_table.dart';
import '../tables/manual_geolocation_table.dart';
import '../tables/movement_table.dart';
import '../tables/reason_table.dart';
import '../tables/sensor_geolocation_table.dart';
import '../tables/stop_table.dart';
import '../tables/vehicle_table.dart';

part 'movement_dao.g.dart';

@DriftAccessor(tables: [ClassifiedPeriods, Movements, Stops, Reasons, GoogleMapsDatas, ManualGeolocations, Vehicles, SensorGeolocations, ManualGeolocations])
class MovementDao extends DatabaseAccessor<Database> with _$MovementDaoMixin {
  MovementDao(super.db);

  Future<void> removeMovement(MovementDto movementDto) async => db.classifiedPeriodDao.removeClassifiedPeriods([movementDto.classifiedPeriod]);

  Future<void> addMovement(MovementDto movementDto) async {
    return await transaction(
      () async {
        final classifiedPeriodUuid = await db.classifiedPeriodDtoDao.addClassifiedPeriod(movementDto);
        await into(movements).insert(
          MovementsCompanion.insert(
              clusterCount: 0,
              classifiedPeriodUuid: classifiedPeriodUuid,
              vehicleId: movementDto.vehicle != null ? Value(movementDto.vehicle!.id) : Value.absent(),
              transport: Value(movementDto.transport?.index)),
        );
      },
    );
  }

  Future<void> addMovementWithClusterCount(MovementDto movementDto, int clusterCount) async {
    return await transaction(
      () async {
        final classifiedPeriodUuid = await db.classifiedPeriodDtoDao.addClassifiedPeriod(movementDto);
        await into(movements).insert(
          MovementsCompanion.insert(
              clusterCount: clusterCount,
              classifiedPeriodUuid: classifiedPeriodUuid,
              vehicleId: movementDto.vehicle != null ? Value(movementDto.vehicle!.id) : Value.absent(),
              transport: Value(movementDto.transport?.index)),
        );
      },
    );
  }

  Future updateMovement(MovementDto movementDto, MovementDto oldMovementDto) async {
    final _c = movementDto.classifiedPeriod;
    await removeMovement(oldMovementDto);
    await addMovement(movementDto.copyWith(classifiedPeriod: _c.copyWith(uuid: Uuid().v4(), origin: Value(_c.uuid)), transport: movementDto.transport));
  }

  Future updateMovementWithClusterCount(MovementDto movementDto, MovementDto oldMovementDto, int clusterCount) async {
    final _c = movementDto.classifiedPeriod;
    await removeMovement(oldMovementDto);
    await addMovement(movementDto.copyWith(
        classifiedPeriod: _c.copyWith(uuid: const Uuid().v4(), origin: Value(_c.uuid)), clusterCount: clusterCount, transport: movementDto.transport));
  }

  Future<List<MovementDto>> getMovements() async {
    final query = select(classifiedPeriods)..where((c) => c.deletedOn.isNull());

    var results = await query.asyncMap((row) async {
      final movement = await (select(movements)..where((m) => m.classifiedPeriodUuid.equals(row.uuid))).getSingle();
      var c = await (select(clusters)..where((m) => m.classifiedPeriodUuid.equals(row.uuid))).get();
      var locations = await (select(sensorGeolocations)..where((m) => m.classifiedPeriodUuid.equals(row.uuid))).get();

      return MovementDto(
          movementUuid: movement.uuid,
          clusterCount: c.length,
          vehicle: null,
          transport: movement.transport == null ? null : Transport.values[movement.transport!],
          classifiedPeriod: row,
          manualGeolocations: [],
          sensorGeolocations: locations,
          clusters: c);
    }).get();

    return results;
  }

  Future<List<Movement>> getUnsycnedMovements() async => (select(movements)..where((c) => c.synced.equals(false))).get();

  Future<void> setSynced(Movement movement) async => await update(movements).replace(movement.copyWith(synced: Value(true)));

  Stream<List<MovementDto>> streamMovements(DateTime dateTime) async* {
    final _movementsStream = (select(classifiedPeriods).join([
      leftOuterJoin(movements, movements.classifiedPeriodUuid.equalsExp(classifiedPeriods.uuid)),
      leftOuterJoin(vehicles, vehicles.id.equalsExp(movements.vehicleId)),
    ])
          ..where(classifiedPeriods.createdOn.isBiggerOrEqual(Variable(dateTime.startOfDay())))
          ..where(classifiedPeriods.createdOn.isSmallerOrEqual(Variable(dateTime.endOfDay()))))
        .asyncMap((row) async {
          final sensorlocations = await (select(sensorGeolocations)..where((s) => s.classifiedPeriodUuid.equals(row.readTable(movements).uuid))).get();
          final manuallocations = await (select(manualGeolocations)..where((s) => s.classifiedPeriodUuid.equals(row.readTable(movements).uuid))).get();

          return MovementDto(
              movementUuid: row.readTable(movements).uuid,
              clusterCount: row.readTable(movements).clusterCount,
              vehicle: row.readTableOrNull(vehicles),
              transport: row.readTable(movements).transport == null ? null : Transport.values[row.readTable(movements).transport!],
              classifiedPeriod: row.readTable(classifiedPeriods),
              manualGeolocations: manuallocations,
              sensorGeolocations: sensorlocations,
              clusters: []);
        })
        .watch()
        .debounce(const Duration(milliseconds: 100));

    yield* _movementsStream;
  }
}
