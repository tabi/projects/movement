// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'manual_geolocation_dao.dart';

// ignore_for_file: type=lint
mixin _$ManualGeolocationDaoMixin on DatabaseAccessor<Database> {
  $TrackedDaysTable get trackedDays => attachedDatabase.trackedDays;
  $ClassifiedPeriodsTable get classifiedPeriods =>
      attachedDatabase.classifiedPeriods;
  $ManualGeolocationsTable get manualGeolocations =>
      attachedDatabase.manualGeolocations;
}
