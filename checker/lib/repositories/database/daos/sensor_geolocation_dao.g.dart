// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensor_geolocation_dao.dart';

// ignore_for_file: type=lint
mixin _$SensorGeolocationDaoMixin on DatabaseAccessor<Database> {
  $TrackedDaysTable get trackedDays => attachedDatabase.trackedDays;
  $ClassifiedPeriodsTable get classifiedPeriods =>
      attachedDatabase.classifiedPeriods;
  $ClustersTable get clusters => attachedDatabase.clusters;
  $SensorGeolocationsTable get sensorGeolocations =>
      attachedDatabase.sensorGeolocations;
}
