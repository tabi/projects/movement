// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tracked_day_dao.dart';

// ignore_for_file: type=lint
mixin _$TrackedDayDaoMixin on DatabaseAccessor<Database> {
  $TrackedDaysTable get trackedDays => attachedDatabase.trackedDays;
  $ClassifiedPeriodsTable get classifiedPeriods =>
      attachedDatabase.classifiedPeriods;
}
