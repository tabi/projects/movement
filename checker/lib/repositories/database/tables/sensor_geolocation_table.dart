import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

import 'classified_period_table.dart';
import 'cluster_table.dart';

@DriftAccessor(tables: [Clusters, ClassifiedPeriods])
class SensorGeolocations extends Table {
  TextColumn get uuid => text().clientDefault(() => Uuid().v4())();
  RealColumn get latitude => real()();
  RealColumn get longitude => real()();
  RealColumn get altitude => real()();
  RealColumn get bearing => real()();
  RealColumn get accuracy => real()();
  RealColumn get speed => real()();
  RealColumn get calculatedSpeed => real()();
  RealColumn get medianSpeed => real()();
  RealColumn get distance => real()();
  TextColumn get sensoryType => text()();
  TextColumn? get clusterId => text().nullable().references(Clusters, #uuid).withDefault(const Constant(null))();
  TextColumn? get classifiedPeriodUuid => text().nullable().references(ClassifiedPeriods, #uuid).withDefault(const Constant(null))();
  TextColumn get userId => text().withDefault(const Constant(""))();
  TextColumn get provider => text()();
  BoolColumn get isNoise => boolean()();
  IntColumn get batteryLevel => integer()();
  DateTimeColumn get createdOn => dateTime()();
  DateTimeColumn? get deletedOn => dateTime().nullable()();
  BoolColumn get synced => boolean().nullable().withDefault(const Constant(false))();

  @override
  Set<Column> get primaryKey => {uuid};
}
