import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';
import 'classified_period_table.dart';

@DriftAccessor(tables: [ClassifiedPeriods])
class Clusters extends Table {
  TextColumn get uuid => text().clientDefault(() => Uuid().v4())();
  TextColumn? get classifiedPeriodUuid => text().nullable().references(ClassifiedPeriods, #uuid).withDefault(const Constant(null))();
  DateTimeColumn get startTime => dateTime()();
  DateTimeColumn get endTime => dateTime()();
  DateTimeColumn get createdOn => dateTime()();
  IntColumn get transport => integer().nullable()();
  IntColumn get averageSpeed => integer()();
  RealColumn get averageAccuracy => real()();
  DateTimeColumn? get deletedOn => dateTime().nullable()();
  RealColumn get trainProbability => real().nullable().withDefault(const Constant(null))();
  RealColumn get tramProbability => real().nullable().withDefault(const Constant(null))();
  RealColumn get subwayProbability => real().nullable().withDefault(const Constant(null))();
  RealColumn get walkingProbability => real().nullable().withDefault(const Constant(null))();
  RealColumn get carProbability => real().nullable().withDefault(const Constant(null))();
  RealColumn get bicycleProbability => real().nullable().withDefault(const Constant(null))();
  BoolColumn get synced => boolean().withDefault(const Constant(false))();
  TextColumn? get userUuid => text().nullable().withDefault(const Constant(null))();

  @override
  Set<Column> get primaryKey => {uuid};
}
