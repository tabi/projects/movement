import 'dart:io';
import 'dart:math';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path/path.dart' as p;
import 'package:uuid/uuid.dart';

import 'cache/classified_period_dto_cache.dart';
import 'daos/classified_period_dao.dart';
import 'daos/classified_period_dto_dao.dart';
import 'daos/cluster_dao.dart';
import 'daos/device_dao.dart';
import 'daos/google_maps_data_dao.dart';
import 'daos/log_dao.dart';
import 'daos/manual_geolocation_dao.dart';
import 'daos/movement_dao.dart';
import 'daos/reason_dao.dart';
import 'daos/sensor_geolocation_dao.dart';
import 'daos/stop_dao.dart';
import 'daos/token_dao.dart';
import 'daos/tracked_day_dao.dart';
import 'daos/vehicle_dao.dart';
import 'tables/classified_period_table.dart';
import 'tables/cluster_table.dart';
import 'tables/device_table.dart';
import 'tables/google_maps_table.dart';
import 'tables/log_table.dart';
import 'tables/manual_geolocation_table.dart';
import 'tables/movement_table.dart';
import 'tables/reason_table.dart';
import 'tables/sensor_geolocation_table.dart';
import 'tables/stop_table.dart';
import 'tables/token_table.dart';
import 'tables/tracked_day_table.dart';
import 'tables/vehicle_table.dart';

part 'database.g.dart';

@DriftDatabase(
  tables: [
    Devices,
    Logs,
    Tokens,
    ClassifiedPeriods,
    ManualGeolocations,
    Movements,
    Reasons,
    SensorGeolocations,
    Stops,
    Clusters,
    TrackedDays,
    Vehicles,
    GoogleMapsDatas,
  ],
  daos: [
    ReasonDao,
    VehicleDao,
    DevicesDao,
    LogsDao,
    TokensDao,
    SensorGeolocationDao,
    ManualGeolocationDao,
    ClassifiedPeriodDao,
    ClassifiedPeriodDtoDao,
    TrackedDayDao,
    MovementDao,
    StopDao,
    ClusterDao,
    GoogleMapsDataDao,
  ],
  include: {'tables/index.drift'},
)
class Database extends _$Database {
  final classifiedPeriodDtoCache = ClassifiedPeriodDtoCache();

  Database() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(
        beforeOpen: (details) async {
          await customStatement('PRAGMA foreign_keys = ON');
        },
      );
}

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final script = File(Platform.script.toFilePath());
    final random = Random().nextInt(600);
    final file = File(p.join(script.path.replaceFirst("main.dart", ""), '/generated_databases/${random}db.sqlite'));
    return NativeDatabase(file);
  });
}
