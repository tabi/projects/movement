// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// ignore_for_file: type=lint
class $TrackedDaysTable extends TrackedDays
    with TableInfo<$TrackedDaysTable, TrackedDay> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TrackedDaysTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _dateMeta = const VerificationMeta('date');
  @override
  late final GeneratedColumn<DateTime> date = GeneratedColumn<DateTime>(
      'date', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _confirmedMeta =
      const VerificationMeta('confirmed');
  @override
  late final GeneratedColumn<bool> confirmed = GeneratedColumn<bool>(
      'confirmed', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("confirmed" IN (0, 1))'),
      defaultValue: const Constant(false));
  static const VerificationMeta _choiceIdMeta =
      const VerificationMeta('choiceId');
  @override
  late final GeneratedColumn<int> choiceId = GeneratedColumn<int>(
      'choice_id', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _choiceTextMeta =
      const VerificationMeta('choiceText');
  @override
  late final GeneratedColumn<String> choiceText = GeneratedColumn<String>(
      'choice_text', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, date, confirmed, choiceId, choiceText, synced];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'tracked_days';
  @override
  VerificationContext validateIntegrity(Insertable<TrackedDay> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('date')) {
      context.handle(
          _dateMeta, date.isAcceptableOrUnknown(data['date']!, _dateMeta));
    } else if (isInserting) {
      context.missing(_dateMeta);
    }
    if (data.containsKey('confirmed')) {
      context.handle(_confirmedMeta,
          confirmed.isAcceptableOrUnknown(data['confirmed']!, _confirmedMeta));
    }
    if (data.containsKey('choice_id')) {
      context.handle(_choiceIdMeta,
          choiceId.isAcceptableOrUnknown(data['choice_id']!, _choiceIdMeta));
    }
    if (data.containsKey('choice_text')) {
      context.handle(
          _choiceTextMeta,
          choiceText.isAcceptableOrUnknown(
              data['choice_text']!, _choiceTextMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  TrackedDay map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TrackedDay(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      date: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}date'])!,
      confirmed: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}confirmed'])!,
      choiceId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}choice_id']),
      choiceText: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}choice_text']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced']),
    );
  }

  @override
  $TrackedDaysTable createAlias(String alias) {
    return $TrackedDaysTable(attachedDatabase, alias);
  }
}

class TrackedDay extends DataClass implements Insertable<TrackedDay> {
  final String uuid;
  final DateTime date;
  final bool confirmed;
  final int? choiceId;
  final String? choiceText;
  final bool? synced;
  const TrackedDay(
      {required this.uuid,
      required this.date,
      required this.confirmed,
      this.choiceId,
      this.choiceText,
      this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['date'] = Variable<DateTime>(date);
    map['confirmed'] = Variable<bool>(confirmed);
    if (!nullToAbsent || choiceId != null) {
      map['choice_id'] = Variable<int>(choiceId);
    }
    if (!nullToAbsent || choiceText != null) {
      map['choice_text'] = Variable<String>(choiceText);
    }
    if (!nullToAbsent || synced != null) {
      map['synced'] = Variable<bool>(synced);
    }
    return map;
  }

  TrackedDaysCompanion toCompanion(bool nullToAbsent) {
    return TrackedDaysCompanion(
      uuid: Value(uuid),
      date: Value(date),
      confirmed: Value(confirmed),
      choiceId: choiceId == null && nullToAbsent
          ? const Value.absent()
          : Value(choiceId),
      choiceText: choiceText == null && nullToAbsent
          ? const Value.absent()
          : Value(choiceText),
      synced:
          synced == null && nullToAbsent ? const Value.absent() : Value(synced),
    );
  }

  factory TrackedDay.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TrackedDay(
      uuid: serializer.fromJson<String>(json['uuid']),
      date: serializer.fromJson<DateTime>(json['date']),
      confirmed: serializer.fromJson<bool>(json['confirmed']),
      choiceId: serializer.fromJson<int?>(json['choiceId']),
      choiceText: serializer.fromJson<String?>(json['choiceText']),
      synced: serializer.fromJson<bool?>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'date': serializer.toJson<DateTime>(date),
      'confirmed': serializer.toJson<bool>(confirmed),
      'choiceId': serializer.toJson<int?>(choiceId),
      'choiceText': serializer.toJson<String?>(choiceText),
      'synced': serializer.toJson<bool?>(synced),
    };
  }

  TrackedDay copyWith(
          {String? uuid,
          DateTime? date,
          bool? confirmed,
          Value<int?> choiceId = const Value.absent(),
          Value<String?> choiceText = const Value.absent(),
          Value<bool?> synced = const Value.absent()}) =>
      TrackedDay(
        uuid: uuid ?? this.uuid,
        date: date ?? this.date,
        confirmed: confirmed ?? this.confirmed,
        choiceId: choiceId.present ? choiceId.value : this.choiceId,
        choiceText: choiceText.present ? choiceText.value : this.choiceText,
        synced: synced.present ? synced.value : this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('TrackedDay(')
          ..write('uuid: $uuid, ')
          ..write('date: $date, ')
          ..write('confirmed: $confirmed, ')
          ..write('choiceId: $choiceId, ')
          ..write('choiceText: $choiceText, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(uuid, date, confirmed, choiceId, choiceText, synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TrackedDay &&
          other.uuid == this.uuid &&
          other.date == this.date &&
          other.confirmed == this.confirmed &&
          other.choiceId == this.choiceId &&
          other.choiceText == this.choiceText &&
          other.synced == this.synced);
}

class TrackedDaysCompanion extends UpdateCompanion<TrackedDay> {
  final Value<String> uuid;
  final Value<DateTime> date;
  final Value<bool> confirmed;
  final Value<int?> choiceId;
  final Value<String?> choiceText;
  final Value<bool?> synced;
  final Value<int> rowid;
  const TrackedDaysCompanion({
    this.uuid = const Value.absent(),
    this.date = const Value.absent(),
    this.confirmed = const Value.absent(),
    this.choiceId = const Value.absent(),
    this.choiceText = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  TrackedDaysCompanion.insert({
    this.uuid = const Value.absent(),
    required DateTime date,
    this.confirmed = const Value.absent(),
    this.choiceId = const Value.absent(),
    this.choiceText = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  }) : date = Value(date);
  static Insertable<TrackedDay> custom({
    Expression<String>? uuid,
    Expression<DateTime>? date,
    Expression<bool>? confirmed,
    Expression<int>? choiceId,
    Expression<String>? choiceText,
    Expression<bool>? synced,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (date != null) 'date': date,
      if (confirmed != null) 'confirmed': confirmed,
      if (choiceId != null) 'choice_id': choiceId,
      if (choiceText != null) 'choice_text': choiceText,
      if (synced != null) 'synced': synced,
      if (rowid != null) 'rowid': rowid,
    });
  }

  TrackedDaysCompanion copyWith(
      {Value<String>? uuid,
      Value<DateTime>? date,
      Value<bool>? confirmed,
      Value<int?>? choiceId,
      Value<String?>? choiceText,
      Value<bool?>? synced,
      Value<int>? rowid}) {
    return TrackedDaysCompanion(
      uuid: uuid ?? this.uuid,
      date: date ?? this.date,
      confirmed: confirmed ?? this.confirmed,
      choiceId: choiceId ?? this.choiceId,
      choiceText: choiceText ?? this.choiceText,
      synced: synced ?? this.synced,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (date.present) {
      map['date'] = Variable<DateTime>(date.value);
    }
    if (confirmed.present) {
      map['confirmed'] = Variable<bool>(confirmed.value);
    }
    if (choiceId.present) {
      map['choice_id'] = Variable<int>(choiceId.value);
    }
    if (choiceText.present) {
      map['choice_text'] = Variable<String>(choiceText.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TrackedDaysCompanion(')
          ..write('uuid: $uuid, ')
          ..write('date: $date, ')
          ..write('confirmed: $confirmed, ')
          ..write('choiceId: $choiceId, ')
          ..write('choiceText: $choiceText, ')
          ..write('synced: $synced, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $ClassifiedPeriodsTable extends ClassifiedPeriods
    with TableInfo<$ClassifiedPeriodsTable, ClassifiedPeriod> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ClassifiedPeriodsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _trackedDayUuidMeta =
      const VerificationMeta('trackedDayUuid');
  @override
  late final GeneratedColumn<String> trackedDayUuid = GeneratedColumn<String>(
      'tracked_day_uuid', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES tracked_days (uuid)'));
  static const VerificationMeta _originMeta = const VerificationMeta('origin');
  @override
  late final GeneratedColumn<String> origin = GeneratedColumn<String>(
      'origin', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _startDateMeta =
      const VerificationMeta('startDate');
  @override
  late final GeneratedColumn<DateTime> startDate = GeneratedColumn<DateTime>(
      'start_date', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _endDateMeta =
      const VerificationMeta('endDate');
  @override
  late final GeneratedColumn<DateTime> endDate = GeneratedColumn<DateTime>(
      'end_date', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _confirmedMeta =
      const VerificationMeta('confirmed');
  @override
  late final GeneratedColumn<bool> confirmed = GeneratedColumn<bool>(
      'confirmed', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("confirmed" IN (0, 1))'),
      defaultValue: const Constant(false));
  static const VerificationMeta _createdOnMeta =
      const VerificationMeta('createdOn');
  @override
  late final GeneratedColumn<DateTime> createdOn = GeneratedColumn<DateTime>(
      'created_on', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _deletedOnMeta =
      const VerificationMeta('deletedOn');
  @override
  late final GeneratedColumn<DateTime> deletedOn = GeneratedColumn<DateTime>(
      'deleted_on', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        trackedDayUuid,
        origin,
        startDate,
        endDate,
        confirmed,
        createdOn,
        deletedOn,
        synced
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'classified_periods';
  @override
  VerificationContext validateIntegrity(Insertable<ClassifiedPeriod> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('tracked_day_uuid')) {
      context.handle(
          _trackedDayUuidMeta,
          trackedDayUuid.isAcceptableOrUnknown(
              data['tracked_day_uuid']!, _trackedDayUuidMeta));
    }
    if (data.containsKey('origin')) {
      context.handle(_originMeta,
          origin.isAcceptableOrUnknown(data['origin']!, _originMeta));
    }
    if (data.containsKey('start_date')) {
      context.handle(_startDateMeta,
          startDate.isAcceptableOrUnknown(data['start_date']!, _startDateMeta));
    } else if (isInserting) {
      context.missing(_startDateMeta);
    }
    if (data.containsKey('end_date')) {
      context.handle(_endDateMeta,
          endDate.isAcceptableOrUnknown(data['end_date']!, _endDateMeta));
    } else if (isInserting) {
      context.missing(_endDateMeta);
    }
    if (data.containsKey('confirmed')) {
      context.handle(_confirmedMeta,
          confirmed.isAcceptableOrUnknown(data['confirmed']!, _confirmedMeta));
    }
    if (data.containsKey('created_on')) {
      context.handle(_createdOnMeta,
          createdOn.isAcceptableOrUnknown(data['created_on']!, _createdOnMeta));
    } else if (isInserting) {
      context.missing(_createdOnMeta);
    }
    if (data.containsKey('deleted_on')) {
      context.handle(_deletedOnMeta,
          deletedOn.isAcceptableOrUnknown(data['deleted_on']!, _deletedOnMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  ClassifiedPeriod map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ClassifiedPeriod(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      trackedDayUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}tracked_day_uuid']),
      origin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}origin']),
      startDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}start_date'])!,
      endDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}end_date'])!,
      confirmed: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}confirmed'])!,
      createdOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}created_on'])!,
      deletedOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}deleted_on']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced'])!,
    );
  }

  @override
  $ClassifiedPeriodsTable createAlias(String alias) {
    return $ClassifiedPeriodsTable(attachedDatabase, alias);
  }
}

class ClassifiedPeriod extends DataClass
    implements Insertable<ClassifiedPeriod> {
  final String uuid;
  final String? trackedDayUuid;
  final String? origin;
  final DateTime startDate;
  final DateTime endDate;
  final bool confirmed;
  final DateTime createdOn;
  final DateTime? deletedOn;
  final bool synced;
  const ClassifiedPeriod(
      {required this.uuid,
      this.trackedDayUuid,
      this.origin,
      required this.startDate,
      required this.endDate,
      required this.confirmed,
      required this.createdOn,
      this.deletedOn,
      required this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    if (!nullToAbsent || trackedDayUuid != null) {
      map['tracked_day_uuid'] = Variable<String>(trackedDayUuid);
    }
    if (!nullToAbsent || origin != null) {
      map['origin'] = Variable<String>(origin);
    }
    map['start_date'] = Variable<DateTime>(startDate);
    map['end_date'] = Variable<DateTime>(endDate);
    map['confirmed'] = Variable<bool>(confirmed);
    map['created_on'] = Variable<DateTime>(createdOn);
    if (!nullToAbsent || deletedOn != null) {
      map['deleted_on'] = Variable<DateTime>(deletedOn);
    }
    map['synced'] = Variable<bool>(synced);
    return map;
  }

  ClassifiedPeriodsCompanion toCompanion(bool nullToAbsent) {
    return ClassifiedPeriodsCompanion(
      uuid: Value(uuid),
      trackedDayUuid: trackedDayUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(trackedDayUuid),
      origin:
          origin == null && nullToAbsent ? const Value.absent() : Value(origin),
      startDate: Value(startDate),
      endDate: Value(endDate),
      confirmed: Value(confirmed),
      createdOn: Value(createdOn),
      deletedOn: deletedOn == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedOn),
      synced: Value(synced),
    );
  }

  factory ClassifiedPeriod.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ClassifiedPeriod(
      uuid: serializer.fromJson<String>(json['uuid']),
      trackedDayUuid: serializer.fromJson<String?>(json['trackedDayUuid']),
      origin: serializer.fromJson<String?>(json['origin']),
      startDate: serializer.fromJson<DateTime>(json['startDate']),
      endDate: serializer.fromJson<DateTime>(json['endDate']),
      confirmed: serializer.fromJson<bool>(json['confirmed']),
      createdOn: serializer.fromJson<DateTime>(json['createdOn']),
      deletedOn: serializer.fromJson<DateTime?>(json['deletedOn']),
      synced: serializer.fromJson<bool>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'trackedDayUuid': serializer.toJson<String?>(trackedDayUuid),
      'origin': serializer.toJson<String?>(origin),
      'startDate': serializer.toJson<DateTime>(startDate),
      'endDate': serializer.toJson<DateTime>(endDate),
      'confirmed': serializer.toJson<bool>(confirmed),
      'createdOn': serializer.toJson<DateTime>(createdOn),
      'deletedOn': serializer.toJson<DateTime?>(deletedOn),
      'synced': serializer.toJson<bool>(synced),
    };
  }

  ClassifiedPeriod copyWith(
          {String? uuid,
          Value<String?> trackedDayUuid = const Value.absent(),
          Value<String?> origin = const Value.absent(),
          DateTime? startDate,
          DateTime? endDate,
          bool? confirmed,
          DateTime? createdOn,
          Value<DateTime?> deletedOn = const Value.absent(),
          bool? synced}) =>
      ClassifiedPeriod(
        uuid: uuid ?? this.uuid,
        trackedDayUuid:
            trackedDayUuid.present ? trackedDayUuid.value : this.trackedDayUuid,
        origin: origin.present ? origin.value : this.origin,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
        confirmed: confirmed ?? this.confirmed,
        createdOn: createdOn ?? this.createdOn,
        deletedOn: deletedOn.present ? deletedOn.value : this.deletedOn,
        synced: synced ?? this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('ClassifiedPeriod(')
          ..write('uuid: $uuid, ')
          ..write('trackedDayUuid: $trackedDayUuid, ')
          ..write('origin: $origin, ')
          ..write('startDate: $startDate, ')
          ..write('endDate: $endDate, ')
          ..write('confirmed: $confirmed, ')
          ..write('createdOn: $createdOn, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, trackedDayUuid, origin, startDate,
      endDate, confirmed, createdOn, deletedOn, synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ClassifiedPeriod &&
          other.uuid == this.uuid &&
          other.trackedDayUuid == this.trackedDayUuid &&
          other.origin == this.origin &&
          other.startDate == this.startDate &&
          other.endDate == this.endDate &&
          other.confirmed == this.confirmed &&
          other.createdOn == this.createdOn &&
          other.deletedOn == this.deletedOn &&
          other.synced == this.synced);
}

class ClassifiedPeriodsCompanion extends UpdateCompanion<ClassifiedPeriod> {
  final Value<String> uuid;
  final Value<String?> trackedDayUuid;
  final Value<String?> origin;
  final Value<DateTime> startDate;
  final Value<DateTime> endDate;
  final Value<bool> confirmed;
  final Value<DateTime> createdOn;
  final Value<DateTime?> deletedOn;
  final Value<bool> synced;
  final Value<int> rowid;
  const ClassifiedPeriodsCompanion({
    this.uuid = const Value.absent(),
    this.trackedDayUuid = const Value.absent(),
    this.origin = const Value.absent(),
    this.startDate = const Value.absent(),
    this.endDate = const Value.absent(),
    this.confirmed = const Value.absent(),
    this.createdOn = const Value.absent(),
    this.deletedOn = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ClassifiedPeriodsCompanion.insert({
    this.uuid = const Value.absent(),
    this.trackedDayUuid = const Value.absent(),
    this.origin = const Value.absent(),
    required DateTime startDate,
    required DateTime endDate,
    this.confirmed = const Value.absent(),
    required DateTime createdOn,
    this.deletedOn = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : startDate = Value(startDate),
        endDate = Value(endDate),
        createdOn = Value(createdOn);
  static Insertable<ClassifiedPeriod> custom({
    Expression<String>? uuid,
    Expression<String>? trackedDayUuid,
    Expression<String>? origin,
    Expression<DateTime>? startDate,
    Expression<DateTime>? endDate,
    Expression<bool>? confirmed,
    Expression<DateTime>? createdOn,
    Expression<DateTime>? deletedOn,
    Expression<bool>? synced,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (trackedDayUuid != null) 'tracked_day_uuid': trackedDayUuid,
      if (origin != null) 'origin': origin,
      if (startDate != null) 'start_date': startDate,
      if (endDate != null) 'end_date': endDate,
      if (confirmed != null) 'confirmed': confirmed,
      if (createdOn != null) 'created_on': createdOn,
      if (deletedOn != null) 'deleted_on': deletedOn,
      if (synced != null) 'synced': synced,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ClassifiedPeriodsCompanion copyWith(
      {Value<String>? uuid,
      Value<String?>? trackedDayUuid,
      Value<String?>? origin,
      Value<DateTime>? startDate,
      Value<DateTime>? endDate,
      Value<bool>? confirmed,
      Value<DateTime>? createdOn,
      Value<DateTime?>? deletedOn,
      Value<bool>? synced,
      Value<int>? rowid}) {
    return ClassifiedPeriodsCompanion(
      uuid: uuid ?? this.uuid,
      trackedDayUuid: trackedDayUuid ?? this.trackedDayUuid,
      origin: origin ?? this.origin,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      confirmed: confirmed ?? this.confirmed,
      createdOn: createdOn ?? this.createdOn,
      deletedOn: deletedOn ?? this.deletedOn,
      synced: synced ?? this.synced,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (trackedDayUuid.present) {
      map['tracked_day_uuid'] = Variable<String>(trackedDayUuid.value);
    }
    if (origin.present) {
      map['origin'] = Variable<String>(origin.value);
    }
    if (startDate.present) {
      map['start_date'] = Variable<DateTime>(startDate.value);
    }
    if (endDate.present) {
      map['end_date'] = Variable<DateTime>(endDate.value);
    }
    if (confirmed.present) {
      map['confirmed'] = Variable<bool>(confirmed.value);
    }
    if (createdOn.present) {
      map['created_on'] = Variable<DateTime>(createdOn.value);
    }
    if (deletedOn.present) {
      map['deleted_on'] = Variable<DateTime>(deletedOn.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ClassifiedPeriodsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('trackedDayUuid: $trackedDayUuid, ')
          ..write('origin: $origin, ')
          ..write('startDate: $startDate, ')
          ..write('endDate: $endDate, ')
          ..write('confirmed: $confirmed, ')
          ..write('createdOn: $createdOn, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('synced: $synced, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $ClustersTable extends Clusters with TableInfo<$ClustersTable, Cluster> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ClustersTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _classifiedPeriodUuidMeta =
      const VerificationMeta('classifiedPeriodUuid');
  @override
  late final GeneratedColumn<String> classifiedPeriodUuid =
      GeneratedColumn<String>(
          'classified_period_uuid', aliasedName, true,
          type: DriftSqlType.string,
          requiredDuringInsert: false,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES classified_periods (uuid)'),
          defaultValue: const Constant(null));
  static const VerificationMeta _startTimeMeta =
      const VerificationMeta('startTime');
  @override
  late final GeneratedColumn<DateTime> startTime = GeneratedColumn<DateTime>(
      'start_time', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _endTimeMeta =
      const VerificationMeta('endTime');
  @override
  late final GeneratedColumn<DateTime> endTime = GeneratedColumn<DateTime>(
      'end_time', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _createdOnMeta =
      const VerificationMeta('createdOn');
  @override
  late final GeneratedColumn<DateTime> createdOn = GeneratedColumn<DateTime>(
      'created_on', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _transportMeta =
      const VerificationMeta('transport');
  @override
  late final GeneratedColumn<int> transport = GeneratedColumn<int>(
      'transport', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _averageSpeedMeta =
      const VerificationMeta('averageSpeed');
  @override
  late final GeneratedColumn<int> averageSpeed = GeneratedColumn<int>(
      'average_speed', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _averageAccuracyMeta =
      const VerificationMeta('averageAccuracy');
  @override
  late final GeneratedColumn<double> averageAccuracy = GeneratedColumn<double>(
      'average_accuracy', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _deletedOnMeta =
      const VerificationMeta('deletedOn');
  @override
  late final GeneratedColumn<DateTime> deletedOn = GeneratedColumn<DateTime>(
      'deleted_on', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _trainProbabilityMeta =
      const VerificationMeta('trainProbability');
  @override
  late final GeneratedColumn<double> trainProbability = GeneratedColumn<double>(
      'train_probability', aliasedName, true,
      type: DriftSqlType.double,
      requiredDuringInsert: false,
      defaultValue: const Constant(null));
  static const VerificationMeta _tramProbabilityMeta =
      const VerificationMeta('tramProbability');
  @override
  late final GeneratedColumn<double> tramProbability = GeneratedColumn<double>(
      'tram_probability', aliasedName, true,
      type: DriftSqlType.double,
      requiredDuringInsert: false,
      defaultValue: const Constant(null));
  static const VerificationMeta _subwayProbabilityMeta =
      const VerificationMeta('subwayProbability');
  @override
  late final GeneratedColumn<double> subwayProbability =
      GeneratedColumn<double>('subway_probability', aliasedName, true,
          type: DriftSqlType.double,
          requiredDuringInsert: false,
          defaultValue: const Constant(null));
  static const VerificationMeta _walkingProbabilityMeta =
      const VerificationMeta('walkingProbability');
  @override
  late final GeneratedColumn<double> walkingProbability =
      GeneratedColumn<double>('walking_probability', aliasedName, true,
          type: DriftSqlType.double,
          requiredDuringInsert: false,
          defaultValue: const Constant(null));
  static const VerificationMeta _carProbabilityMeta =
      const VerificationMeta('carProbability');
  @override
  late final GeneratedColumn<double> carProbability = GeneratedColumn<double>(
      'car_probability', aliasedName, true,
      type: DriftSqlType.double,
      requiredDuringInsert: false,
      defaultValue: const Constant(null));
  static const VerificationMeta _bicycleProbabilityMeta =
      const VerificationMeta('bicycleProbability');
  @override
  late final GeneratedColumn<double> bicycleProbability =
      GeneratedColumn<double>('bicycle_probability', aliasedName, true,
          type: DriftSqlType.double,
          requiredDuringInsert: false,
          defaultValue: const Constant(null));
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  static const VerificationMeta _userUuidMeta =
      const VerificationMeta('userUuid');
  @override
  late final GeneratedColumn<String> userUuid = GeneratedColumn<String>(
      'user_uuid', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant(null));
  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        classifiedPeriodUuid,
        startTime,
        endTime,
        createdOn,
        transport,
        averageSpeed,
        averageAccuracy,
        deletedOn,
        trainProbability,
        tramProbability,
        subwayProbability,
        walkingProbability,
        carProbability,
        bicycleProbability,
        synced,
        userUuid
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'clusters';
  @override
  VerificationContext validateIntegrity(Insertable<Cluster> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('classified_period_uuid')) {
      context.handle(
          _classifiedPeriodUuidMeta,
          classifiedPeriodUuid.isAcceptableOrUnknown(
              data['classified_period_uuid']!, _classifiedPeriodUuidMeta));
    }
    if (data.containsKey('start_time')) {
      context.handle(_startTimeMeta,
          startTime.isAcceptableOrUnknown(data['start_time']!, _startTimeMeta));
    } else if (isInserting) {
      context.missing(_startTimeMeta);
    }
    if (data.containsKey('end_time')) {
      context.handle(_endTimeMeta,
          endTime.isAcceptableOrUnknown(data['end_time']!, _endTimeMeta));
    } else if (isInserting) {
      context.missing(_endTimeMeta);
    }
    if (data.containsKey('created_on')) {
      context.handle(_createdOnMeta,
          createdOn.isAcceptableOrUnknown(data['created_on']!, _createdOnMeta));
    } else if (isInserting) {
      context.missing(_createdOnMeta);
    }
    if (data.containsKey('transport')) {
      context.handle(_transportMeta,
          transport.isAcceptableOrUnknown(data['transport']!, _transportMeta));
    }
    if (data.containsKey('average_speed')) {
      context.handle(
          _averageSpeedMeta,
          averageSpeed.isAcceptableOrUnknown(
              data['average_speed']!, _averageSpeedMeta));
    } else if (isInserting) {
      context.missing(_averageSpeedMeta);
    }
    if (data.containsKey('average_accuracy')) {
      context.handle(
          _averageAccuracyMeta,
          averageAccuracy.isAcceptableOrUnknown(
              data['average_accuracy']!, _averageAccuracyMeta));
    } else if (isInserting) {
      context.missing(_averageAccuracyMeta);
    }
    if (data.containsKey('deleted_on')) {
      context.handle(_deletedOnMeta,
          deletedOn.isAcceptableOrUnknown(data['deleted_on']!, _deletedOnMeta));
    }
    if (data.containsKey('train_probability')) {
      context.handle(
          _trainProbabilityMeta,
          trainProbability.isAcceptableOrUnknown(
              data['train_probability']!, _trainProbabilityMeta));
    }
    if (data.containsKey('tram_probability')) {
      context.handle(
          _tramProbabilityMeta,
          tramProbability.isAcceptableOrUnknown(
              data['tram_probability']!, _tramProbabilityMeta));
    }
    if (data.containsKey('subway_probability')) {
      context.handle(
          _subwayProbabilityMeta,
          subwayProbability.isAcceptableOrUnknown(
              data['subway_probability']!, _subwayProbabilityMeta));
    }
    if (data.containsKey('walking_probability')) {
      context.handle(
          _walkingProbabilityMeta,
          walkingProbability.isAcceptableOrUnknown(
              data['walking_probability']!, _walkingProbabilityMeta));
    }
    if (data.containsKey('car_probability')) {
      context.handle(
          _carProbabilityMeta,
          carProbability.isAcceptableOrUnknown(
              data['car_probability']!, _carProbabilityMeta));
    }
    if (data.containsKey('bicycle_probability')) {
      context.handle(
          _bicycleProbabilityMeta,
          bicycleProbability.isAcceptableOrUnknown(
              data['bicycle_probability']!, _bicycleProbabilityMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    if (data.containsKey('user_uuid')) {
      context.handle(_userUuidMeta,
          userUuid.isAcceptableOrUnknown(data['user_uuid']!, _userUuidMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  Cluster map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Cluster(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      classifiedPeriodUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}classified_period_uuid']),
      startTime: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}start_time'])!,
      endTime: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}end_time'])!,
      createdOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}created_on'])!,
      transport: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}transport']),
      averageSpeed: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}average_speed'])!,
      averageAccuracy: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}average_accuracy'])!,
      deletedOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}deleted_on']),
      trainProbability: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}train_probability']),
      tramProbability: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}tram_probability']),
      subwayProbability: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}subway_probability']),
      walkingProbability: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}walking_probability']),
      carProbability: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}car_probability']),
      bicycleProbability: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}bicycle_probability']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced'])!,
      userUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}user_uuid']),
    );
  }

  @override
  $ClustersTable createAlias(String alias) {
    return $ClustersTable(attachedDatabase, alias);
  }
}

class Cluster extends DataClass implements Insertable<Cluster> {
  final String uuid;
  final String? classifiedPeriodUuid;
  final DateTime startTime;
  final DateTime endTime;
  final DateTime createdOn;
  final int? transport;
  final int averageSpeed;
  final double averageAccuracy;
  final DateTime? deletedOn;
  final double? trainProbability;
  final double? tramProbability;
  final double? subwayProbability;
  final double? walkingProbability;
  final double? carProbability;
  final double? bicycleProbability;
  final bool synced;
  final String? userUuid;
  const Cluster(
      {required this.uuid,
      this.classifiedPeriodUuid,
      required this.startTime,
      required this.endTime,
      required this.createdOn,
      this.transport,
      required this.averageSpeed,
      required this.averageAccuracy,
      this.deletedOn,
      this.trainProbability,
      this.tramProbability,
      this.subwayProbability,
      this.walkingProbability,
      this.carProbability,
      this.bicycleProbability,
      required this.synced,
      this.userUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    if (!nullToAbsent || classifiedPeriodUuid != null) {
      map['classified_period_uuid'] = Variable<String>(classifiedPeriodUuid);
    }
    map['start_time'] = Variable<DateTime>(startTime);
    map['end_time'] = Variable<DateTime>(endTime);
    map['created_on'] = Variable<DateTime>(createdOn);
    if (!nullToAbsent || transport != null) {
      map['transport'] = Variable<int>(transport);
    }
    map['average_speed'] = Variable<int>(averageSpeed);
    map['average_accuracy'] = Variable<double>(averageAccuracy);
    if (!nullToAbsent || deletedOn != null) {
      map['deleted_on'] = Variable<DateTime>(deletedOn);
    }
    if (!nullToAbsent || trainProbability != null) {
      map['train_probability'] = Variable<double>(trainProbability);
    }
    if (!nullToAbsent || tramProbability != null) {
      map['tram_probability'] = Variable<double>(tramProbability);
    }
    if (!nullToAbsent || subwayProbability != null) {
      map['subway_probability'] = Variable<double>(subwayProbability);
    }
    if (!nullToAbsent || walkingProbability != null) {
      map['walking_probability'] = Variable<double>(walkingProbability);
    }
    if (!nullToAbsent || carProbability != null) {
      map['car_probability'] = Variable<double>(carProbability);
    }
    if (!nullToAbsent || bicycleProbability != null) {
      map['bicycle_probability'] = Variable<double>(bicycleProbability);
    }
    map['synced'] = Variable<bool>(synced);
    if (!nullToAbsent || userUuid != null) {
      map['user_uuid'] = Variable<String>(userUuid);
    }
    return map;
  }

  ClustersCompanion toCompanion(bool nullToAbsent) {
    return ClustersCompanion(
      uuid: Value(uuid),
      classifiedPeriodUuid: classifiedPeriodUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(classifiedPeriodUuid),
      startTime: Value(startTime),
      endTime: Value(endTime),
      createdOn: Value(createdOn),
      transport: transport == null && nullToAbsent
          ? const Value.absent()
          : Value(transport),
      averageSpeed: Value(averageSpeed),
      averageAccuracy: Value(averageAccuracy),
      deletedOn: deletedOn == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedOn),
      trainProbability: trainProbability == null && nullToAbsent
          ? const Value.absent()
          : Value(trainProbability),
      tramProbability: tramProbability == null && nullToAbsent
          ? const Value.absent()
          : Value(tramProbability),
      subwayProbability: subwayProbability == null && nullToAbsent
          ? const Value.absent()
          : Value(subwayProbability),
      walkingProbability: walkingProbability == null && nullToAbsent
          ? const Value.absent()
          : Value(walkingProbability),
      carProbability: carProbability == null && nullToAbsent
          ? const Value.absent()
          : Value(carProbability),
      bicycleProbability: bicycleProbability == null && nullToAbsent
          ? const Value.absent()
          : Value(bicycleProbability),
      synced: Value(synced),
      userUuid: userUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(userUuid),
    );
  }

  factory Cluster.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Cluster(
      uuid: serializer.fromJson<String>(json['uuid']),
      classifiedPeriodUuid:
          serializer.fromJson<String?>(json['classifiedPeriodUuid']),
      startTime: serializer.fromJson<DateTime>(json['startTime']),
      endTime: serializer.fromJson<DateTime>(json['endTime']),
      createdOn: serializer.fromJson<DateTime>(json['createdOn']),
      transport: serializer.fromJson<int?>(json['transport']),
      averageSpeed: serializer.fromJson<int>(json['averageSpeed']),
      averageAccuracy: serializer.fromJson<double>(json['averageAccuracy']),
      deletedOn: serializer.fromJson<DateTime?>(json['deletedOn']),
      trainProbability: serializer.fromJson<double?>(json['trainProbability']),
      tramProbability: serializer.fromJson<double?>(json['tramProbability']),
      subwayProbability:
          serializer.fromJson<double?>(json['subwayProbability']),
      walkingProbability:
          serializer.fromJson<double?>(json['walkingProbability']),
      carProbability: serializer.fromJson<double?>(json['carProbability']),
      bicycleProbability:
          serializer.fromJson<double?>(json['bicycleProbability']),
      synced: serializer.fromJson<bool>(json['synced']),
      userUuid: serializer.fromJson<String?>(json['userUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'classifiedPeriodUuid': serializer.toJson<String?>(classifiedPeriodUuid),
      'startTime': serializer.toJson<DateTime>(startTime),
      'endTime': serializer.toJson<DateTime>(endTime),
      'createdOn': serializer.toJson<DateTime>(createdOn),
      'transport': serializer.toJson<int?>(transport),
      'averageSpeed': serializer.toJson<int>(averageSpeed),
      'averageAccuracy': serializer.toJson<double>(averageAccuracy),
      'deletedOn': serializer.toJson<DateTime?>(deletedOn),
      'trainProbability': serializer.toJson<double?>(trainProbability),
      'tramProbability': serializer.toJson<double?>(tramProbability),
      'subwayProbability': serializer.toJson<double?>(subwayProbability),
      'walkingProbability': serializer.toJson<double?>(walkingProbability),
      'carProbability': serializer.toJson<double?>(carProbability),
      'bicycleProbability': serializer.toJson<double?>(bicycleProbability),
      'synced': serializer.toJson<bool>(synced),
      'userUuid': serializer.toJson<String?>(userUuid),
    };
  }

  Cluster copyWith(
          {String? uuid,
          Value<String?> classifiedPeriodUuid = const Value.absent(),
          DateTime? startTime,
          DateTime? endTime,
          DateTime? createdOn,
          Value<int?> transport = const Value.absent(),
          int? averageSpeed,
          double? averageAccuracy,
          Value<DateTime?> deletedOn = const Value.absent(),
          Value<double?> trainProbability = const Value.absent(),
          Value<double?> tramProbability = const Value.absent(),
          Value<double?> subwayProbability = const Value.absent(),
          Value<double?> walkingProbability = const Value.absent(),
          Value<double?> carProbability = const Value.absent(),
          Value<double?> bicycleProbability = const Value.absent(),
          bool? synced,
          Value<String?> userUuid = const Value.absent()}) =>
      Cluster(
        uuid: uuid ?? this.uuid,
        classifiedPeriodUuid: classifiedPeriodUuid.present
            ? classifiedPeriodUuid.value
            : this.classifiedPeriodUuid,
        startTime: startTime ?? this.startTime,
        endTime: endTime ?? this.endTime,
        createdOn: createdOn ?? this.createdOn,
        transport: transport.present ? transport.value : this.transport,
        averageSpeed: averageSpeed ?? this.averageSpeed,
        averageAccuracy: averageAccuracy ?? this.averageAccuracy,
        deletedOn: deletedOn.present ? deletedOn.value : this.deletedOn,
        trainProbability: trainProbability.present
            ? trainProbability.value
            : this.trainProbability,
        tramProbability: tramProbability.present
            ? tramProbability.value
            : this.tramProbability,
        subwayProbability: subwayProbability.present
            ? subwayProbability.value
            : this.subwayProbability,
        walkingProbability: walkingProbability.present
            ? walkingProbability.value
            : this.walkingProbability,
        carProbability:
            carProbability.present ? carProbability.value : this.carProbability,
        bicycleProbability: bicycleProbability.present
            ? bicycleProbability.value
            : this.bicycleProbability,
        synced: synced ?? this.synced,
        userUuid: userUuid.present ? userUuid.value : this.userUuid,
      );
  @override
  String toString() {
    return (StringBuffer('Cluster(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('startTime: $startTime, ')
          ..write('endTime: $endTime, ')
          ..write('createdOn: $createdOn, ')
          ..write('transport: $transport, ')
          ..write('averageSpeed: $averageSpeed, ')
          ..write('averageAccuracy: $averageAccuracy, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('trainProbability: $trainProbability, ')
          ..write('tramProbability: $tramProbability, ')
          ..write('subwayProbability: $subwayProbability, ')
          ..write('walkingProbability: $walkingProbability, ')
          ..write('carProbability: $carProbability, ')
          ..write('bicycleProbability: $bicycleProbability, ')
          ..write('synced: $synced, ')
          ..write('userUuid: $userUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      uuid,
      classifiedPeriodUuid,
      startTime,
      endTime,
      createdOn,
      transport,
      averageSpeed,
      averageAccuracy,
      deletedOn,
      trainProbability,
      tramProbability,
      subwayProbability,
      walkingProbability,
      carProbability,
      bicycleProbability,
      synced,
      userUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Cluster &&
          other.uuid == this.uuid &&
          other.classifiedPeriodUuid == this.classifiedPeriodUuid &&
          other.startTime == this.startTime &&
          other.endTime == this.endTime &&
          other.createdOn == this.createdOn &&
          other.transport == this.transport &&
          other.averageSpeed == this.averageSpeed &&
          other.averageAccuracy == this.averageAccuracy &&
          other.deletedOn == this.deletedOn &&
          other.trainProbability == this.trainProbability &&
          other.tramProbability == this.tramProbability &&
          other.subwayProbability == this.subwayProbability &&
          other.walkingProbability == this.walkingProbability &&
          other.carProbability == this.carProbability &&
          other.bicycleProbability == this.bicycleProbability &&
          other.synced == this.synced &&
          other.userUuid == this.userUuid);
}

class ClustersCompanion extends UpdateCompanion<Cluster> {
  final Value<String> uuid;
  final Value<String?> classifiedPeriodUuid;
  final Value<DateTime> startTime;
  final Value<DateTime> endTime;
  final Value<DateTime> createdOn;
  final Value<int?> transport;
  final Value<int> averageSpeed;
  final Value<double> averageAccuracy;
  final Value<DateTime?> deletedOn;
  final Value<double?> trainProbability;
  final Value<double?> tramProbability;
  final Value<double?> subwayProbability;
  final Value<double?> walkingProbability;
  final Value<double?> carProbability;
  final Value<double?> bicycleProbability;
  final Value<bool> synced;
  final Value<String?> userUuid;
  final Value<int> rowid;
  const ClustersCompanion({
    this.uuid = const Value.absent(),
    this.classifiedPeriodUuid = const Value.absent(),
    this.startTime = const Value.absent(),
    this.endTime = const Value.absent(),
    this.createdOn = const Value.absent(),
    this.transport = const Value.absent(),
    this.averageSpeed = const Value.absent(),
    this.averageAccuracy = const Value.absent(),
    this.deletedOn = const Value.absent(),
    this.trainProbability = const Value.absent(),
    this.tramProbability = const Value.absent(),
    this.subwayProbability = const Value.absent(),
    this.walkingProbability = const Value.absent(),
    this.carProbability = const Value.absent(),
    this.bicycleProbability = const Value.absent(),
    this.synced = const Value.absent(),
    this.userUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ClustersCompanion.insert({
    this.uuid = const Value.absent(),
    this.classifiedPeriodUuid = const Value.absent(),
    required DateTime startTime,
    required DateTime endTime,
    required DateTime createdOn,
    this.transport = const Value.absent(),
    required int averageSpeed,
    required double averageAccuracy,
    this.deletedOn = const Value.absent(),
    this.trainProbability = const Value.absent(),
    this.tramProbability = const Value.absent(),
    this.subwayProbability = const Value.absent(),
    this.walkingProbability = const Value.absent(),
    this.carProbability = const Value.absent(),
    this.bicycleProbability = const Value.absent(),
    this.synced = const Value.absent(),
    this.userUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : startTime = Value(startTime),
        endTime = Value(endTime),
        createdOn = Value(createdOn),
        averageSpeed = Value(averageSpeed),
        averageAccuracy = Value(averageAccuracy);
  static Insertable<Cluster> custom({
    Expression<String>? uuid,
    Expression<String>? classifiedPeriodUuid,
    Expression<DateTime>? startTime,
    Expression<DateTime>? endTime,
    Expression<DateTime>? createdOn,
    Expression<int>? transport,
    Expression<int>? averageSpeed,
    Expression<double>? averageAccuracy,
    Expression<DateTime>? deletedOn,
    Expression<double>? trainProbability,
    Expression<double>? tramProbability,
    Expression<double>? subwayProbability,
    Expression<double>? walkingProbability,
    Expression<double>? carProbability,
    Expression<double>? bicycleProbability,
    Expression<bool>? synced,
    Expression<String>? userUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (classifiedPeriodUuid != null)
        'classified_period_uuid': classifiedPeriodUuid,
      if (startTime != null) 'start_time': startTime,
      if (endTime != null) 'end_time': endTime,
      if (createdOn != null) 'created_on': createdOn,
      if (transport != null) 'transport': transport,
      if (averageSpeed != null) 'average_speed': averageSpeed,
      if (averageAccuracy != null) 'average_accuracy': averageAccuracy,
      if (deletedOn != null) 'deleted_on': deletedOn,
      if (trainProbability != null) 'train_probability': trainProbability,
      if (tramProbability != null) 'tram_probability': tramProbability,
      if (subwayProbability != null) 'subway_probability': subwayProbability,
      if (walkingProbability != null) 'walking_probability': walkingProbability,
      if (carProbability != null) 'car_probability': carProbability,
      if (bicycleProbability != null) 'bicycle_probability': bicycleProbability,
      if (synced != null) 'synced': synced,
      if (userUuid != null) 'user_uuid': userUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ClustersCompanion copyWith(
      {Value<String>? uuid,
      Value<String?>? classifiedPeriodUuid,
      Value<DateTime>? startTime,
      Value<DateTime>? endTime,
      Value<DateTime>? createdOn,
      Value<int?>? transport,
      Value<int>? averageSpeed,
      Value<double>? averageAccuracy,
      Value<DateTime?>? deletedOn,
      Value<double?>? trainProbability,
      Value<double?>? tramProbability,
      Value<double?>? subwayProbability,
      Value<double?>? walkingProbability,
      Value<double?>? carProbability,
      Value<double?>? bicycleProbability,
      Value<bool>? synced,
      Value<String?>? userUuid,
      Value<int>? rowid}) {
    return ClustersCompanion(
      uuid: uuid ?? this.uuid,
      classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      createdOn: createdOn ?? this.createdOn,
      transport: transport ?? this.transport,
      averageSpeed: averageSpeed ?? this.averageSpeed,
      averageAccuracy: averageAccuracy ?? this.averageAccuracy,
      deletedOn: deletedOn ?? this.deletedOn,
      trainProbability: trainProbability ?? this.trainProbability,
      tramProbability: tramProbability ?? this.tramProbability,
      subwayProbability: subwayProbability ?? this.subwayProbability,
      walkingProbability: walkingProbability ?? this.walkingProbability,
      carProbability: carProbability ?? this.carProbability,
      bicycleProbability: bicycleProbability ?? this.bicycleProbability,
      synced: synced ?? this.synced,
      userUuid: userUuid ?? this.userUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (classifiedPeriodUuid.present) {
      map['classified_period_uuid'] =
          Variable<String>(classifiedPeriodUuid.value);
    }
    if (startTime.present) {
      map['start_time'] = Variable<DateTime>(startTime.value);
    }
    if (endTime.present) {
      map['end_time'] = Variable<DateTime>(endTime.value);
    }
    if (createdOn.present) {
      map['created_on'] = Variable<DateTime>(createdOn.value);
    }
    if (transport.present) {
      map['transport'] = Variable<int>(transport.value);
    }
    if (averageSpeed.present) {
      map['average_speed'] = Variable<int>(averageSpeed.value);
    }
    if (averageAccuracy.present) {
      map['average_accuracy'] = Variable<double>(averageAccuracy.value);
    }
    if (deletedOn.present) {
      map['deleted_on'] = Variable<DateTime>(deletedOn.value);
    }
    if (trainProbability.present) {
      map['train_probability'] = Variable<double>(trainProbability.value);
    }
    if (tramProbability.present) {
      map['tram_probability'] = Variable<double>(tramProbability.value);
    }
    if (subwayProbability.present) {
      map['subway_probability'] = Variable<double>(subwayProbability.value);
    }
    if (walkingProbability.present) {
      map['walking_probability'] = Variable<double>(walkingProbability.value);
    }
    if (carProbability.present) {
      map['car_probability'] = Variable<double>(carProbability.value);
    }
    if (bicycleProbability.present) {
      map['bicycle_probability'] = Variable<double>(bicycleProbability.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (userUuid.present) {
      map['user_uuid'] = Variable<String>(userUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ClustersCompanion(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('startTime: $startTime, ')
          ..write('endTime: $endTime, ')
          ..write('createdOn: $createdOn, ')
          ..write('transport: $transport, ')
          ..write('averageSpeed: $averageSpeed, ')
          ..write('averageAccuracy: $averageAccuracy, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('trainProbability: $trainProbability, ')
          ..write('tramProbability: $tramProbability, ')
          ..write('subwayProbability: $subwayProbability, ')
          ..write('walkingProbability: $walkingProbability, ')
          ..write('carProbability: $carProbability, ')
          ..write('bicycleProbability: $bicycleProbability, ')
          ..write('synced: $synced, ')
          ..write('userUuid: $userUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $SensorGeolocationsTable extends SensorGeolocations
    with TableInfo<$SensorGeolocationsTable, SensorGeolocation> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SensorGeolocationsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _latitudeMeta =
      const VerificationMeta('latitude');
  @override
  late final GeneratedColumn<double> latitude = GeneratedColumn<double>(
      'latitude', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _longitudeMeta =
      const VerificationMeta('longitude');
  @override
  late final GeneratedColumn<double> longitude = GeneratedColumn<double>(
      'longitude', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _altitudeMeta =
      const VerificationMeta('altitude');
  @override
  late final GeneratedColumn<double> altitude = GeneratedColumn<double>(
      'altitude', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _bearingMeta =
      const VerificationMeta('bearing');
  @override
  late final GeneratedColumn<double> bearing = GeneratedColumn<double>(
      'bearing', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _accuracyMeta =
      const VerificationMeta('accuracy');
  @override
  late final GeneratedColumn<double> accuracy = GeneratedColumn<double>(
      'accuracy', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _speedMeta = const VerificationMeta('speed');
  @override
  late final GeneratedColumn<double> speed = GeneratedColumn<double>(
      'speed', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _calculatedSpeedMeta =
      const VerificationMeta('calculatedSpeed');
  @override
  late final GeneratedColumn<double> calculatedSpeed = GeneratedColumn<double>(
      'calculated_speed', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _medianSpeedMeta =
      const VerificationMeta('medianSpeed');
  @override
  late final GeneratedColumn<double> medianSpeed = GeneratedColumn<double>(
      'median_speed', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _distanceMeta =
      const VerificationMeta('distance');
  @override
  late final GeneratedColumn<double> distance = GeneratedColumn<double>(
      'distance', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _sensoryTypeMeta =
      const VerificationMeta('sensoryType');
  @override
  late final GeneratedColumn<String> sensoryType = GeneratedColumn<String>(
      'sensory_type', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _clusterIdMeta =
      const VerificationMeta('clusterId');
  @override
  late final GeneratedColumn<String> clusterId = GeneratedColumn<String>(
      'cluster_id', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES clusters (uuid)'),
      defaultValue: const Constant(null));
  static const VerificationMeta _classifiedPeriodUuidMeta =
      const VerificationMeta('classifiedPeriodUuid');
  @override
  late final GeneratedColumn<String> classifiedPeriodUuid =
      GeneratedColumn<String>(
          'classified_period_uuid', aliasedName, true,
          type: DriftSqlType.string,
          requiredDuringInsert: false,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES classified_periods (uuid)'),
          defaultValue: const Constant(null));
  static const VerificationMeta _userIdMeta = const VerificationMeta('userId');
  @override
  late final GeneratedColumn<String> userId = GeneratedColumn<String>(
      'user_id', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  static const VerificationMeta _providerMeta =
      const VerificationMeta('provider');
  @override
  late final GeneratedColumn<String> provider = GeneratedColumn<String>(
      'provider', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _isNoiseMeta =
      const VerificationMeta('isNoise');
  @override
  late final GeneratedColumn<bool> isNoise = GeneratedColumn<bool>(
      'is_noise', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("is_noise" IN (0, 1))'));
  static const VerificationMeta _batteryLevelMeta =
      const VerificationMeta('batteryLevel');
  @override
  late final GeneratedColumn<int> batteryLevel = GeneratedColumn<int>(
      'battery_level', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _createdOnMeta =
      const VerificationMeta('createdOn');
  @override
  late final GeneratedColumn<DateTime> createdOn = GeneratedColumn<DateTime>(
      'created_on', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _deletedOnMeta =
      const VerificationMeta('deletedOn');
  @override
  late final GeneratedColumn<DateTime> deletedOn = GeneratedColumn<DateTime>(
      'deleted_on', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        latitude,
        longitude,
        altitude,
        bearing,
        accuracy,
        speed,
        calculatedSpeed,
        medianSpeed,
        distance,
        sensoryType,
        clusterId,
        classifiedPeriodUuid,
        userId,
        provider,
        isNoise,
        batteryLevel,
        createdOn,
        deletedOn,
        synced
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'sensor_geolocations';
  @override
  VerificationContext validateIntegrity(Insertable<SensorGeolocation> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('latitude')) {
      context.handle(_latitudeMeta,
          latitude.isAcceptableOrUnknown(data['latitude']!, _latitudeMeta));
    } else if (isInserting) {
      context.missing(_latitudeMeta);
    }
    if (data.containsKey('longitude')) {
      context.handle(_longitudeMeta,
          longitude.isAcceptableOrUnknown(data['longitude']!, _longitudeMeta));
    } else if (isInserting) {
      context.missing(_longitudeMeta);
    }
    if (data.containsKey('altitude')) {
      context.handle(_altitudeMeta,
          altitude.isAcceptableOrUnknown(data['altitude']!, _altitudeMeta));
    } else if (isInserting) {
      context.missing(_altitudeMeta);
    }
    if (data.containsKey('bearing')) {
      context.handle(_bearingMeta,
          bearing.isAcceptableOrUnknown(data['bearing']!, _bearingMeta));
    } else if (isInserting) {
      context.missing(_bearingMeta);
    }
    if (data.containsKey('accuracy')) {
      context.handle(_accuracyMeta,
          accuracy.isAcceptableOrUnknown(data['accuracy']!, _accuracyMeta));
    } else if (isInserting) {
      context.missing(_accuracyMeta);
    }
    if (data.containsKey('speed')) {
      context.handle(
          _speedMeta, speed.isAcceptableOrUnknown(data['speed']!, _speedMeta));
    } else if (isInserting) {
      context.missing(_speedMeta);
    }
    if (data.containsKey('calculated_speed')) {
      context.handle(
          _calculatedSpeedMeta,
          calculatedSpeed.isAcceptableOrUnknown(
              data['calculated_speed']!, _calculatedSpeedMeta));
    } else if (isInserting) {
      context.missing(_calculatedSpeedMeta);
    }
    if (data.containsKey('median_speed')) {
      context.handle(
          _medianSpeedMeta,
          medianSpeed.isAcceptableOrUnknown(
              data['median_speed']!, _medianSpeedMeta));
    } else if (isInserting) {
      context.missing(_medianSpeedMeta);
    }
    if (data.containsKey('distance')) {
      context.handle(_distanceMeta,
          distance.isAcceptableOrUnknown(data['distance']!, _distanceMeta));
    } else if (isInserting) {
      context.missing(_distanceMeta);
    }
    if (data.containsKey('sensory_type')) {
      context.handle(
          _sensoryTypeMeta,
          sensoryType.isAcceptableOrUnknown(
              data['sensory_type']!, _sensoryTypeMeta));
    } else if (isInserting) {
      context.missing(_sensoryTypeMeta);
    }
    if (data.containsKey('cluster_id')) {
      context.handle(_clusterIdMeta,
          clusterId.isAcceptableOrUnknown(data['cluster_id']!, _clusterIdMeta));
    }
    if (data.containsKey('classified_period_uuid')) {
      context.handle(
          _classifiedPeriodUuidMeta,
          classifiedPeriodUuid.isAcceptableOrUnknown(
              data['classified_period_uuid']!, _classifiedPeriodUuidMeta));
    }
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id']!, _userIdMeta));
    }
    if (data.containsKey('provider')) {
      context.handle(_providerMeta,
          provider.isAcceptableOrUnknown(data['provider']!, _providerMeta));
    } else if (isInserting) {
      context.missing(_providerMeta);
    }
    if (data.containsKey('is_noise')) {
      context.handle(_isNoiseMeta,
          isNoise.isAcceptableOrUnknown(data['is_noise']!, _isNoiseMeta));
    } else if (isInserting) {
      context.missing(_isNoiseMeta);
    }
    if (data.containsKey('battery_level')) {
      context.handle(
          _batteryLevelMeta,
          batteryLevel.isAcceptableOrUnknown(
              data['battery_level']!, _batteryLevelMeta));
    } else if (isInserting) {
      context.missing(_batteryLevelMeta);
    }
    if (data.containsKey('created_on')) {
      context.handle(_createdOnMeta,
          createdOn.isAcceptableOrUnknown(data['created_on']!, _createdOnMeta));
    } else if (isInserting) {
      context.missing(_createdOnMeta);
    }
    if (data.containsKey('deleted_on')) {
      context.handle(_deletedOnMeta,
          deletedOn.isAcceptableOrUnknown(data['deleted_on']!, _deletedOnMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  SensorGeolocation map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SensorGeolocation(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      latitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}latitude'])!,
      longitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}longitude'])!,
      altitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}altitude'])!,
      bearing: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}bearing'])!,
      accuracy: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}accuracy'])!,
      speed: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}speed'])!,
      calculatedSpeed: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}calculated_speed'])!,
      medianSpeed: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}median_speed'])!,
      distance: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}distance'])!,
      sensoryType: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}sensory_type'])!,
      clusterId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}cluster_id']),
      classifiedPeriodUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}classified_period_uuid']),
      userId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}user_id'])!,
      provider: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}provider'])!,
      isNoise: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}is_noise'])!,
      batteryLevel: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}battery_level'])!,
      createdOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}created_on'])!,
      deletedOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}deleted_on']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced']),
    );
  }

  @override
  $SensorGeolocationsTable createAlias(String alias) {
    return $SensorGeolocationsTable(attachedDatabase, alias);
  }
}

class SensorGeolocation extends DataClass
    implements Insertable<SensorGeolocation> {
  final String uuid;
  final double latitude;
  final double longitude;
  final double altitude;
  final double bearing;
  final double accuracy;
  final double speed;
  final double calculatedSpeed;
  final double medianSpeed;
  final double distance;
  final String sensoryType;
  final String? clusterId;
  final String? classifiedPeriodUuid;
  final String userId;
  final String provider;
  final bool isNoise;
  final int batteryLevel;
  final DateTime createdOn;
  final DateTime? deletedOn;
  final bool? synced;
  const SensorGeolocation(
      {required this.uuid,
      required this.latitude,
      required this.longitude,
      required this.altitude,
      required this.bearing,
      required this.accuracy,
      required this.speed,
      required this.calculatedSpeed,
      required this.medianSpeed,
      required this.distance,
      required this.sensoryType,
      this.clusterId,
      this.classifiedPeriodUuid,
      required this.userId,
      required this.provider,
      required this.isNoise,
      required this.batteryLevel,
      required this.createdOn,
      this.deletedOn,
      this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['latitude'] = Variable<double>(latitude);
    map['longitude'] = Variable<double>(longitude);
    map['altitude'] = Variable<double>(altitude);
    map['bearing'] = Variable<double>(bearing);
    map['accuracy'] = Variable<double>(accuracy);
    map['speed'] = Variable<double>(speed);
    map['calculated_speed'] = Variable<double>(calculatedSpeed);
    map['median_speed'] = Variable<double>(medianSpeed);
    map['distance'] = Variable<double>(distance);
    map['sensory_type'] = Variable<String>(sensoryType);
    if (!nullToAbsent || clusterId != null) {
      map['cluster_id'] = Variable<String>(clusterId);
    }
    if (!nullToAbsent || classifiedPeriodUuid != null) {
      map['classified_period_uuid'] = Variable<String>(classifiedPeriodUuid);
    }
    map['user_id'] = Variable<String>(userId);
    map['provider'] = Variable<String>(provider);
    map['is_noise'] = Variable<bool>(isNoise);
    map['battery_level'] = Variable<int>(batteryLevel);
    map['created_on'] = Variable<DateTime>(createdOn);
    if (!nullToAbsent || deletedOn != null) {
      map['deleted_on'] = Variable<DateTime>(deletedOn);
    }
    if (!nullToAbsent || synced != null) {
      map['synced'] = Variable<bool>(synced);
    }
    return map;
  }

  SensorGeolocationsCompanion toCompanion(bool nullToAbsent) {
    return SensorGeolocationsCompanion(
      uuid: Value(uuid),
      latitude: Value(latitude),
      longitude: Value(longitude),
      altitude: Value(altitude),
      bearing: Value(bearing),
      accuracy: Value(accuracy),
      speed: Value(speed),
      calculatedSpeed: Value(calculatedSpeed),
      medianSpeed: Value(medianSpeed),
      distance: Value(distance),
      sensoryType: Value(sensoryType),
      clusterId: clusterId == null && nullToAbsent
          ? const Value.absent()
          : Value(clusterId),
      classifiedPeriodUuid: classifiedPeriodUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(classifiedPeriodUuid),
      userId: Value(userId),
      provider: Value(provider),
      isNoise: Value(isNoise),
      batteryLevel: Value(batteryLevel),
      createdOn: Value(createdOn),
      deletedOn: deletedOn == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedOn),
      synced:
          synced == null && nullToAbsent ? const Value.absent() : Value(synced),
    );
  }

  factory SensorGeolocation.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SensorGeolocation(
      uuid: serializer.fromJson<String>(json['uuid']),
      latitude: serializer.fromJson<double>(json['latitude']),
      longitude: serializer.fromJson<double>(json['longitude']),
      altitude: serializer.fromJson<double>(json['altitude']),
      bearing: serializer.fromJson<double>(json['bearing']),
      accuracy: serializer.fromJson<double>(json['accuracy']),
      speed: serializer.fromJson<double>(json['speed']),
      calculatedSpeed: serializer.fromJson<double>(json['calculatedSpeed']),
      medianSpeed: serializer.fromJson<double>(json['medianSpeed']),
      distance: serializer.fromJson<double>(json['distance']),
      sensoryType: serializer.fromJson<String>(json['sensoryType']),
      clusterId: serializer.fromJson<String?>(json['clusterId']),
      classifiedPeriodUuid:
          serializer.fromJson<String?>(json['classifiedPeriodUuid']),
      userId: serializer.fromJson<String>(json['userId']),
      provider: serializer.fromJson<String>(json['provider']),
      isNoise: serializer.fromJson<bool>(json['isNoise']),
      batteryLevel: serializer.fromJson<int>(json['batteryLevel']),
      createdOn: serializer.fromJson<DateTime>(json['createdOn']),
      deletedOn: serializer.fromJson<DateTime?>(json['deletedOn']),
      synced: serializer.fromJson<bool?>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'latitude': serializer.toJson<double>(latitude),
      'longitude': serializer.toJson<double>(longitude),
      'altitude': serializer.toJson<double>(altitude),
      'bearing': serializer.toJson<double>(bearing),
      'accuracy': serializer.toJson<double>(accuracy),
      'speed': serializer.toJson<double>(speed),
      'calculatedSpeed': serializer.toJson<double>(calculatedSpeed),
      'medianSpeed': serializer.toJson<double>(medianSpeed),
      'distance': serializer.toJson<double>(distance),
      'sensoryType': serializer.toJson<String>(sensoryType),
      'clusterId': serializer.toJson<String?>(clusterId),
      'classifiedPeriodUuid': serializer.toJson<String?>(classifiedPeriodUuid),
      'userId': serializer.toJson<String>(userId),
      'provider': serializer.toJson<String>(provider),
      'isNoise': serializer.toJson<bool>(isNoise),
      'batteryLevel': serializer.toJson<int>(batteryLevel),
      'createdOn': serializer.toJson<DateTime>(createdOn),
      'deletedOn': serializer.toJson<DateTime?>(deletedOn),
      'synced': serializer.toJson<bool?>(synced),
    };
  }

  SensorGeolocation copyWith(
          {String? uuid,
          double? latitude,
          double? longitude,
          double? altitude,
          double? bearing,
          double? accuracy,
          double? speed,
          double? calculatedSpeed,
          double? medianSpeed,
          double? distance,
          String? sensoryType,
          Value<String?> clusterId = const Value.absent(),
          Value<String?> classifiedPeriodUuid = const Value.absent(),
          String? userId,
          String? provider,
          bool? isNoise,
          int? batteryLevel,
          DateTime? createdOn,
          Value<DateTime?> deletedOn = const Value.absent(),
          Value<bool?> synced = const Value.absent()}) =>
      SensorGeolocation(
        uuid: uuid ?? this.uuid,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
        altitude: altitude ?? this.altitude,
        bearing: bearing ?? this.bearing,
        accuracy: accuracy ?? this.accuracy,
        speed: speed ?? this.speed,
        calculatedSpeed: calculatedSpeed ?? this.calculatedSpeed,
        medianSpeed: medianSpeed ?? this.medianSpeed,
        distance: distance ?? this.distance,
        sensoryType: sensoryType ?? this.sensoryType,
        clusterId: clusterId.present ? clusterId.value : this.clusterId,
        classifiedPeriodUuid: classifiedPeriodUuid.present
            ? classifiedPeriodUuid.value
            : this.classifiedPeriodUuid,
        userId: userId ?? this.userId,
        provider: provider ?? this.provider,
        isNoise: isNoise ?? this.isNoise,
        batteryLevel: batteryLevel ?? this.batteryLevel,
        createdOn: createdOn ?? this.createdOn,
        deletedOn: deletedOn.present ? deletedOn.value : this.deletedOn,
        synced: synced.present ? synced.value : this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('SensorGeolocation(')
          ..write('uuid: $uuid, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude, ')
          ..write('altitude: $altitude, ')
          ..write('bearing: $bearing, ')
          ..write('accuracy: $accuracy, ')
          ..write('speed: $speed, ')
          ..write('calculatedSpeed: $calculatedSpeed, ')
          ..write('medianSpeed: $medianSpeed, ')
          ..write('distance: $distance, ')
          ..write('sensoryType: $sensoryType, ')
          ..write('clusterId: $clusterId, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('userId: $userId, ')
          ..write('provider: $provider, ')
          ..write('isNoise: $isNoise, ')
          ..write('batteryLevel: $batteryLevel, ')
          ..write('createdOn: $createdOn, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      uuid,
      latitude,
      longitude,
      altitude,
      bearing,
      accuracy,
      speed,
      calculatedSpeed,
      medianSpeed,
      distance,
      sensoryType,
      clusterId,
      classifiedPeriodUuid,
      userId,
      provider,
      isNoise,
      batteryLevel,
      createdOn,
      deletedOn,
      synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SensorGeolocation &&
          other.uuid == this.uuid &&
          other.latitude == this.latitude &&
          other.longitude == this.longitude &&
          other.altitude == this.altitude &&
          other.bearing == this.bearing &&
          other.accuracy == this.accuracy &&
          other.speed == this.speed &&
          other.calculatedSpeed == this.calculatedSpeed &&
          other.medianSpeed == this.medianSpeed &&
          other.distance == this.distance &&
          other.sensoryType == this.sensoryType &&
          other.clusterId == this.clusterId &&
          other.classifiedPeriodUuid == this.classifiedPeriodUuid &&
          other.userId == this.userId &&
          other.provider == this.provider &&
          other.isNoise == this.isNoise &&
          other.batteryLevel == this.batteryLevel &&
          other.createdOn == this.createdOn &&
          other.deletedOn == this.deletedOn &&
          other.synced == this.synced);
}

class SensorGeolocationsCompanion extends UpdateCompanion<SensorGeolocation> {
  final Value<String> uuid;
  final Value<double> latitude;
  final Value<double> longitude;
  final Value<double> altitude;
  final Value<double> bearing;
  final Value<double> accuracy;
  final Value<double> speed;
  final Value<double> calculatedSpeed;
  final Value<double> medianSpeed;
  final Value<double> distance;
  final Value<String> sensoryType;
  final Value<String?> clusterId;
  final Value<String?> classifiedPeriodUuid;
  final Value<String> userId;
  final Value<String> provider;
  final Value<bool> isNoise;
  final Value<int> batteryLevel;
  final Value<DateTime> createdOn;
  final Value<DateTime?> deletedOn;
  final Value<bool?> synced;
  final Value<int> rowid;
  const SensorGeolocationsCompanion({
    this.uuid = const Value.absent(),
    this.latitude = const Value.absent(),
    this.longitude = const Value.absent(),
    this.altitude = const Value.absent(),
    this.bearing = const Value.absent(),
    this.accuracy = const Value.absent(),
    this.speed = const Value.absent(),
    this.calculatedSpeed = const Value.absent(),
    this.medianSpeed = const Value.absent(),
    this.distance = const Value.absent(),
    this.sensoryType = const Value.absent(),
    this.clusterId = const Value.absent(),
    this.classifiedPeriodUuid = const Value.absent(),
    this.userId = const Value.absent(),
    this.provider = const Value.absent(),
    this.isNoise = const Value.absent(),
    this.batteryLevel = const Value.absent(),
    this.createdOn = const Value.absent(),
    this.deletedOn = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  SensorGeolocationsCompanion.insert({
    this.uuid = const Value.absent(),
    required double latitude,
    required double longitude,
    required double altitude,
    required double bearing,
    required double accuracy,
    required double speed,
    required double calculatedSpeed,
    required double medianSpeed,
    required double distance,
    required String sensoryType,
    this.clusterId = const Value.absent(),
    this.classifiedPeriodUuid = const Value.absent(),
    this.userId = const Value.absent(),
    required String provider,
    required bool isNoise,
    required int batteryLevel,
    required DateTime createdOn,
    this.deletedOn = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : latitude = Value(latitude),
        longitude = Value(longitude),
        altitude = Value(altitude),
        bearing = Value(bearing),
        accuracy = Value(accuracy),
        speed = Value(speed),
        calculatedSpeed = Value(calculatedSpeed),
        medianSpeed = Value(medianSpeed),
        distance = Value(distance),
        sensoryType = Value(sensoryType),
        provider = Value(provider),
        isNoise = Value(isNoise),
        batteryLevel = Value(batteryLevel),
        createdOn = Value(createdOn);
  static Insertable<SensorGeolocation> custom({
    Expression<String>? uuid,
    Expression<double>? latitude,
    Expression<double>? longitude,
    Expression<double>? altitude,
    Expression<double>? bearing,
    Expression<double>? accuracy,
    Expression<double>? speed,
    Expression<double>? calculatedSpeed,
    Expression<double>? medianSpeed,
    Expression<double>? distance,
    Expression<String>? sensoryType,
    Expression<String>? clusterId,
    Expression<String>? classifiedPeriodUuid,
    Expression<String>? userId,
    Expression<String>? provider,
    Expression<bool>? isNoise,
    Expression<int>? batteryLevel,
    Expression<DateTime>? createdOn,
    Expression<DateTime>? deletedOn,
    Expression<bool>? synced,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (latitude != null) 'latitude': latitude,
      if (longitude != null) 'longitude': longitude,
      if (altitude != null) 'altitude': altitude,
      if (bearing != null) 'bearing': bearing,
      if (accuracy != null) 'accuracy': accuracy,
      if (speed != null) 'speed': speed,
      if (calculatedSpeed != null) 'calculated_speed': calculatedSpeed,
      if (medianSpeed != null) 'median_speed': medianSpeed,
      if (distance != null) 'distance': distance,
      if (sensoryType != null) 'sensory_type': sensoryType,
      if (clusterId != null) 'cluster_id': clusterId,
      if (classifiedPeriodUuid != null)
        'classified_period_uuid': classifiedPeriodUuid,
      if (userId != null) 'user_id': userId,
      if (provider != null) 'provider': provider,
      if (isNoise != null) 'is_noise': isNoise,
      if (batteryLevel != null) 'battery_level': batteryLevel,
      if (createdOn != null) 'created_on': createdOn,
      if (deletedOn != null) 'deleted_on': deletedOn,
      if (synced != null) 'synced': synced,
      if (rowid != null) 'rowid': rowid,
    });
  }

  SensorGeolocationsCompanion copyWith(
      {Value<String>? uuid,
      Value<double>? latitude,
      Value<double>? longitude,
      Value<double>? altitude,
      Value<double>? bearing,
      Value<double>? accuracy,
      Value<double>? speed,
      Value<double>? calculatedSpeed,
      Value<double>? medianSpeed,
      Value<double>? distance,
      Value<String>? sensoryType,
      Value<String?>? clusterId,
      Value<String?>? classifiedPeriodUuid,
      Value<String>? userId,
      Value<String>? provider,
      Value<bool>? isNoise,
      Value<int>? batteryLevel,
      Value<DateTime>? createdOn,
      Value<DateTime?>? deletedOn,
      Value<bool?>? synced,
      Value<int>? rowid}) {
    return SensorGeolocationsCompanion(
      uuid: uuid ?? this.uuid,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      altitude: altitude ?? this.altitude,
      bearing: bearing ?? this.bearing,
      accuracy: accuracy ?? this.accuracy,
      speed: speed ?? this.speed,
      calculatedSpeed: calculatedSpeed ?? this.calculatedSpeed,
      medianSpeed: medianSpeed ?? this.medianSpeed,
      distance: distance ?? this.distance,
      sensoryType: sensoryType ?? this.sensoryType,
      clusterId: clusterId ?? this.clusterId,
      classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
      userId: userId ?? this.userId,
      provider: provider ?? this.provider,
      isNoise: isNoise ?? this.isNoise,
      batteryLevel: batteryLevel ?? this.batteryLevel,
      createdOn: createdOn ?? this.createdOn,
      deletedOn: deletedOn ?? this.deletedOn,
      synced: synced ?? this.synced,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (latitude.present) {
      map['latitude'] = Variable<double>(latitude.value);
    }
    if (longitude.present) {
      map['longitude'] = Variable<double>(longitude.value);
    }
    if (altitude.present) {
      map['altitude'] = Variable<double>(altitude.value);
    }
    if (bearing.present) {
      map['bearing'] = Variable<double>(bearing.value);
    }
    if (accuracy.present) {
      map['accuracy'] = Variable<double>(accuracy.value);
    }
    if (speed.present) {
      map['speed'] = Variable<double>(speed.value);
    }
    if (calculatedSpeed.present) {
      map['calculated_speed'] = Variable<double>(calculatedSpeed.value);
    }
    if (medianSpeed.present) {
      map['median_speed'] = Variable<double>(medianSpeed.value);
    }
    if (distance.present) {
      map['distance'] = Variable<double>(distance.value);
    }
    if (sensoryType.present) {
      map['sensory_type'] = Variable<String>(sensoryType.value);
    }
    if (clusterId.present) {
      map['cluster_id'] = Variable<String>(clusterId.value);
    }
    if (classifiedPeriodUuid.present) {
      map['classified_period_uuid'] =
          Variable<String>(classifiedPeriodUuid.value);
    }
    if (userId.present) {
      map['user_id'] = Variable<String>(userId.value);
    }
    if (provider.present) {
      map['provider'] = Variable<String>(provider.value);
    }
    if (isNoise.present) {
      map['is_noise'] = Variable<bool>(isNoise.value);
    }
    if (batteryLevel.present) {
      map['battery_level'] = Variable<int>(batteryLevel.value);
    }
    if (createdOn.present) {
      map['created_on'] = Variable<DateTime>(createdOn.value);
    }
    if (deletedOn.present) {
      map['deleted_on'] = Variable<DateTime>(deletedOn.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SensorGeolocationsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude, ')
          ..write('altitude: $altitude, ')
          ..write('bearing: $bearing, ')
          ..write('accuracy: $accuracy, ')
          ..write('speed: $speed, ')
          ..write('calculatedSpeed: $calculatedSpeed, ')
          ..write('medianSpeed: $medianSpeed, ')
          ..write('distance: $distance, ')
          ..write('sensoryType: $sensoryType, ')
          ..write('clusterId: $clusterId, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('userId: $userId, ')
          ..write('provider: $provider, ')
          ..write('isNoise: $isNoise, ')
          ..write('batteryLevel: $batteryLevel, ')
          ..write('createdOn: $createdOn, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('synced: $synced, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $ManualGeolocationsTable extends ManualGeolocations
    with TableInfo<$ManualGeolocationsTable, ManualGeolocation> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ManualGeolocationsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _classifiedPeriodUuidMeta =
      const VerificationMeta('classifiedPeriodUuid');
  @override
  late final GeneratedColumn<String> classifiedPeriodUuid =
      GeneratedColumn<String>(
          'classified_period_uuid', aliasedName, false,
          type: DriftSqlType.string,
          requiredDuringInsert: true,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES classified_periods (uuid)'));
  static const VerificationMeta _latitudeMeta =
      const VerificationMeta('latitude');
  @override
  late final GeneratedColumn<double> latitude = GeneratedColumn<double>(
      'latitude', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _longitudeMeta =
      const VerificationMeta('longitude');
  @override
  late final GeneratedColumn<double> longitude = GeneratedColumn<double>(
      'longitude', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _createdOnMeta =
      const VerificationMeta('createdOn');
  @override
  late final GeneratedColumn<DateTime> createdOn = GeneratedColumn<DateTime>(
      'created_on', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _deletedOnMeta =
      const VerificationMeta('deletedOn');
  @override
  late final GeneratedColumn<DateTime> deletedOn = GeneratedColumn<DateTime>(
      'deleted_on', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        classifiedPeriodUuid,
        latitude,
        longitude,
        createdOn,
        deletedOn,
        synced
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'manual_geolocations';
  @override
  VerificationContext validateIntegrity(Insertable<ManualGeolocation> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('classified_period_uuid')) {
      context.handle(
          _classifiedPeriodUuidMeta,
          classifiedPeriodUuid.isAcceptableOrUnknown(
              data['classified_period_uuid']!, _classifiedPeriodUuidMeta));
    } else if (isInserting) {
      context.missing(_classifiedPeriodUuidMeta);
    }
    if (data.containsKey('latitude')) {
      context.handle(_latitudeMeta,
          latitude.isAcceptableOrUnknown(data['latitude']!, _latitudeMeta));
    } else if (isInserting) {
      context.missing(_latitudeMeta);
    }
    if (data.containsKey('longitude')) {
      context.handle(_longitudeMeta,
          longitude.isAcceptableOrUnknown(data['longitude']!, _longitudeMeta));
    } else if (isInserting) {
      context.missing(_longitudeMeta);
    }
    if (data.containsKey('created_on')) {
      context.handle(_createdOnMeta,
          createdOn.isAcceptableOrUnknown(data['created_on']!, _createdOnMeta));
    } else if (isInserting) {
      context.missing(_createdOnMeta);
    }
    if (data.containsKey('deleted_on')) {
      context.handle(_deletedOnMeta,
          deletedOn.isAcceptableOrUnknown(data['deleted_on']!, _deletedOnMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  ManualGeolocation map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ManualGeolocation(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      classifiedPeriodUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}classified_period_uuid'])!,
      latitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}latitude'])!,
      longitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}longitude'])!,
      createdOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}created_on'])!,
      deletedOn: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}deleted_on']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced']),
    );
  }

  @override
  $ManualGeolocationsTable createAlias(String alias) {
    return $ManualGeolocationsTable(attachedDatabase, alias);
  }
}

class ManualGeolocation extends DataClass
    implements Insertable<ManualGeolocation> {
  final String uuid;
  final String classifiedPeriodUuid;
  final double latitude;
  final double longitude;
  final DateTime createdOn;
  final DateTime? deletedOn;
  final bool? synced;
  const ManualGeolocation(
      {required this.uuid,
      required this.classifiedPeriodUuid,
      required this.latitude,
      required this.longitude,
      required this.createdOn,
      this.deletedOn,
      this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['classified_period_uuid'] = Variable<String>(classifiedPeriodUuid);
    map['latitude'] = Variable<double>(latitude);
    map['longitude'] = Variable<double>(longitude);
    map['created_on'] = Variable<DateTime>(createdOn);
    if (!nullToAbsent || deletedOn != null) {
      map['deleted_on'] = Variable<DateTime>(deletedOn);
    }
    if (!nullToAbsent || synced != null) {
      map['synced'] = Variable<bool>(synced);
    }
    return map;
  }

  ManualGeolocationsCompanion toCompanion(bool nullToAbsent) {
    return ManualGeolocationsCompanion(
      uuid: Value(uuid),
      classifiedPeriodUuid: Value(classifiedPeriodUuid),
      latitude: Value(latitude),
      longitude: Value(longitude),
      createdOn: Value(createdOn),
      deletedOn: deletedOn == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedOn),
      synced:
          synced == null && nullToAbsent ? const Value.absent() : Value(synced),
    );
  }

  factory ManualGeolocation.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ManualGeolocation(
      uuid: serializer.fromJson<String>(json['uuid']),
      classifiedPeriodUuid:
          serializer.fromJson<String>(json['classifiedPeriodUuid']),
      latitude: serializer.fromJson<double>(json['latitude']),
      longitude: serializer.fromJson<double>(json['longitude']),
      createdOn: serializer.fromJson<DateTime>(json['createdOn']),
      deletedOn: serializer.fromJson<DateTime?>(json['deletedOn']),
      synced: serializer.fromJson<bool?>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'classifiedPeriodUuid': serializer.toJson<String>(classifiedPeriodUuid),
      'latitude': serializer.toJson<double>(latitude),
      'longitude': serializer.toJson<double>(longitude),
      'createdOn': serializer.toJson<DateTime>(createdOn),
      'deletedOn': serializer.toJson<DateTime?>(deletedOn),
      'synced': serializer.toJson<bool?>(synced),
    };
  }

  ManualGeolocation copyWith(
          {String? uuid,
          String? classifiedPeriodUuid,
          double? latitude,
          double? longitude,
          DateTime? createdOn,
          Value<DateTime?> deletedOn = const Value.absent(),
          Value<bool?> synced = const Value.absent()}) =>
      ManualGeolocation(
        uuid: uuid ?? this.uuid,
        classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
        createdOn: createdOn ?? this.createdOn,
        deletedOn: deletedOn.present ? deletedOn.value : this.deletedOn,
        synced: synced.present ? synced.value : this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('ManualGeolocation(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude, ')
          ..write('createdOn: $createdOn, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, classifiedPeriodUuid, latitude,
      longitude, createdOn, deletedOn, synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ManualGeolocation &&
          other.uuid == this.uuid &&
          other.classifiedPeriodUuid == this.classifiedPeriodUuid &&
          other.latitude == this.latitude &&
          other.longitude == this.longitude &&
          other.createdOn == this.createdOn &&
          other.deletedOn == this.deletedOn &&
          other.synced == this.synced);
}

class ManualGeolocationsCompanion extends UpdateCompanion<ManualGeolocation> {
  final Value<String> uuid;
  final Value<String> classifiedPeriodUuid;
  final Value<double> latitude;
  final Value<double> longitude;
  final Value<DateTime> createdOn;
  final Value<DateTime?> deletedOn;
  final Value<bool?> synced;
  final Value<int> rowid;
  const ManualGeolocationsCompanion({
    this.uuid = const Value.absent(),
    this.classifiedPeriodUuid = const Value.absent(),
    this.latitude = const Value.absent(),
    this.longitude = const Value.absent(),
    this.createdOn = const Value.absent(),
    this.deletedOn = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ManualGeolocationsCompanion.insert({
    this.uuid = const Value.absent(),
    required String classifiedPeriodUuid,
    required double latitude,
    required double longitude,
    required DateTime createdOn,
    this.deletedOn = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : classifiedPeriodUuid = Value(classifiedPeriodUuid),
        latitude = Value(latitude),
        longitude = Value(longitude),
        createdOn = Value(createdOn);
  static Insertable<ManualGeolocation> custom({
    Expression<String>? uuid,
    Expression<String>? classifiedPeriodUuid,
    Expression<double>? latitude,
    Expression<double>? longitude,
    Expression<DateTime>? createdOn,
    Expression<DateTime>? deletedOn,
    Expression<bool>? synced,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (classifiedPeriodUuid != null)
        'classified_period_uuid': classifiedPeriodUuid,
      if (latitude != null) 'latitude': latitude,
      if (longitude != null) 'longitude': longitude,
      if (createdOn != null) 'created_on': createdOn,
      if (deletedOn != null) 'deleted_on': deletedOn,
      if (synced != null) 'synced': synced,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ManualGeolocationsCompanion copyWith(
      {Value<String>? uuid,
      Value<String>? classifiedPeriodUuid,
      Value<double>? latitude,
      Value<double>? longitude,
      Value<DateTime>? createdOn,
      Value<DateTime?>? deletedOn,
      Value<bool?>? synced,
      Value<int>? rowid}) {
    return ManualGeolocationsCompanion(
      uuid: uuid ?? this.uuid,
      classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      createdOn: createdOn ?? this.createdOn,
      deletedOn: deletedOn ?? this.deletedOn,
      synced: synced ?? this.synced,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (classifiedPeriodUuid.present) {
      map['classified_period_uuid'] =
          Variable<String>(classifiedPeriodUuid.value);
    }
    if (latitude.present) {
      map['latitude'] = Variable<double>(latitude.value);
    }
    if (longitude.present) {
      map['longitude'] = Variable<double>(longitude.value);
    }
    if (createdOn.present) {
      map['created_on'] = Variable<DateTime>(createdOn.value);
    }
    if (deletedOn.present) {
      map['deleted_on'] = Variable<DateTime>(deletedOn.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ManualGeolocationsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude, ')
          ..write('createdOn: $createdOn, ')
          ..write('deletedOn: $deletedOn, ')
          ..write('synced: $synced, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $LogsTable extends Logs with TableInfo<$LogsTable, Log> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $LogsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, true,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _messageMeta =
      const VerificationMeta('message');
  @override
  late final GeneratedColumn<String> message = GeneratedColumn<String>(
      'message', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String> type = GeneratedColumn<String>(
      'type', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _dateMeta = const VerificationMeta('date');
  @override
  late final GeneratedColumn<DateTime> date = GeneratedColumn<DateTime>(
      'date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns =>
      [id, message, description, type, date, synced];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'logs';
  @override
  VerificationContext validateIntegrity(Insertable<Log> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('message')) {
      context.handle(_messageMeta,
          message.isAcceptableOrUnknown(data['message']!, _messageMeta));
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    }
    if (data.containsKey('date')) {
      context.handle(
          _dateMeta, date.isAcceptableOrUnknown(data['date']!, _dateMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Log map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Log(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id']),
      message: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}message']),
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description']),
      type: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type']),
      date: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}date']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced']),
    );
  }

  @override
  $LogsTable createAlias(String alias) {
    return $LogsTable(attachedDatabase, alias);
  }
}

class Log extends DataClass implements Insertable<Log> {
  final int? id;
  final String? message;
  final String? description;
  final String? type;
  final DateTime? date;
  final bool? synced;
  const Log(
      {this.id,
      this.message,
      this.description,
      this.type,
      this.date,
      this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || message != null) {
      map['message'] = Variable<String>(message);
    }
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String>(description);
    }
    if (!nullToAbsent || type != null) {
      map['type'] = Variable<String>(type);
    }
    if (!nullToAbsent || date != null) {
      map['date'] = Variable<DateTime>(date);
    }
    if (!nullToAbsent || synced != null) {
      map['synced'] = Variable<bool>(synced);
    }
    return map;
  }

  LogsCompanion toCompanion(bool nullToAbsent) {
    return LogsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      message: message == null && nullToAbsent
          ? const Value.absent()
          : Value(message),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      type: type == null && nullToAbsent ? const Value.absent() : Value(type),
      date: date == null && nullToAbsent ? const Value.absent() : Value(date),
      synced:
          synced == null && nullToAbsent ? const Value.absent() : Value(synced),
    );
  }

  factory Log.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Log(
      id: serializer.fromJson<int?>(json['id']),
      message: serializer.fromJson<String?>(json['message']),
      description: serializer.fromJson<String?>(json['description']),
      type: serializer.fromJson<String?>(json['type']),
      date: serializer.fromJson<DateTime?>(json['date']),
      synced: serializer.fromJson<bool?>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int?>(id),
      'message': serializer.toJson<String?>(message),
      'description': serializer.toJson<String?>(description),
      'type': serializer.toJson<String?>(type),
      'date': serializer.toJson<DateTime?>(date),
      'synced': serializer.toJson<bool?>(synced),
    };
  }

  Log copyWith(
          {Value<int?> id = const Value.absent(),
          Value<String?> message = const Value.absent(),
          Value<String?> description = const Value.absent(),
          Value<String?> type = const Value.absent(),
          Value<DateTime?> date = const Value.absent(),
          Value<bool?> synced = const Value.absent()}) =>
      Log(
        id: id.present ? id.value : this.id,
        message: message.present ? message.value : this.message,
        description: description.present ? description.value : this.description,
        type: type.present ? type.value : this.type,
        date: date.present ? date.value : this.date,
        synced: synced.present ? synced.value : this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('Log(')
          ..write('id: $id, ')
          ..write('message: $message, ')
          ..write('description: $description, ')
          ..write('type: $type, ')
          ..write('date: $date, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, message, description, type, date, synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Log &&
          other.id == this.id &&
          other.message == this.message &&
          other.description == this.description &&
          other.type == this.type &&
          other.date == this.date &&
          other.synced == this.synced);
}

class LogsCompanion extends UpdateCompanion<Log> {
  final Value<int?> id;
  final Value<String?> message;
  final Value<String?> description;
  final Value<String?> type;
  final Value<DateTime?> date;
  final Value<bool?> synced;
  const LogsCompanion({
    this.id = const Value.absent(),
    this.message = const Value.absent(),
    this.description = const Value.absent(),
    this.type = const Value.absent(),
    this.date = const Value.absent(),
    this.synced = const Value.absent(),
  });
  LogsCompanion.insert({
    this.id = const Value.absent(),
    this.message = const Value.absent(),
    this.description = const Value.absent(),
    this.type = const Value.absent(),
    this.date = const Value.absent(),
    this.synced = const Value.absent(),
  });
  static Insertable<Log> custom({
    Expression<int>? id,
    Expression<String>? message,
    Expression<String>? description,
    Expression<String>? type,
    Expression<DateTime>? date,
    Expression<bool>? synced,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (message != null) 'message': message,
      if (description != null) 'description': description,
      if (type != null) 'type': type,
      if (date != null) 'date': date,
      if (synced != null) 'synced': synced,
    });
  }

  LogsCompanion copyWith(
      {Value<int?>? id,
      Value<String?>? message,
      Value<String?>? description,
      Value<String?>? type,
      Value<DateTime?>? date,
      Value<bool?>? synced}) {
    return LogsCompanion(
      id: id ?? this.id,
      message: message ?? this.message,
      description: description ?? this.description,
      type: type ?? this.type,
      date: date ?? this.date,
      synced: synced ?? this.synced,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (message.present) {
      map['message'] = Variable<String>(message.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (date.present) {
      map['date'] = Variable<DateTime>(date.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LogsCompanion(')
          ..write('id: $id, ')
          ..write('message: $message, ')
          ..write('description: $description, ')
          ..write('type: $type, ')
          ..write('date: $date, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }
}

class $ReasonsTable extends Reasons with TableInfo<$ReasonsTable, Reason> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ReasonsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, true,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _iconMeta = const VerificationMeta('icon');
  @override
  late final GeneratedColumn<String> icon = GeneratedColumn<String>(
      'icon', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _colorMeta = const VerificationMeta('color');
  @override
  late final GeneratedColumn<String> color = GeneratedColumn<String>(
      'color', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [id, name, icon, color];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'reasons';
  @override
  VerificationContext validateIntegrity(Insertable<Reason> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    }
    if (data.containsKey('icon')) {
      context.handle(
          _iconMeta, icon.isAcceptableOrUnknown(data['icon']!, _iconMeta));
    }
    if (data.containsKey('color')) {
      context.handle(
          _colorMeta, color.isAcceptableOrUnknown(data['color']!, _colorMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Reason map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Reason(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id']),
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name']),
      icon: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}icon']),
      color: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}color']),
    );
  }

  @override
  $ReasonsTable createAlias(String alias) {
    return $ReasonsTable(attachedDatabase, alias);
  }
}

class Reason extends DataClass implements Insertable<Reason> {
  final int? id;
  final String? name;
  final String? icon;
  final String? color;
  const Reason({this.id, this.name, this.icon, this.color});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || icon != null) {
      map['icon'] = Variable<String>(icon);
    }
    if (!nullToAbsent || color != null) {
      map['color'] = Variable<String>(color);
    }
    return map;
  }

  ReasonsCompanion toCompanion(bool nullToAbsent) {
    return ReasonsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      icon: icon == null && nullToAbsent ? const Value.absent() : Value(icon),
      color:
          color == null && nullToAbsent ? const Value.absent() : Value(color),
    );
  }

  factory Reason.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Reason(
      id: serializer.fromJson<int?>(json['id']),
      name: serializer.fromJson<String?>(json['name']),
      icon: serializer.fromJson<String?>(json['icon']),
      color: serializer.fromJson<String?>(json['color']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int?>(id),
      'name': serializer.toJson<String?>(name),
      'icon': serializer.toJson<String?>(icon),
      'color': serializer.toJson<String?>(color),
    };
  }

  Reason copyWith(
          {Value<int?> id = const Value.absent(),
          Value<String?> name = const Value.absent(),
          Value<String?> icon = const Value.absent(),
          Value<String?> color = const Value.absent()}) =>
      Reason(
        id: id.present ? id.value : this.id,
        name: name.present ? name.value : this.name,
        icon: icon.present ? icon.value : this.icon,
        color: color.present ? color.value : this.color,
      );
  @override
  String toString() {
    return (StringBuffer('Reason(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('icon: $icon, ')
          ..write('color: $color')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, icon, color);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Reason &&
          other.id == this.id &&
          other.name == this.name &&
          other.icon == this.icon &&
          other.color == this.color);
}

class ReasonsCompanion extends UpdateCompanion<Reason> {
  final Value<int?> id;
  final Value<String?> name;
  final Value<String?> icon;
  final Value<String?> color;
  const ReasonsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.icon = const Value.absent(),
    this.color = const Value.absent(),
  });
  ReasonsCompanion.insert({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.icon = const Value.absent(),
    this.color = const Value.absent(),
  });
  static Insertable<Reason> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<String>? icon,
    Expression<String>? color,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (icon != null) 'icon': icon,
      if (color != null) 'color': color,
    });
  }

  ReasonsCompanion copyWith(
      {Value<int?>? id,
      Value<String?>? name,
      Value<String?>? icon,
      Value<String?>? color}) {
    return ReasonsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      icon: icon ?? this.icon,
      color: color ?? this.color,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (icon.present) {
      map['icon'] = Variable<String>(icon.value);
    }
    if (color.present) {
      map['color'] = Variable<String>(color.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ReasonsCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('icon: $icon, ')
          ..write('color: $color')
          ..write(')'))
        .toString();
  }
}

class $GoogleMapsDatasTable extends GoogleMapsDatas
    with TableInfo<$GoogleMapsDatasTable, GoogleMapsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $GoogleMapsDatasTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _googleIdMeta =
      const VerificationMeta('googleId');
  @override
  late final GeneratedColumn<String> googleId = GeneratedColumn<String>(
      'google_id', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _addressMeta =
      const VerificationMeta('address');
  @override
  late final GeneratedColumn<String> address = GeneratedColumn<String>(
      'address', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _cityMeta = const VerificationMeta('city');
  @override
  late final GeneratedColumn<String> city = GeneratedColumn<String>(
      'city', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _postcodeMeta =
      const VerificationMeta('postcode');
  @override
  late final GeneratedColumn<String> postcode = GeneratedColumn<String>(
      'postcode', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _countryMeta =
      const VerificationMeta('country');
  @override
  late final GeneratedColumn<String> country = GeneratedColumn<String>(
      'country', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, googleId, address, city, postcode, country, name, synced];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'google_maps_datas';
  @override
  VerificationContext validateIntegrity(Insertable<GoogleMapsData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('google_id')) {
      context.handle(_googleIdMeta,
          googleId.isAcceptableOrUnknown(data['google_id']!, _googleIdMeta));
    }
    if (data.containsKey('address')) {
      context.handle(_addressMeta,
          address.isAcceptableOrUnknown(data['address']!, _addressMeta));
    }
    if (data.containsKey('city')) {
      context.handle(
          _cityMeta, city.isAcceptableOrUnknown(data['city']!, _cityMeta));
    }
    if (data.containsKey('postcode')) {
      context.handle(_postcodeMeta,
          postcode.isAcceptableOrUnknown(data['postcode']!, _postcodeMeta));
    }
    if (data.containsKey('country')) {
      context.handle(_countryMeta,
          country.isAcceptableOrUnknown(data['country']!, _countryMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  GoogleMapsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return GoogleMapsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      googleId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}google_id']),
      address: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}address']),
      city: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}city']),
      postcode: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}postcode']),
      country: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}country']),
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced']),
    );
  }

  @override
  $GoogleMapsDatasTable createAlias(String alias) {
    return $GoogleMapsDatasTable(attachedDatabase, alias);
  }
}

class GoogleMapsData extends DataClass implements Insertable<GoogleMapsData> {
  final String uuid;
  final String? googleId;
  final String? address;
  final String? city;
  final String? postcode;
  final String? country;
  final String? name;
  final bool? synced;
  const GoogleMapsData(
      {required this.uuid,
      this.googleId,
      this.address,
      this.city,
      this.postcode,
      this.country,
      this.name,
      this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    if (!nullToAbsent || googleId != null) {
      map['google_id'] = Variable<String>(googleId);
    }
    if (!nullToAbsent || address != null) {
      map['address'] = Variable<String>(address);
    }
    if (!nullToAbsent || city != null) {
      map['city'] = Variable<String>(city);
    }
    if (!nullToAbsent || postcode != null) {
      map['postcode'] = Variable<String>(postcode);
    }
    if (!nullToAbsent || country != null) {
      map['country'] = Variable<String>(country);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || synced != null) {
      map['synced'] = Variable<bool>(synced);
    }
    return map;
  }

  GoogleMapsDatasCompanion toCompanion(bool nullToAbsent) {
    return GoogleMapsDatasCompanion(
      uuid: Value(uuid),
      googleId: googleId == null && nullToAbsent
          ? const Value.absent()
          : Value(googleId),
      address: address == null && nullToAbsent
          ? const Value.absent()
          : Value(address),
      city: city == null && nullToAbsent ? const Value.absent() : Value(city),
      postcode: postcode == null && nullToAbsent
          ? const Value.absent()
          : Value(postcode),
      country: country == null && nullToAbsent
          ? const Value.absent()
          : Value(country),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      synced:
          synced == null && nullToAbsent ? const Value.absent() : Value(synced),
    );
  }

  factory GoogleMapsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return GoogleMapsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      googleId: serializer.fromJson<String?>(json['googleId']),
      address: serializer.fromJson<String?>(json['address']),
      city: serializer.fromJson<String?>(json['city']),
      postcode: serializer.fromJson<String?>(json['postcode']),
      country: serializer.fromJson<String?>(json['country']),
      name: serializer.fromJson<String?>(json['name']),
      synced: serializer.fromJson<bool?>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'googleId': serializer.toJson<String?>(googleId),
      'address': serializer.toJson<String?>(address),
      'city': serializer.toJson<String?>(city),
      'postcode': serializer.toJson<String?>(postcode),
      'country': serializer.toJson<String?>(country),
      'name': serializer.toJson<String?>(name),
      'synced': serializer.toJson<bool?>(synced),
    };
  }

  GoogleMapsData copyWith(
          {String? uuid,
          Value<String?> googleId = const Value.absent(),
          Value<String?> address = const Value.absent(),
          Value<String?> city = const Value.absent(),
          Value<String?> postcode = const Value.absent(),
          Value<String?> country = const Value.absent(),
          Value<String?> name = const Value.absent(),
          Value<bool?> synced = const Value.absent()}) =>
      GoogleMapsData(
        uuid: uuid ?? this.uuid,
        googleId: googleId.present ? googleId.value : this.googleId,
        address: address.present ? address.value : this.address,
        city: city.present ? city.value : this.city,
        postcode: postcode.present ? postcode.value : this.postcode,
        country: country.present ? country.value : this.country,
        name: name.present ? name.value : this.name,
        synced: synced.present ? synced.value : this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('GoogleMapsData(')
          ..write('uuid: $uuid, ')
          ..write('googleId: $googleId, ')
          ..write('address: $address, ')
          ..write('city: $city, ')
          ..write('postcode: $postcode, ')
          ..write('country: $country, ')
          ..write('name: $name, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      uuid, googleId, address, city, postcode, country, name, synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is GoogleMapsData &&
          other.uuid == this.uuid &&
          other.googleId == this.googleId &&
          other.address == this.address &&
          other.city == this.city &&
          other.postcode == this.postcode &&
          other.country == this.country &&
          other.name == this.name &&
          other.synced == this.synced);
}

class GoogleMapsDatasCompanion extends UpdateCompanion<GoogleMapsData> {
  final Value<String> uuid;
  final Value<String?> googleId;
  final Value<String?> address;
  final Value<String?> city;
  final Value<String?> postcode;
  final Value<String?> country;
  final Value<String?> name;
  final Value<bool?> synced;
  final Value<int> rowid;
  const GoogleMapsDatasCompanion({
    this.uuid = const Value.absent(),
    this.googleId = const Value.absent(),
    this.address = const Value.absent(),
    this.city = const Value.absent(),
    this.postcode = const Value.absent(),
    this.country = const Value.absent(),
    this.name = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  GoogleMapsDatasCompanion.insert({
    this.uuid = const Value.absent(),
    this.googleId = const Value.absent(),
    this.address = const Value.absent(),
    this.city = const Value.absent(),
    this.postcode = const Value.absent(),
    this.country = const Value.absent(),
    this.name = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  static Insertable<GoogleMapsData> custom({
    Expression<String>? uuid,
    Expression<String>? googleId,
    Expression<String>? address,
    Expression<String>? city,
    Expression<String>? postcode,
    Expression<String>? country,
    Expression<String>? name,
    Expression<bool>? synced,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (googleId != null) 'google_id': googleId,
      if (address != null) 'address': address,
      if (city != null) 'city': city,
      if (postcode != null) 'postcode': postcode,
      if (country != null) 'country': country,
      if (name != null) 'name': name,
      if (synced != null) 'synced': synced,
      if (rowid != null) 'rowid': rowid,
    });
  }

  GoogleMapsDatasCompanion copyWith(
      {Value<String>? uuid,
      Value<String?>? googleId,
      Value<String?>? address,
      Value<String?>? city,
      Value<String?>? postcode,
      Value<String?>? country,
      Value<String?>? name,
      Value<bool?>? synced,
      Value<int>? rowid}) {
    return GoogleMapsDatasCompanion(
      uuid: uuid ?? this.uuid,
      googleId: googleId ?? this.googleId,
      address: address ?? this.address,
      city: city ?? this.city,
      postcode: postcode ?? this.postcode,
      country: country ?? this.country,
      name: name ?? this.name,
      synced: synced ?? this.synced,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (googleId.present) {
      map['google_id'] = Variable<String>(googleId.value);
    }
    if (address.present) {
      map['address'] = Variable<String>(address.value);
    }
    if (city.present) {
      map['city'] = Variable<String>(city.value);
    }
    if (postcode.present) {
      map['postcode'] = Variable<String>(postcode.value);
    }
    if (country.present) {
      map['country'] = Variable<String>(country.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('GoogleMapsDatasCompanion(')
          ..write('uuid: $uuid, ')
          ..write('googleId: $googleId, ')
          ..write('address: $address, ')
          ..write('city: $city, ')
          ..write('postcode: $postcode, ')
          ..write('country: $country, ')
          ..write('name: $name, ')
          ..write('synced: $synced, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $StopsTable extends Stops with TableInfo<$StopsTable, Stop> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $StopsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _classifiedPeriodUuidMeta =
      const VerificationMeta('classifiedPeriodUuid');
  @override
  late final GeneratedColumn<String> classifiedPeriodUuid =
      GeneratedColumn<String>(
          'classified_period_uuid', aliasedName, false,
          type: DriftSqlType.string,
          requiredDuringInsert: true,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES classified_periods (uuid)'));
  static const VerificationMeta _reasonIdMeta =
      const VerificationMeta('reasonId');
  @override
  late final GeneratedColumn<int> reasonId = GeneratedColumn<int>(
      'reason_id', aliasedName, true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES reasons (id)'));
  static const VerificationMeta _googleMapsDataUuidMeta =
      const VerificationMeta('googleMapsDataUuid');
  @override
  late final GeneratedColumn<String> googleMapsDataUuid =
      GeneratedColumn<String>('google_maps_data_uuid', aliasedName, true,
          type: DriftSqlType.string,
          requiredDuringInsert: false,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES google_maps_datas (uuid)'));
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, classifiedPeriodUuid, reasonId, googleMapsDataUuid, synced];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'stops';
  @override
  VerificationContext validateIntegrity(Insertable<Stop> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('classified_period_uuid')) {
      context.handle(
          _classifiedPeriodUuidMeta,
          classifiedPeriodUuid.isAcceptableOrUnknown(
              data['classified_period_uuid']!, _classifiedPeriodUuidMeta));
    } else if (isInserting) {
      context.missing(_classifiedPeriodUuidMeta);
    }
    if (data.containsKey('reason_id')) {
      context.handle(_reasonIdMeta,
          reasonId.isAcceptableOrUnknown(data['reason_id']!, _reasonIdMeta));
    }
    if (data.containsKey('google_maps_data_uuid')) {
      context.handle(
          _googleMapsDataUuidMeta,
          googleMapsDataUuid.isAcceptableOrUnknown(
              data['google_maps_data_uuid']!, _googleMapsDataUuidMeta));
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  Stop map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Stop(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      classifiedPeriodUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}classified_period_uuid'])!,
      reasonId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}reason_id']),
      googleMapsDataUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}google_maps_data_uuid']),
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced']),
    );
  }

  @override
  $StopsTable createAlias(String alias) {
    return $StopsTable(attachedDatabase, alias);
  }
}

class Stop extends DataClass implements Insertable<Stop> {
  final String uuid;
  final String classifiedPeriodUuid;
  final int? reasonId;
  final String? googleMapsDataUuid;
  final bool? synced;
  const Stop(
      {required this.uuid,
      required this.classifiedPeriodUuid,
      this.reasonId,
      this.googleMapsDataUuid,
      this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['classified_period_uuid'] = Variable<String>(classifiedPeriodUuid);
    if (!nullToAbsent || reasonId != null) {
      map['reason_id'] = Variable<int>(reasonId);
    }
    if (!nullToAbsent || googleMapsDataUuid != null) {
      map['google_maps_data_uuid'] = Variable<String>(googleMapsDataUuid);
    }
    if (!nullToAbsent || synced != null) {
      map['synced'] = Variable<bool>(synced);
    }
    return map;
  }

  StopsCompanion toCompanion(bool nullToAbsent) {
    return StopsCompanion(
      uuid: Value(uuid),
      classifiedPeriodUuid: Value(classifiedPeriodUuid),
      reasonId: reasonId == null && nullToAbsent
          ? const Value.absent()
          : Value(reasonId),
      googleMapsDataUuid: googleMapsDataUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(googleMapsDataUuid),
      synced:
          synced == null && nullToAbsent ? const Value.absent() : Value(synced),
    );
  }

  factory Stop.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Stop(
      uuid: serializer.fromJson<String>(json['uuid']),
      classifiedPeriodUuid:
          serializer.fromJson<String>(json['classifiedPeriodUuid']),
      reasonId: serializer.fromJson<int?>(json['reasonId']),
      googleMapsDataUuid:
          serializer.fromJson<String?>(json['googleMapsDataUuid']),
      synced: serializer.fromJson<bool?>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'classifiedPeriodUuid': serializer.toJson<String>(classifiedPeriodUuid),
      'reasonId': serializer.toJson<int?>(reasonId),
      'googleMapsDataUuid': serializer.toJson<String?>(googleMapsDataUuid),
      'synced': serializer.toJson<bool?>(synced),
    };
  }

  Stop copyWith(
          {String? uuid,
          String? classifiedPeriodUuid,
          Value<int?> reasonId = const Value.absent(),
          Value<String?> googleMapsDataUuid = const Value.absent(),
          Value<bool?> synced = const Value.absent()}) =>
      Stop(
        uuid: uuid ?? this.uuid,
        classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
        reasonId: reasonId.present ? reasonId.value : this.reasonId,
        googleMapsDataUuid: googleMapsDataUuid.present
            ? googleMapsDataUuid.value
            : this.googleMapsDataUuid,
        synced: synced.present ? synced.value : this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('Stop(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('reasonId: $reasonId, ')
          ..write('googleMapsDataUuid: $googleMapsDataUuid, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      uuid, classifiedPeriodUuid, reasonId, googleMapsDataUuid, synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Stop &&
          other.uuid == this.uuid &&
          other.classifiedPeriodUuid == this.classifiedPeriodUuid &&
          other.reasonId == this.reasonId &&
          other.googleMapsDataUuid == this.googleMapsDataUuid &&
          other.synced == this.synced);
}

class StopsCompanion extends UpdateCompanion<Stop> {
  final Value<String> uuid;
  final Value<String> classifiedPeriodUuid;
  final Value<int?> reasonId;
  final Value<String?> googleMapsDataUuid;
  final Value<bool?> synced;
  final Value<int> rowid;
  const StopsCompanion({
    this.uuid = const Value.absent(),
    this.classifiedPeriodUuid = const Value.absent(),
    this.reasonId = const Value.absent(),
    this.googleMapsDataUuid = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  StopsCompanion.insert({
    this.uuid = const Value.absent(),
    required String classifiedPeriodUuid,
    this.reasonId = const Value.absent(),
    this.googleMapsDataUuid = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  }) : classifiedPeriodUuid = Value(classifiedPeriodUuid);
  static Insertable<Stop> custom({
    Expression<String>? uuid,
    Expression<String>? classifiedPeriodUuid,
    Expression<int>? reasonId,
    Expression<String>? googleMapsDataUuid,
    Expression<bool>? synced,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (classifiedPeriodUuid != null)
        'classified_period_uuid': classifiedPeriodUuid,
      if (reasonId != null) 'reason_id': reasonId,
      if (googleMapsDataUuid != null)
        'google_maps_data_uuid': googleMapsDataUuid,
      if (synced != null) 'synced': synced,
      if (rowid != null) 'rowid': rowid,
    });
  }

  StopsCompanion copyWith(
      {Value<String>? uuid,
      Value<String>? classifiedPeriodUuid,
      Value<int?>? reasonId,
      Value<String?>? googleMapsDataUuid,
      Value<bool?>? synced,
      Value<int>? rowid}) {
    return StopsCompanion(
      uuid: uuid ?? this.uuid,
      classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
      reasonId: reasonId ?? this.reasonId,
      googleMapsDataUuid: googleMapsDataUuid ?? this.googleMapsDataUuid,
      synced: synced ?? this.synced,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (classifiedPeriodUuid.present) {
      map['classified_period_uuid'] =
          Variable<String>(classifiedPeriodUuid.value);
    }
    if (reasonId.present) {
      map['reason_id'] = Variable<int>(reasonId.value);
    }
    if (googleMapsDataUuid.present) {
      map['google_maps_data_uuid'] = Variable<String>(googleMapsDataUuid.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('StopsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('reasonId: $reasonId, ')
          ..write('googleMapsDataUuid: $googleMapsDataUuid, ')
          ..write('synced: $synced, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $VehiclesTable extends Vehicles with TableInfo<$VehiclesTable, Vehicle> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $VehiclesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, true,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _iconMeta = const VerificationMeta('icon');
  @override
  late final GeneratedColumn<String> icon = GeneratedColumn<String>(
      'icon', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _colorMeta = const VerificationMeta('color');
  @override
  late final GeneratedColumn<String> color = GeneratedColumn<String>(
      'color', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [id, name, icon, color];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'vehicles';
  @override
  VerificationContext validateIntegrity(Insertable<Vehicle> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    }
    if (data.containsKey('icon')) {
      context.handle(
          _iconMeta, icon.isAcceptableOrUnknown(data['icon']!, _iconMeta));
    }
    if (data.containsKey('color')) {
      context.handle(
          _colorMeta, color.isAcceptableOrUnknown(data['color']!, _colorMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Vehicle map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Vehicle(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id']),
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name']),
      icon: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}icon']),
      color: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}color']),
    );
  }

  @override
  $VehiclesTable createAlias(String alias) {
    return $VehiclesTable(attachedDatabase, alias);
  }
}

class Vehicle extends DataClass implements Insertable<Vehicle> {
  final int? id;
  final String? name;
  final String? icon;
  final String? color;
  const Vehicle({this.id, this.name, this.icon, this.color});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || icon != null) {
      map['icon'] = Variable<String>(icon);
    }
    if (!nullToAbsent || color != null) {
      map['color'] = Variable<String>(color);
    }
    return map;
  }

  VehiclesCompanion toCompanion(bool nullToAbsent) {
    return VehiclesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      icon: icon == null && nullToAbsent ? const Value.absent() : Value(icon),
      color:
          color == null && nullToAbsent ? const Value.absent() : Value(color),
    );
  }

  factory Vehicle.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Vehicle(
      id: serializer.fromJson<int?>(json['id']),
      name: serializer.fromJson<String?>(json['name']),
      icon: serializer.fromJson<String?>(json['icon']),
      color: serializer.fromJson<String?>(json['color']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int?>(id),
      'name': serializer.toJson<String?>(name),
      'icon': serializer.toJson<String?>(icon),
      'color': serializer.toJson<String?>(color),
    };
  }

  Vehicle copyWith(
          {Value<int?> id = const Value.absent(),
          Value<String?> name = const Value.absent(),
          Value<String?> icon = const Value.absent(),
          Value<String?> color = const Value.absent()}) =>
      Vehicle(
        id: id.present ? id.value : this.id,
        name: name.present ? name.value : this.name,
        icon: icon.present ? icon.value : this.icon,
        color: color.present ? color.value : this.color,
      );
  @override
  String toString() {
    return (StringBuffer('Vehicle(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('icon: $icon, ')
          ..write('color: $color')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, icon, color);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Vehicle &&
          other.id == this.id &&
          other.name == this.name &&
          other.icon == this.icon &&
          other.color == this.color);
}

class VehiclesCompanion extends UpdateCompanion<Vehicle> {
  final Value<int?> id;
  final Value<String?> name;
  final Value<String?> icon;
  final Value<String?> color;
  const VehiclesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.icon = const Value.absent(),
    this.color = const Value.absent(),
  });
  VehiclesCompanion.insert({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.icon = const Value.absent(),
    this.color = const Value.absent(),
  });
  static Insertable<Vehicle> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<String>? icon,
    Expression<String>? color,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (icon != null) 'icon': icon,
      if (color != null) 'color': color,
    });
  }

  VehiclesCompanion copyWith(
      {Value<int?>? id,
      Value<String?>? name,
      Value<String?>? icon,
      Value<String?>? color}) {
    return VehiclesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      icon: icon ?? this.icon,
      color: color ?? this.color,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (icon.present) {
      map['icon'] = Variable<String>(icon.value);
    }
    if (color.present) {
      map['color'] = Variable<String>(color.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('VehiclesCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('icon: $icon, ')
          ..write('color: $color')
          ..write(')'))
        .toString();
  }
}

class $MovementsTable extends Movements
    with TableInfo<$MovementsTable, Movement> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $MovementsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      clientDefault: () => Uuid().v4());
  static const VerificationMeta _classifiedPeriodUuidMeta =
      const VerificationMeta('classifiedPeriodUuid');
  @override
  late final GeneratedColumn<String> classifiedPeriodUuid =
      GeneratedColumn<String>(
          'classified_period_uuid', aliasedName, false,
          type: DriftSqlType.string,
          requiredDuringInsert: true,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES classified_periods (uuid)'));
  static const VerificationMeta _vehicleIdMeta =
      const VerificationMeta('vehicleId');
  @override
  late final GeneratedColumn<int> vehicleId = GeneratedColumn<int>(
      'vehicle_id', aliasedName, true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES vehicles (id)'));
  static const VerificationMeta _transportMeta =
      const VerificationMeta('transport');
  @override
  late final GeneratedColumn<int> transport = GeneratedColumn<int>(
      'transport', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _clusterCountMeta =
      const VerificationMeta('clusterCount');
  @override
  late final GeneratedColumn<int> clusterCount = GeneratedColumn<int>(
      'cluster_count', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _syncedMeta = const VerificationMeta('synced');
  @override
  late final GeneratedColumn<bool> synced = GeneratedColumn<bool>(
      'synced', aliasedName, true,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("synced" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, classifiedPeriodUuid, vehicleId, transport, clusterCount, synced];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'movements';
  @override
  VerificationContext validateIntegrity(Insertable<Movement> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('classified_period_uuid')) {
      context.handle(
          _classifiedPeriodUuidMeta,
          classifiedPeriodUuid.isAcceptableOrUnknown(
              data['classified_period_uuid']!, _classifiedPeriodUuidMeta));
    } else if (isInserting) {
      context.missing(_classifiedPeriodUuidMeta);
    }
    if (data.containsKey('vehicle_id')) {
      context.handle(_vehicleIdMeta,
          vehicleId.isAcceptableOrUnknown(data['vehicle_id']!, _vehicleIdMeta));
    }
    if (data.containsKey('transport')) {
      context.handle(_transportMeta,
          transport.isAcceptableOrUnknown(data['transport']!, _transportMeta));
    }
    if (data.containsKey('cluster_count')) {
      context.handle(
          _clusterCountMeta,
          clusterCount.isAcceptableOrUnknown(
              data['cluster_count']!, _clusterCountMeta));
    } else if (isInserting) {
      context.missing(_clusterCountMeta);
    }
    if (data.containsKey('synced')) {
      context.handle(_syncedMeta,
          synced.isAcceptableOrUnknown(data['synced']!, _syncedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  Movement map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Movement(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      classifiedPeriodUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}classified_period_uuid'])!,
      vehicleId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}vehicle_id']),
      transport: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}transport']),
      clusterCount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}cluster_count'])!,
      synced: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}synced']),
    );
  }

  @override
  $MovementsTable createAlias(String alias) {
    return $MovementsTable(attachedDatabase, alias);
  }
}

class Movement extends DataClass implements Insertable<Movement> {
  final String uuid;
  final String classifiedPeriodUuid;
  final int? vehicleId;
  final int? transport;
  final int clusterCount;
  final bool? synced;
  const Movement(
      {required this.uuid,
      required this.classifiedPeriodUuid,
      this.vehicleId,
      this.transport,
      required this.clusterCount,
      this.synced});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['classified_period_uuid'] = Variable<String>(classifiedPeriodUuid);
    if (!nullToAbsent || vehicleId != null) {
      map['vehicle_id'] = Variable<int>(vehicleId);
    }
    if (!nullToAbsent || transport != null) {
      map['transport'] = Variable<int>(transport);
    }
    map['cluster_count'] = Variable<int>(clusterCount);
    if (!nullToAbsent || synced != null) {
      map['synced'] = Variable<bool>(synced);
    }
    return map;
  }

  MovementsCompanion toCompanion(bool nullToAbsent) {
    return MovementsCompanion(
      uuid: Value(uuid),
      classifiedPeriodUuid: Value(classifiedPeriodUuid),
      vehicleId: vehicleId == null && nullToAbsent
          ? const Value.absent()
          : Value(vehicleId),
      transport: transport == null && nullToAbsent
          ? const Value.absent()
          : Value(transport),
      clusterCount: Value(clusterCount),
      synced:
          synced == null && nullToAbsent ? const Value.absent() : Value(synced),
    );
  }

  factory Movement.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Movement(
      uuid: serializer.fromJson<String>(json['uuid']),
      classifiedPeriodUuid:
          serializer.fromJson<String>(json['classifiedPeriodUuid']),
      vehicleId: serializer.fromJson<int?>(json['vehicleId']),
      transport: serializer.fromJson<int?>(json['transport']),
      clusterCount: serializer.fromJson<int>(json['clusterCount']),
      synced: serializer.fromJson<bool?>(json['synced']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'classifiedPeriodUuid': serializer.toJson<String>(classifiedPeriodUuid),
      'vehicleId': serializer.toJson<int?>(vehicleId),
      'transport': serializer.toJson<int?>(transport),
      'clusterCount': serializer.toJson<int>(clusterCount),
      'synced': serializer.toJson<bool?>(synced),
    };
  }

  Movement copyWith(
          {String? uuid,
          String? classifiedPeriodUuid,
          Value<int?> vehicleId = const Value.absent(),
          Value<int?> transport = const Value.absent(),
          int? clusterCount,
          Value<bool?> synced = const Value.absent()}) =>
      Movement(
        uuid: uuid ?? this.uuid,
        classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
        vehicleId: vehicleId.present ? vehicleId.value : this.vehicleId,
        transport: transport.present ? transport.value : this.transport,
        clusterCount: clusterCount ?? this.clusterCount,
        synced: synced.present ? synced.value : this.synced,
      );
  @override
  String toString() {
    return (StringBuffer('Movement(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('vehicleId: $vehicleId, ')
          ..write('transport: $transport, ')
          ..write('clusterCount: $clusterCount, ')
          ..write('synced: $synced')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      uuid, classifiedPeriodUuid, vehicleId, transport, clusterCount, synced);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Movement &&
          other.uuid == this.uuid &&
          other.classifiedPeriodUuid == this.classifiedPeriodUuid &&
          other.vehicleId == this.vehicleId &&
          other.transport == this.transport &&
          other.clusterCount == this.clusterCount &&
          other.synced == this.synced);
}

class MovementsCompanion extends UpdateCompanion<Movement> {
  final Value<String> uuid;
  final Value<String> classifiedPeriodUuid;
  final Value<int?> vehicleId;
  final Value<int?> transport;
  final Value<int> clusterCount;
  final Value<bool?> synced;
  final Value<int> rowid;
  const MovementsCompanion({
    this.uuid = const Value.absent(),
    this.classifiedPeriodUuid = const Value.absent(),
    this.vehicleId = const Value.absent(),
    this.transport = const Value.absent(),
    this.clusterCount = const Value.absent(),
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  MovementsCompanion.insert({
    this.uuid = const Value.absent(),
    required String classifiedPeriodUuid,
    this.vehicleId = const Value.absent(),
    this.transport = const Value.absent(),
    required int clusterCount,
    this.synced = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : classifiedPeriodUuid = Value(classifiedPeriodUuid),
        clusterCount = Value(clusterCount);
  static Insertable<Movement> custom({
    Expression<String>? uuid,
    Expression<String>? classifiedPeriodUuid,
    Expression<int>? vehicleId,
    Expression<int>? transport,
    Expression<int>? clusterCount,
    Expression<bool>? synced,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (classifiedPeriodUuid != null)
        'classified_period_uuid': classifiedPeriodUuid,
      if (vehicleId != null) 'vehicle_id': vehicleId,
      if (transport != null) 'transport': transport,
      if (clusterCount != null) 'cluster_count': clusterCount,
      if (synced != null) 'synced': synced,
      if (rowid != null) 'rowid': rowid,
    });
  }

  MovementsCompanion copyWith(
      {Value<String>? uuid,
      Value<String>? classifiedPeriodUuid,
      Value<int?>? vehicleId,
      Value<int?>? transport,
      Value<int>? clusterCount,
      Value<bool?>? synced,
      Value<int>? rowid}) {
    return MovementsCompanion(
      uuid: uuid ?? this.uuid,
      classifiedPeriodUuid: classifiedPeriodUuid ?? this.classifiedPeriodUuid,
      vehicleId: vehicleId ?? this.vehicleId,
      transport: transport ?? this.transport,
      clusterCount: clusterCount ?? this.clusterCount,
      synced: synced ?? this.synced,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (classifiedPeriodUuid.present) {
      map['classified_period_uuid'] =
          Variable<String>(classifiedPeriodUuid.value);
    }
    if (vehicleId.present) {
      map['vehicle_id'] = Variable<int>(vehicleId.value);
    }
    if (transport.present) {
      map['transport'] = Variable<int>(transport.value);
    }
    if (clusterCount.present) {
      map['cluster_count'] = Variable<int>(clusterCount.value);
    }
    if (synced.present) {
      map['synced'] = Variable<bool>(synced.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('MovementsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('classifiedPeriodUuid: $classifiedPeriodUuid, ')
          ..write('vehicleId: $vehicleId, ')
          ..write('transport: $transport, ')
          ..write('clusterCount: $clusterCount, ')
          ..write('synced: $synced, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $DevicesTable extends Devices with TableInfo<$DevicesTable, Device> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DevicesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, true,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _deviceMeta = const VerificationMeta('device');
  @override
  late final GeneratedColumn<String> device = GeneratedColumn<String>(
      'device', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _versionMeta =
      const VerificationMeta('version');
  @override
  late final GeneratedColumn<String> version = GeneratedColumn<String>(
      'version', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _productMeta =
      const VerificationMeta('product');
  @override
  late final GeneratedColumn<String> product = GeneratedColumn<String>(
      'product', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _modelMeta = const VerificationMeta('model');
  @override
  late final GeneratedColumn<String> model = GeneratedColumn<String>(
      'model', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _brandMeta = const VerificationMeta('brand');
  @override
  late final GeneratedColumn<String> brand = GeneratedColumn<String>(
      'brand', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _androidIdMeta =
      const VerificationMeta('androidId');
  @override
  late final GeneratedColumn<String> androidId = GeneratedColumn<String>(
      'android_id', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _secureIdMeta =
      const VerificationMeta('secureId');
  @override
  late final GeneratedColumn<String> secureId = GeneratedColumn<String>(
      'secure_id', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _sdkMeta = const VerificationMeta('sdk');
  @override
  late final GeneratedColumn<String> sdk = GeneratedColumn<String>(
      'sdk', aliasedName, true,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 150),
      type: DriftSqlType.string,
      requiredDuringInsert: false);
  static const VerificationMeta _widthMeta = const VerificationMeta('width');
  @override
  late final GeneratedColumn<double> width = GeneratedColumn<double>(
      'width', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _heightMeta = const VerificationMeta('height');
  @override
  late final GeneratedColumn<double> height = GeneratedColumn<double>(
      'height', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _widthLogicalMeta =
      const VerificationMeta('widthLogical');
  @override
  late final GeneratedColumn<double> widthLogical = GeneratedColumn<double>(
      'width_logical', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _heightLogicalMeta =
      const VerificationMeta('heightLogical');
  @override
  late final GeneratedColumn<double> heightLogical = GeneratedColumn<double>(
      'height_logical', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _sensorLockMeta =
      const VerificationMeta('sensorLock');
  @override
  late final GeneratedColumn<bool> sensorLock = GeneratedColumn<bool>(
      'sensor_lock', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("sensor_lock" IN (0, 1))'),
      defaultValue: const Constant(false));
  static const VerificationMeta _logLockMeta =
      const VerificationMeta('logLock');
  @override
  late final GeneratedColumn<bool> logLock = GeneratedColumn<bool>(
      'log_lock', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("log_lock" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        device,
        version,
        product,
        model,
        brand,
        androidId,
        secureId,
        sdk,
        width,
        height,
        widthLogical,
        heightLogical,
        sensorLock,
        logLock
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'devices';
  @override
  VerificationContext validateIntegrity(Insertable<Device> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('device')) {
      context.handle(_deviceMeta,
          device.isAcceptableOrUnknown(data['device']!, _deviceMeta));
    }
    if (data.containsKey('version')) {
      context.handle(_versionMeta,
          version.isAcceptableOrUnknown(data['version']!, _versionMeta));
    }
    if (data.containsKey('product')) {
      context.handle(_productMeta,
          product.isAcceptableOrUnknown(data['product']!, _productMeta));
    }
    if (data.containsKey('model')) {
      context.handle(
          _modelMeta, model.isAcceptableOrUnknown(data['model']!, _modelMeta));
    }
    if (data.containsKey('brand')) {
      context.handle(
          _brandMeta, brand.isAcceptableOrUnknown(data['brand']!, _brandMeta));
    }
    if (data.containsKey('android_id')) {
      context.handle(_androidIdMeta,
          androidId.isAcceptableOrUnknown(data['android_id']!, _androidIdMeta));
    }
    if (data.containsKey('secure_id')) {
      context.handle(_secureIdMeta,
          secureId.isAcceptableOrUnknown(data['secure_id']!, _secureIdMeta));
    }
    if (data.containsKey('sdk')) {
      context.handle(
          _sdkMeta, sdk.isAcceptableOrUnknown(data['sdk']!, _sdkMeta));
    }
    if (data.containsKey('width')) {
      context.handle(
          _widthMeta, width.isAcceptableOrUnknown(data['width']!, _widthMeta));
    }
    if (data.containsKey('height')) {
      context.handle(_heightMeta,
          height.isAcceptableOrUnknown(data['height']!, _heightMeta));
    }
    if (data.containsKey('width_logical')) {
      context.handle(
          _widthLogicalMeta,
          widthLogical.isAcceptableOrUnknown(
              data['width_logical']!, _widthLogicalMeta));
    }
    if (data.containsKey('height_logical')) {
      context.handle(
          _heightLogicalMeta,
          heightLogical.isAcceptableOrUnknown(
              data['height_logical']!, _heightLogicalMeta));
    }
    if (data.containsKey('sensor_lock')) {
      context.handle(
          _sensorLockMeta,
          sensorLock.isAcceptableOrUnknown(
              data['sensor_lock']!, _sensorLockMeta));
    }
    if (data.containsKey('log_lock')) {
      context.handle(_logLockMeta,
          logLock.isAcceptableOrUnknown(data['log_lock']!, _logLockMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Device map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Device(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id']),
      device: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device']),
      version: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}version']),
      product: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}product']),
      model: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}model']),
      brand: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}brand']),
      androidId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}android_id']),
      secureId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}secure_id']),
      sdk: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}sdk']),
      width: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}width']),
      height: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}height']),
      widthLogical: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}width_logical']),
      heightLogical: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}height_logical']),
      sensorLock: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}sensor_lock'])!,
      logLock: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}log_lock'])!,
    );
  }

  @override
  $DevicesTable createAlias(String alias) {
    return $DevicesTable(attachedDatabase, alias);
  }
}

class Device extends DataClass implements Insertable<Device> {
  final int? id;
  final String? device;
  final String? version;
  final String? product;
  final String? model;
  final String? brand;
  final String? androidId;
  final String? secureId;
  final String? sdk;
  final double? width;
  final double? height;
  final double? widthLogical;
  final double? heightLogical;
  final bool sensorLock;
  final bool logLock;
  const Device(
      {this.id,
      this.device,
      this.version,
      this.product,
      this.model,
      this.brand,
      this.androidId,
      this.secureId,
      this.sdk,
      this.width,
      this.height,
      this.widthLogical,
      this.heightLogical,
      required this.sensorLock,
      required this.logLock});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || device != null) {
      map['device'] = Variable<String>(device);
    }
    if (!nullToAbsent || version != null) {
      map['version'] = Variable<String>(version);
    }
    if (!nullToAbsent || product != null) {
      map['product'] = Variable<String>(product);
    }
    if (!nullToAbsent || model != null) {
      map['model'] = Variable<String>(model);
    }
    if (!nullToAbsent || brand != null) {
      map['brand'] = Variable<String>(brand);
    }
    if (!nullToAbsent || androidId != null) {
      map['android_id'] = Variable<String>(androidId);
    }
    if (!nullToAbsent || secureId != null) {
      map['secure_id'] = Variable<String>(secureId);
    }
    if (!nullToAbsent || sdk != null) {
      map['sdk'] = Variable<String>(sdk);
    }
    if (!nullToAbsent || width != null) {
      map['width'] = Variable<double>(width);
    }
    if (!nullToAbsent || height != null) {
      map['height'] = Variable<double>(height);
    }
    if (!nullToAbsent || widthLogical != null) {
      map['width_logical'] = Variable<double>(widthLogical);
    }
    if (!nullToAbsent || heightLogical != null) {
      map['height_logical'] = Variable<double>(heightLogical);
    }
    map['sensor_lock'] = Variable<bool>(sensorLock);
    map['log_lock'] = Variable<bool>(logLock);
    return map;
  }

  DevicesCompanion toCompanion(bool nullToAbsent) {
    return DevicesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      device:
          device == null && nullToAbsent ? const Value.absent() : Value(device),
      version: version == null && nullToAbsent
          ? const Value.absent()
          : Value(version),
      product: product == null && nullToAbsent
          ? const Value.absent()
          : Value(product),
      model:
          model == null && nullToAbsent ? const Value.absent() : Value(model),
      brand:
          brand == null && nullToAbsent ? const Value.absent() : Value(brand),
      androidId: androidId == null && nullToAbsent
          ? const Value.absent()
          : Value(androidId),
      secureId: secureId == null && nullToAbsent
          ? const Value.absent()
          : Value(secureId),
      sdk: sdk == null && nullToAbsent ? const Value.absent() : Value(sdk),
      width:
          width == null && nullToAbsent ? const Value.absent() : Value(width),
      height:
          height == null && nullToAbsent ? const Value.absent() : Value(height),
      widthLogical: widthLogical == null && nullToAbsent
          ? const Value.absent()
          : Value(widthLogical),
      heightLogical: heightLogical == null && nullToAbsent
          ? const Value.absent()
          : Value(heightLogical),
      sensorLock: Value(sensorLock),
      logLock: Value(logLock),
    );
  }

  factory Device.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Device(
      id: serializer.fromJson<int?>(json['id']),
      device: serializer.fromJson<String?>(json['device']),
      version: serializer.fromJson<String?>(json['version']),
      product: serializer.fromJson<String?>(json['product']),
      model: serializer.fromJson<String?>(json['model']),
      brand: serializer.fromJson<String?>(json['brand']),
      androidId: serializer.fromJson<String?>(json['androidId']),
      secureId: serializer.fromJson<String?>(json['secureId']),
      sdk: serializer.fromJson<String?>(json['sdk']),
      width: serializer.fromJson<double?>(json['width']),
      height: serializer.fromJson<double?>(json['height']),
      widthLogical: serializer.fromJson<double?>(json['widthLogical']),
      heightLogical: serializer.fromJson<double?>(json['heightLogical']),
      sensorLock: serializer.fromJson<bool>(json['sensorLock']),
      logLock: serializer.fromJson<bool>(json['logLock']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int?>(id),
      'device': serializer.toJson<String?>(device),
      'version': serializer.toJson<String?>(version),
      'product': serializer.toJson<String?>(product),
      'model': serializer.toJson<String?>(model),
      'brand': serializer.toJson<String?>(brand),
      'androidId': serializer.toJson<String?>(androidId),
      'secureId': serializer.toJson<String?>(secureId),
      'sdk': serializer.toJson<String?>(sdk),
      'width': serializer.toJson<double?>(width),
      'height': serializer.toJson<double?>(height),
      'widthLogical': serializer.toJson<double?>(widthLogical),
      'heightLogical': serializer.toJson<double?>(heightLogical),
      'sensorLock': serializer.toJson<bool>(sensorLock),
      'logLock': serializer.toJson<bool>(logLock),
    };
  }

  Device copyWith(
          {Value<int?> id = const Value.absent(),
          Value<String?> device = const Value.absent(),
          Value<String?> version = const Value.absent(),
          Value<String?> product = const Value.absent(),
          Value<String?> model = const Value.absent(),
          Value<String?> brand = const Value.absent(),
          Value<String?> androidId = const Value.absent(),
          Value<String?> secureId = const Value.absent(),
          Value<String?> sdk = const Value.absent(),
          Value<double?> width = const Value.absent(),
          Value<double?> height = const Value.absent(),
          Value<double?> widthLogical = const Value.absent(),
          Value<double?> heightLogical = const Value.absent(),
          bool? sensorLock,
          bool? logLock}) =>
      Device(
        id: id.present ? id.value : this.id,
        device: device.present ? device.value : this.device,
        version: version.present ? version.value : this.version,
        product: product.present ? product.value : this.product,
        model: model.present ? model.value : this.model,
        brand: brand.present ? brand.value : this.brand,
        androidId: androidId.present ? androidId.value : this.androidId,
        secureId: secureId.present ? secureId.value : this.secureId,
        sdk: sdk.present ? sdk.value : this.sdk,
        width: width.present ? width.value : this.width,
        height: height.present ? height.value : this.height,
        widthLogical:
            widthLogical.present ? widthLogical.value : this.widthLogical,
        heightLogical:
            heightLogical.present ? heightLogical.value : this.heightLogical,
        sensorLock: sensorLock ?? this.sensorLock,
        logLock: logLock ?? this.logLock,
      );
  @override
  String toString() {
    return (StringBuffer('Device(')
          ..write('id: $id, ')
          ..write('device: $device, ')
          ..write('version: $version, ')
          ..write('product: $product, ')
          ..write('model: $model, ')
          ..write('brand: $brand, ')
          ..write('androidId: $androidId, ')
          ..write('secureId: $secureId, ')
          ..write('sdk: $sdk, ')
          ..write('width: $width, ')
          ..write('height: $height, ')
          ..write('widthLogical: $widthLogical, ')
          ..write('heightLogical: $heightLogical, ')
          ..write('sensorLock: $sensorLock, ')
          ..write('logLock: $logLock')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      device,
      version,
      product,
      model,
      brand,
      androidId,
      secureId,
      sdk,
      width,
      height,
      widthLogical,
      heightLogical,
      sensorLock,
      logLock);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Device &&
          other.id == this.id &&
          other.device == this.device &&
          other.version == this.version &&
          other.product == this.product &&
          other.model == this.model &&
          other.brand == this.brand &&
          other.androidId == this.androidId &&
          other.secureId == this.secureId &&
          other.sdk == this.sdk &&
          other.width == this.width &&
          other.height == this.height &&
          other.widthLogical == this.widthLogical &&
          other.heightLogical == this.heightLogical &&
          other.sensorLock == this.sensorLock &&
          other.logLock == this.logLock);
}

class DevicesCompanion extends UpdateCompanion<Device> {
  final Value<int?> id;
  final Value<String?> device;
  final Value<String?> version;
  final Value<String?> product;
  final Value<String?> model;
  final Value<String?> brand;
  final Value<String?> androidId;
  final Value<String?> secureId;
  final Value<String?> sdk;
  final Value<double?> width;
  final Value<double?> height;
  final Value<double?> widthLogical;
  final Value<double?> heightLogical;
  final Value<bool> sensorLock;
  final Value<bool> logLock;
  const DevicesCompanion({
    this.id = const Value.absent(),
    this.device = const Value.absent(),
    this.version = const Value.absent(),
    this.product = const Value.absent(),
    this.model = const Value.absent(),
    this.brand = const Value.absent(),
    this.androidId = const Value.absent(),
    this.secureId = const Value.absent(),
    this.sdk = const Value.absent(),
    this.width = const Value.absent(),
    this.height = const Value.absent(),
    this.widthLogical = const Value.absent(),
    this.heightLogical = const Value.absent(),
    this.sensorLock = const Value.absent(),
    this.logLock = const Value.absent(),
  });
  DevicesCompanion.insert({
    this.id = const Value.absent(),
    this.device = const Value.absent(),
    this.version = const Value.absent(),
    this.product = const Value.absent(),
    this.model = const Value.absent(),
    this.brand = const Value.absent(),
    this.androidId = const Value.absent(),
    this.secureId = const Value.absent(),
    this.sdk = const Value.absent(),
    this.width = const Value.absent(),
    this.height = const Value.absent(),
    this.widthLogical = const Value.absent(),
    this.heightLogical = const Value.absent(),
    this.sensorLock = const Value.absent(),
    this.logLock = const Value.absent(),
  });
  static Insertable<Device> custom({
    Expression<int>? id,
    Expression<String>? device,
    Expression<String>? version,
    Expression<String>? product,
    Expression<String>? model,
    Expression<String>? brand,
    Expression<String>? androidId,
    Expression<String>? secureId,
    Expression<String>? sdk,
    Expression<double>? width,
    Expression<double>? height,
    Expression<double>? widthLogical,
    Expression<double>? heightLogical,
    Expression<bool>? sensorLock,
    Expression<bool>? logLock,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (device != null) 'device': device,
      if (version != null) 'version': version,
      if (product != null) 'product': product,
      if (model != null) 'model': model,
      if (brand != null) 'brand': brand,
      if (androidId != null) 'android_id': androidId,
      if (secureId != null) 'secure_id': secureId,
      if (sdk != null) 'sdk': sdk,
      if (width != null) 'width': width,
      if (height != null) 'height': height,
      if (widthLogical != null) 'width_logical': widthLogical,
      if (heightLogical != null) 'height_logical': heightLogical,
      if (sensorLock != null) 'sensor_lock': sensorLock,
      if (logLock != null) 'log_lock': logLock,
    });
  }

  DevicesCompanion copyWith(
      {Value<int?>? id,
      Value<String?>? device,
      Value<String?>? version,
      Value<String?>? product,
      Value<String?>? model,
      Value<String?>? brand,
      Value<String?>? androidId,
      Value<String?>? secureId,
      Value<String?>? sdk,
      Value<double?>? width,
      Value<double?>? height,
      Value<double?>? widthLogical,
      Value<double?>? heightLogical,
      Value<bool>? sensorLock,
      Value<bool>? logLock}) {
    return DevicesCompanion(
      id: id ?? this.id,
      device: device ?? this.device,
      version: version ?? this.version,
      product: product ?? this.product,
      model: model ?? this.model,
      brand: brand ?? this.brand,
      androidId: androidId ?? this.androidId,
      secureId: secureId ?? this.secureId,
      sdk: sdk ?? this.sdk,
      width: width ?? this.width,
      height: height ?? this.height,
      widthLogical: widthLogical ?? this.widthLogical,
      heightLogical: heightLogical ?? this.heightLogical,
      sensorLock: sensorLock ?? this.sensorLock,
      logLock: logLock ?? this.logLock,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (device.present) {
      map['device'] = Variable<String>(device.value);
    }
    if (version.present) {
      map['version'] = Variable<String>(version.value);
    }
    if (product.present) {
      map['product'] = Variable<String>(product.value);
    }
    if (model.present) {
      map['model'] = Variable<String>(model.value);
    }
    if (brand.present) {
      map['brand'] = Variable<String>(brand.value);
    }
    if (androidId.present) {
      map['android_id'] = Variable<String>(androidId.value);
    }
    if (secureId.present) {
      map['secure_id'] = Variable<String>(secureId.value);
    }
    if (sdk.present) {
      map['sdk'] = Variable<String>(sdk.value);
    }
    if (width.present) {
      map['width'] = Variable<double>(width.value);
    }
    if (height.present) {
      map['height'] = Variable<double>(height.value);
    }
    if (widthLogical.present) {
      map['width_logical'] = Variable<double>(widthLogical.value);
    }
    if (heightLogical.present) {
      map['height_logical'] = Variable<double>(heightLogical.value);
    }
    if (sensorLock.present) {
      map['sensor_lock'] = Variable<bool>(sensorLock.value);
    }
    if (logLock.present) {
      map['log_lock'] = Variable<bool>(logLock.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DevicesCompanion(')
          ..write('id: $id, ')
          ..write('device: $device, ')
          ..write('version: $version, ')
          ..write('product: $product, ')
          ..write('model: $model, ')
          ..write('brand: $brand, ')
          ..write('androidId: $androidId, ')
          ..write('secureId: $secureId, ')
          ..write('sdk: $sdk, ')
          ..write('width: $width, ')
          ..write('height: $height, ')
          ..write('widthLogical: $widthLogical, ')
          ..write('heightLogical: $heightLogical, ')
          ..write('sensorLock: $sensorLock, ')
          ..write('logLock: $logLock')
          ..write(')'))
        .toString();
  }
}

class $TokensTable extends Tokens with TableInfo<$TokensTable, Token> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TokensTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, true,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _authTokenMeta =
      const VerificationMeta('authToken');
  @override
  late final GeneratedColumn<String> authToken = GeneratedColumn<String>(
      'auth_token', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, authToken];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'tokens';
  @override
  VerificationContext validateIntegrity(Insertable<Token> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('auth_token')) {
      context.handle(_authTokenMeta,
          authToken.isAcceptableOrUnknown(data['auth_token']!, _authTokenMeta));
    } else if (isInserting) {
      context.missing(_authTokenMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Token map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Token(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id']),
      authToken: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}auth_token'])!,
    );
  }

  @override
  $TokensTable createAlias(String alias) {
    return $TokensTable(attachedDatabase, alias);
  }
}

class Token extends DataClass implements Insertable<Token> {
  final int? id;
  final String authToken;
  const Token({this.id, required this.authToken});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    map['auth_token'] = Variable<String>(authToken);
    return map;
  }

  TokensCompanion toCompanion(bool nullToAbsent) {
    return TokensCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      authToken: Value(authToken),
    );
  }

  factory Token.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Token(
      id: serializer.fromJson<int?>(json['id']),
      authToken: serializer.fromJson<String>(json['authToken']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int?>(id),
      'authToken': serializer.toJson<String>(authToken),
    };
  }

  Token copyWith({Value<int?> id = const Value.absent(), String? authToken}) =>
      Token(
        id: id.present ? id.value : this.id,
        authToken: authToken ?? this.authToken,
      );
  @override
  String toString() {
    return (StringBuffer('Token(')
          ..write('id: $id, ')
          ..write('authToken: $authToken')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, authToken);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Token &&
          other.id == this.id &&
          other.authToken == this.authToken);
}

class TokensCompanion extends UpdateCompanion<Token> {
  final Value<int?> id;
  final Value<String> authToken;
  const TokensCompanion({
    this.id = const Value.absent(),
    this.authToken = const Value.absent(),
  });
  TokensCompanion.insert({
    this.id = const Value.absent(),
    required String authToken,
  }) : authToken = Value(authToken);
  static Insertable<Token> custom({
    Expression<int>? id,
    Expression<String>? authToken,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (authToken != null) 'auth_token': authToken,
    });
  }

  TokensCompanion copyWith({Value<int?>? id, Value<String>? authToken}) {
    return TokensCompanion(
      id: id ?? this.id,
      authToken: authToken ?? this.authToken,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (authToken.present) {
      map['auth_token'] = Variable<String>(authToken.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TokensCompanion(')
          ..write('id: $id, ')
          ..write('authToken: $authToken')
          ..write(')'))
        .toString();
  }
}

abstract class _$Database extends GeneratedDatabase {
  _$Database(QueryExecutor e) : super(e);
  _$DatabaseManager get managers => _$DatabaseManager(this);
  late final $TrackedDaysTable trackedDays = $TrackedDaysTable(this);
  late final $ClassifiedPeriodsTable classifiedPeriods =
      $ClassifiedPeriodsTable(this);
  late final $ClustersTable clusters = $ClustersTable(this);
  late final $SensorGeolocationsTable sensorGeolocations =
      $SensorGeolocationsTable(this);
  late final Index sensorGeolocationCreatedOnIndex = Index(
      'sensor_geolocation_created_on_index',
      'CREATE INDEX sensor_geolocation_created_on_index ON sensor_geolocations (created_on)');
  late final Index sensorGeolocationDeletedOnIndex = Index(
      'sensor_geolocation_deleted_on_index',
      'CREATE INDEX sensor_geolocation_deleted_on_index ON sensor_geolocations (deleted_on)');
  late final Index sensorGeolocationIsNoiseIndex = Index(
      'sensor_geolocation_is_noise_index',
      'CREATE INDEX sensor_geolocation_is_noise_index ON sensor_geolocations (is_noise)');
  late final Index sensorGeolocationSensorTypeIndex = Index(
      'sensor_geolocation_sensor_type_index',
      'CREATE INDEX sensor_geolocation_sensor_type_index ON sensor_geolocations (sensory_type)');
  late final Index sensorGeolocationAccuracyIndex = Index(
      'sensor_geolocation_accuracy_index',
      'CREATE INDEX sensor_geolocation_accuracy_index ON sensor_geolocations (accuracy)');
  late final $ManualGeolocationsTable manualGeolocations =
      $ManualGeolocationsTable(this);
  late final Index manualGeolocationCreatedOnIndex = Index(
      'manual_geolocation_created_on_index',
      'CREATE INDEX manual_geolocation_created_on_index ON manual_geolocations (created_on)');
  late final Index manualGeolocationDeletedOnIndex = Index(
      'manual_geolocation_deleted_on_index',
      'CREATE INDEX manual_geolocation_deleted_on_index ON manual_geolocations (deleted_on)');
  late final Index manualGeolocationClassifiedPeriodUuidIndex = Index(
      'manual_geolocation_classified_period_uuid_index',
      'CREATE INDEX manual_geolocation_classified_period_uuid_index ON manual_geolocations (classified_period_uuid)');
  late final Index classifiedPeriodsStartDateIndex = Index(
      'classified_periods_start_date_index',
      'CREATE INDEX classified_periods_start_date_index ON classified_periods (start_date)');
  late final Index classifiedPeriodsEndDateIndex = Index(
      'classified_periods_end_date_index',
      'CREATE INDEX classified_periods_end_date_index ON classified_periods (end_date)');
  late final Index classifiedPeriodsDeletedOnIndex = Index(
      'classified_periods_deleted_on_index',
      'CREATE INDEX classified_periods_deleted_on_index ON classified_periods (deleted_on)');
  late final Index sensorGeolocationsSyncedIndex = Index(
      'sensor_geolocations_synced_index',
      'CREATE INDEX sensor_geolocations_synced_index ON sensor_geolocations (synced)');
  late final Index classifiedPeriodsSyncedIndex = Index(
      'classified_periods_synced_index',
      'CREATE INDEX classified_periods_synced_index ON classified_periods (synced)');
  late final $LogsTable logs = $LogsTable(this);
  late final Index logsSyncedIndex = Index(
      'logs_synced_index', 'CREATE INDEX logs_synced_index ON logs (synced)');
  late final $ReasonsTable reasons = $ReasonsTable(this);
  late final $GoogleMapsDatasTable googleMapsDatas =
      $GoogleMapsDatasTable(this);
  late final $StopsTable stops = $StopsTable(this);
  late final Index stopsSyncedIndex = Index('stops_synced_index',
      'CREATE INDEX stops_synced_index ON stops (synced)');
  late final $VehiclesTable vehicles = $VehiclesTable(this);
  late final $MovementsTable movements = $MovementsTable(this);
  late final Index movementsSyncedIndex = Index('movements_synced_index',
      'CREATE INDEX movements_synced_index ON movements (synced)');
  late final Index googleMapsDatasSyncedIndex = Index(
      'google_maps_datas_synced_index',
      'CREATE INDEX google_maps_datas_synced_index ON google_maps_datas (synced)');
  late final Index manualGeolocationsSyncedIndex = Index(
      'manual_geolocations_synced_index',
      'CREATE INDEX manual_geolocations_synced_index ON manual_geolocations (synced)');
  late final Index trackedDaysSyncedIndex = Index('tracked_days_synced_index',
      'CREATE INDEX tracked_days_synced_index ON tracked_days (synced)');
  late final Index movementsClassifiedPeriodUuidIndex = Index(
      'movements_classified_period_uuid_index',
      'CREATE INDEX movements_classified_period_uuid_index ON movements (classified_period_uuid)');
  late final Index stopsClassifiedPeriodUuidIndex = Index(
      'stops_classified_period_uuid_index',
      'CREATE INDEX stops_classified_period_uuid_index ON stops (classified_period_uuid)');
  late final $DevicesTable devices = $DevicesTable(this);
  late final $TokensTable tokens = $TokensTable(this);
  late final ReasonDao reasonDao = ReasonDao(this as Database);
  late final VehicleDao vehicleDao = VehicleDao(this as Database);
  late final DevicesDao devicesDao = DevicesDao(this as Database);
  late final LogsDao logsDao = LogsDao(this as Database);
  late final TokensDao tokensDao = TokensDao(this as Database);
  late final SensorGeolocationDao sensorGeolocationDao =
      SensorGeolocationDao(this as Database);
  late final ManualGeolocationDao manualGeolocationDao =
      ManualGeolocationDao(this as Database);
  late final ClassifiedPeriodDao classifiedPeriodDao =
      ClassifiedPeriodDao(this as Database);
  late final ClassifiedPeriodDtoDao classifiedPeriodDtoDao =
      ClassifiedPeriodDtoDao(this as Database);
  late final TrackedDayDao trackedDayDao = TrackedDayDao(this as Database);
  late final MovementDao movementDao = MovementDao(this as Database);
  late final StopDao stopDao = StopDao(this as Database);
  late final ClusterDao clusterDao = ClusterDao(this as Database);
  late final GoogleMapsDataDao googleMapsDataDao =
      GoogleMapsDataDao(this as Database);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        trackedDays,
        classifiedPeriods,
        clusters,
        sensorGeolocations,
        sensorGeolocationCreatedOnIndex,
        sensorGeolocationDeletedOnIndex,
        sensorGeolocationIsNoiseIndex,
        sensorGeolocationSensorTypeIndex,
        sensorGeolocationAccuracyIndex,
        manualGeolocations,
        manualGeolocationCreatedOnIndex,
        manualGeolocationDeletedOnIndex,
        manualGeolocationClassifiedPeriodUuidIndex,
        classifiedPeriodsStartDateIndex,
        classifiedPeriodsEndDateIndex,
        classifiedPeriodsDeletedOnIndex,
        sensorGeolocationsSyncedIndex,
        classifiedPeriodsSyncedIndex,
        logs,
        logsSyncedIndex,
        reasons,
        googleMapsDatas,
        stops,
        stopsSyncedIndex,
        vehicles,
        movements,
        movementsSyncedIndex,
        googleMapsDatasSyncedIndex,
        manualGeolocationsSyncedIndex,
        trackedDaysSyncedIndex,
        movementsClassifiedPeriodUuidIndex,
        stopsClassifiedPeriodUuidIndex,
        devices,
        tokens
      ];
}

typedef $$TrackedDaysTableInsertCompanionBuilder = TrackedDaysCompanion
    Function({
  Value<String> uuid,
  required DateTime date,
  Value<bool> confirmed,
  Value<int?> choiceId,
  Value<String?> choiceText,
  Value<bool?> synced,
  Value<int> rowid,
});
typedef $$TrackedDaysTableUpdateCompanionBuilder = TrackedDaysCompanion
    Function({
  Value<String> uuid,
  Value<DateTime> date,
  Value<bool> confirmed,
  Value<int?> choiceId,
  Value<String?> choiceText,
  Value<bool?> synced,
  Value<int> rowid,
});

class $$TrackedDaysTableTableManager extends RootTableManager<
    _$Database,
    $TrackedDaysTable,
    TrackedDay,
    $$TrackedDaysTableFilterComposer,
    $$TrackedDaysTableOrderingComposer,
    $$TrackedDaysTableProcessedTableManager,
    $$TrackedDaysTableInsertCompanionBuilder,
    $$TrackedDaysTableUpdateCompanionBuilder> {
  $$TrackedDaysTableTableManager(_$Database db, $TrackedDaysTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$TrackedDaysTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$TrackedDaysTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$TrackedDaysTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<DateTime> date = const Value.absent(),
            Value<bool> confirmed = const Value.absent(),
            Value<int?> choiceId = const Value.absent(),
            Value<String?> choiceText = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              TrackedDaysCompanion(
            uuid: uuid,
            date: date,
            confirmed: confirmed,
            choiceId: choiceId,
            choiceText: choiceText,
            synced: synced,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            required DateTime date,
            Value<bool> confirmed = const Value.absent(),
            Value<int?> choiceId = const Value.absent(),
            Value<String?> choiceText = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              TrackedDaysCompanion.insert(
            uuid: uuid,
            date: date,
            confirmed: confirmed,
            choiceId: choiceId,
            choiceText: choiceText,
            synced: synced,
            rowid: rowid,
          ),
        ));
}

class $$TrackedDaysTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $TrackedDaysTable,
    TrackedDay,
    $$TrackedDaysTableFilterComposer,
    $$TrackedDaysTableOrderingComposer,
    $$TrackedDaysTableProcessedTableManager,
    $$TrackedDaysTableInsertCompanionBuilder,
    $$TrackedDaysTableUpdateCompanionBuilder> {
  $$TrackedDaysTableProcessedTableManager(super.$state);
}

class $$TrackedDaysTableFilterComposer
    extends FilterComposer<_$Database, $TrackedDaysTable> {
  $$TrackedDaysTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get date => $state.composableBuilder(
      column: $state.table.date,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get confirmed => $state.composableBuilder(
      column: $state.table.confirmed,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get choiceId => $state.composableBuilder(
      column: $state.table.choiceId,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get choiceText => $state.composableBuilder(
      column: $state.table.choiceText,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ComposableFilter classifiedPeriodsRefs(
      ComposableFilter Function($$ClassifiedPeriodsTableFilterComposer f) f) {
    final $$ClassifiedPeriodsTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.uuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.trackedDayUuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return f(composer);
  }
}

class $$TrackedDaysTableOrderingComposer
    extends OrderingComposer<_$Database, $TrackedDaysTable> {
  $$TrackedDaysTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get date => $state.composableBuilder(
      column: $state.table.date,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get confirmed => $state.composableBuilder(
      column: $state.table.confirmed,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get choiceId => $state.composableBuilder(
      column: $state.table.choiceId,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get choiceText => $state.composableBuilder(
      column: $state.table.choiceText,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$ClassifiedPeriodsTableInsertCompanionBuilder
    = ClassifiedPeriodsCompanion Function({
  Value<String> uuid,
  Value<String?> trackedDayUuid,
  Value<String?> origin,
  required DateTime startDate,
  required DateTime endDate,
  Value<bool> confirmed,
  required DateTime createdOn,
  Value<DateTime?> deletedOn,
  Value<bool> synced,
  Value<int> rowid,
});
typedef $$ClassifiedPeriodsTableUpdateCompanionBuilder
    = ClassifiedPeriodsCompanion Function({
  Value<String> uuid,
  Value<String?> trackedDayUuid,
  Value<String?> origin,
  Value<DateTime> startDate,
  Value<DateTime> endDate,
  Value<bool> confirmed,
  Value<DateTime> createdOn,
  Value<DateTime?> deletedOn,
  Value<bool> synced,
  Value<int> rowid,
});

class $$ClassifiedPeriodsTableTableManager extends RootTableManager<
    _$Database,
    $ClassifiedPeriodsTable,
    ClassifiedPeriod,
    $$ClassifiedPeriodsTableFilterComposer,
    $$ClassifiedPeriodsTableOrderingComposer,
    $$ClassifiedPeriodsTableProcessedTableManager,
    $$ClassifiedPeriodsTableInsertCompanionBuilder,
    $$ClassifiedPeriodsTableUpdateCompanionBuilder> {
  $$ClassifiedPeriodsTableTableManager(
      _$Database db, $ClassifiedPeriodsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$ClassifiedPeriodsTableFilterComposer(ComposerState(db, table)),
          orderingComposer: $$ClassifiedPeriodsTableOrderingComposer(
              ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$ClassifiedPeriodsTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String?> trackedDayUuid = const Value.absent(),
            Value<String?> origin = const Value.absent(),
            Value<DateTime> startDate = const Value.absent(),
            Value<DateTime> endDate = const Value.absent(),
            Value<bool> confirmed = const Value.absent(),
            Value<DateTime> createdOn = const Value.absent(),
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<bool> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ClassifiedPeriodsCompanion(
            uuid: uuid,
            trackedDayUuid: trackedDayUuid,
            origin: origin,
            startDate: startDate,
            endDate: endDate,
            confirmed: confirmed,
            createdOn: createdOn,
            deletedOn: deletedOn,
            synced: synced,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String?> trackedDayUuid = const Value.absent(),
            Value<String?> origin = const Value.absent(),
            required DateTime startDate,
            required DateTime endDate,
            Value<bool> confirmed = const Value.absent(),
            required DateTime createdOn,
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<bool> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ClassifiedPeriodsCompanion.insert(
            uuid: uuid,
            trackedDayUuid: trackedDayUuid,
            origin: origin,
            startDate: startDate,
            endDate: endDate,
            confirmed: confirmed,
            createdOn: createdOn,
            deletedOn: deletedOn,
            synced: synced,
            rowid: rowid,
          ),
        ));
}

class $$ClassifiedPeriodsTableProcessedTableManager
    extends ProcessedTableManager<
        _$Database,
        $ClassifiedPeriodsTable,
        ClassifiedPeriod,
        $$ClassifiedPeriodsTableFilterComposer,
        $$ClassifiedPeriodsTableOrderingComposer,
        $$ClassifiedPeriodsTableProcessedTableManager,
        $$ClassifiedPeriodsTableInsertCompanionBuilder,
        $$ClassifiedPeriodsTableUpdateCompanionBuilder> {
  $$ClassifiedPeriodsTableProcessedTableManager(super.$state);
}

class $$ClassifiedPeriodsTableFilterComposer
    extends FilterComposer<_$Database, $ClassifiedPeriodsTable> {
  $$ClassifiedPeriodsTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get origin => $state.composableBuilder(
      column: $state.table.origin,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get startDate => $state.composableBuilder(
      column: $state.table.startDate,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get endDate => $state.composableBuilder(
      column: $state.table.endDate,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get confirmed => $state.composableBuilder(
      column: $state.table.confirmed,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$TrackedDaysTableFilterComposer get trackedDayUuid {
    final $$TrackedDaysTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.trackedDayUuid,
        referencedTable: $state.db.trackedDays,
        getReferencedColumn: (t) => t.uuid,
        builder: (joinBuilder, parentComposers) =>
            $$TrackedDaysTableFilterComposer(ComposerState($state.db,
                $state.db.trackedDays, joinBuilder, parentComposers)));
    return composer;
  }

  ComposableFilter clustersRefs(
      ComposableFilter Function($$ClustersTableFilterComposer f) f) {
    final $$ClustersTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.uuid,
        referencedTable: $state.db.clusters,
        getReferencedColumn: (t) => t.classifiedPeriodUuid,
        builder: (joinBuilder, parentComposers) =>
            $$ClustersTableFilterComposer(ComposerState(
                $state.db, $state.db.clusters, joinBuilder, parentComposers)));
    return f(composer);
  }

  ComposableFilter sensorGeolocationsRefs(
      ComposableFilter Function($$SensorGeolocationsTableFilterComposer f) f) {
    final $$SensorGeolocationsTableFilterComposer composer = $state
        .composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.uuid,
            referencedTable: $state.db.sensorGeolocations,
            getReferencedColumn: (t) => t.classifiedPeriodUuid,
            builder: (joinBuilder, parentComposers) =>
                $$SensorGeolocationsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.sensorGeolocations,
                    joinBuilder,
                    parentComposers)));
    return f(composer);
  }

  ComposableFilter manualGeolocationsRefs(
      ComposableFilter Function($$ManualGeolocationsTableFilterComposer f) f) {
    final $$ManualGeolocationsTableFilterComposer composer = $state
        .composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.uuid,
            referencedTable: $state.db.manualGeolocations,
            getReferencedColumn: (t) => t.classifiedPeriodUuid,
            builder: (joinBuilder, parentComposers) =>
                $$ManualGeolocationsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.manualGeolocations,
                    joinBuilder,
                    parentComposers)));
    return f(composer);
  }

  ComposableFilter stopsRefs(
      ComposableFilter Function($$StopsTableFilterComposer f) f) {
    final $$StopsTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.uuid,
        referencedTable: $state.db.stops,
        getReferencedColumn: (t) => t.classifiedPeriodUuid,
        builder: (joinBuilder, parentComposers) => $$StopsTableFilterComposer(
            ComposerState(
                $state.db, $state.db.stops, joinBuilder, parentComposers)));
    return f(composer);
  }

  ComposableFilter movementsRefs(
      ComposableFilter Function($$MovementsTableFilterComposer f) f) {
    final $$MovementsTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.uuid,
        referencedTable: $state.db.movements,
        getReferencedColumn: (t) => t.classifiedPeriodUuid,
        builder: (joinBuilder, parentComposers) =>
            $$MovementsTableFilterComposer(ComposerState(
                $state.db, $state.db.movements, joinBuilder, parentComposers)));
    return f(composer);
  }
}

class $$ClassifiedPeriodsTableOrderingComposer
    extends OrderingComposer<_$Database, $ClassifiedPeriodsTable> {
  $$ClassifiedPeriodsTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get origin => $state.composableBuilder(
      column: $state.table.origin,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get startDate => $state.composableBuilder(
      column: $state.table.startDate,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get endDate => $state.composableBuilder(
      column: $state.table.endDate,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get confirmed => $state.composableBuilder(
      column: $state.table.confirmed,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$TrackedDaysTableOrderingComposer get trackedDayUuid {
    final $$TrackedDaysTableOrderingComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.trackedDayUuid,
        referencedTable: $state.db.trackedDays,
        getReferencedColumn: (t) => t.uuid,
        builder: (joinBuilder, parentComposers) =>
            $$TrackedDaysTableOrderingComposer(ComposerState($state.db,
                $state.db.trackedDays, joinBuilder, parentComposers)));
    return composer;
  }
}

typedef $$ClustersTableInsertCompanionBuilder = ClustersCompanion Function({
  Value<String> uuid,
  Value<String?> classifiedPeriodUuid,
  required DateTime startTime,
  required DateTime endTime,
  required DateTime createdOn,
  Value<int?> transport,
  required int averageSpeed,
  required double averageAccuracy,
  Value<DateTime?> deletedOn,
  Value<double?> trainProbability,
  Value<double?> tramProbability,
  Value<double?> subwayProbability,
  Value<double?> walkingProbability,
  Value<double?> carProbability,
  Value<double?> bicycleProbability,
  Value<bool> synced,
  Value<String?> userUuid,
  Value<int> rowid,
});
typedef $$ClustersTableUpdateCompanionBuilder = ClustersCompanion Function({
  Value<String> uuid,
  Value<String?> classifiedPeriodUuid,
  Value<DateTime> startTime,
  Value<DateTime> endTime,
  Value<DateTime> createdOn,
  Value<int?> transport,
  Value<int> averageSpeed,
  Value<double> averageAccuracy,
  Value<DateTime?> deletedOn,
  Value<double?> trainProbability,
  Value<double?> tramProbability,
  Value<double?> subwayProbability,
  Value<double?> walkingProbability,
  Value<double?> carProbability,
  Value<double?> bicycleProbability,
  Value<bool> synced,
  Value<String?> userUuid,
  Value<int> rowid,
});

class $$ClustersTableTableManager extends RootTableManager<
    _$Database,
    $ClustersTable,
    Cluster,
    $$ClustersTableFilterComposer,
    $$ClustersTableOrderingComposer,
    $$ClustersTableProcessedTableManager,
    $$ClustersTableInsertCompanionBuilder,
    $$ClustersTableUpdateCompanionBuilder> {
  $$ClustersTableTableManager(_$Database db, $ClustersTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$ClustersTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$ClustersTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$ClustersTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String?> classifiedPeriodUuid = const Value.absent(),
            Value<DateTime> startTime = const Value.absent(),
            Value<DateTime> endTime = const Value.absent(),
            Value<DateTime> createdOn = const Value.absent(),
            Value<int?> transport = const Value.absent(),
            Value<int> averageSpeed = const Value.absent(),
            Value<double> averageAccuracy = const Value.absent(),
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<double?> trainProbability = const Value.absent(),
            Value<double?> tramProbability = const Value.absent(),
            Value<double?> subwayProbability = const Value.absent(),
            Value<double?> walkingProbability = const Value.absent(),
            Value<double?> carProbability = const Value.absent(),
            Value<double?> bicycleProbability = const Value.absent(),
            Value<bool> synced = const Value.absent(),
            Value<String?> userUuid = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ClustersCompanion(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            startTime: startTime,
            endTime: endTime,
            createdOn: createdOn,
            transport: transport,
            averageSpeed: averageSpeed,
            averageAccuracy: averageAccuracy,
            deletedOn: deletedOn,
            trainProbability: trainProbability,
            tramProbability: tramProbability,
            subwayProbability: subwayProbability,
            walkingProbability: walkingProbability,
            carProbability: carProbability,
            bicycleProbability: bicycleProbability,
            synced: synced,
            userUuid: userUuid,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String?> classifiedPeriodUuid = const Value.absent(),
            required DateTime startTime,
            required DateTime endTime,
            required DateTime createdOn,
            Value<int?> transport = const Value.absent(),
            required int averageSpeed,
            required double averageAccuracy,
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<double?> trainProbability = const Value.absent(),
            Value<double?> tramProbability = const Value.absent(),
            Value<double?> subwayProbability = const Value.absent(),
            Value<double?> walkingProbability = const Value.absent(),
            Value<double?> carProbability = const Value.absent(),
            Value<double?> bicycleProbability = const Value.absent(),
            Value<bool> synced = const Value.absent(),
            Value<String?> userUuid = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ClustersCompanion.insert(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            startTime: startTime,
            endTime: endTime,
            createdOn: createdOn,
            transport: transport,
            averageSpeed: averageSpeed,
            averageAccuracy: averageAccuracy,
            deletedOn: deletedOn,
            trainProbability: trainProbability,
            tramProbability: tramProbability,
            subwayProbability: subwayProbability,
            walkingProbability: walkingProbability,
            carProbability: carProbability,
            bicycleProbability: bicycleProbability,
            synced: synced,
            userUuid: userUuid,
            rowid: rowid,
          ),
        ));
}

class $$ClustersTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $ClustersTable,
    Cluster,
    $$ClustersTableFilterComposer,
    $$ClustersTableOrderingComposer,
    $$ClustersTableProcessedTableManager,
    $$ClustersTableInsertCompanionBuilder,
    $$ClustersTableUpdateCompanionBuilder> {
  $$ClustersTableProcessedTableManager(super.$state);
}

class $$ClustersTableFilterComposer
    extends FilterComposer<_$Database, $ClustersTable> {
  $$ClustersTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get startTime => $state.composableBuilder(
      column: $state.table.startTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get endTime => $state.composableBuilder(
      column: $state.table.endTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get transport => $state.composableBuilder(
      column: $state.table.transport,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get averageSpeed => $state.composableBuilder(
      column: $state.table.averageSpeed,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get averageAccuracy => $state.composableBuilder(
      column: $state.table.averageAccuracy,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get trainProbability => $state.composableBuilder(
      column: $state.table.trainProbability,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get tramProbability => $state.composableBuilder(
      column: $state.table.tramProbability,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get subwayProbability => $state.composableBuilder(
      column: $state.table.subwayProbability,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get walkingProbability => $state.composableBuilder(
      column: $state.table.walkingProbability,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get carProbability => $state.composableBuilder(
      column: $state.table.carProbability,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get bicycleProbability => $state.composableBuilder(
      column: $state.table.bicycleProbability,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get userUuid => $state.composableBuilder(
      column: $state.table.userUuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableFilterComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }

  ComposableFilter sensorGeolocationsRefs(
      ComposableFilter Function($$SensorGeolocationsTableFilterComposer f) f) {
    final $$SensorGeolocationsTableFilterComposer composer = $state
        .composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.uuid,
            referencedTable: $state.db.sensorGeolocations,
            getReferencedColumn: (t) => t.clusterId,
            builder: (joinBuilder, parentComposers) =>
                $$SensorGeolocationsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.sensorGeolocations,
                    joinBuilder,
                    parentComposers)));
    return f(composer);
  }
}

class $$ClustersTableOrderingComposer
    extends OrderingComposer<_$Database, $ClustersTable> {
  $$ClustersTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get startTime => $state.composableBuilder(
      column: $state.table.startTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get endTime => $state.composableBuilder(
      column: $state.table.endTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get transport => $state.composableBuilder(
      column: $state.table.transport,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get averageSpeed => $state.composableBuilder(
      column: $state.table.averageSpeed,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get averageAccuracy => $state.composableBuilder(
      column: $state.table.averageAccuracy,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get trainProbability => $state.composableBuilder(
      column: $state.table.trainProbability,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get tramProbability => $state.composableBuilder(
      column: $state.table.tramProbability,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get subwayProbability => $state.composableBuilder(
      column: $state.table.subwayProbability,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get walkingProbability => $state.composableBuilder(
      column: $state.table.walkingProbability,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get carProbability => $state.composableBuilder(
      column: $state.table.carProbability,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get bicycleProbability => $state.composableBuilder(
      column: $state.table.bicycleProbability,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get userUuid => $state.composableBuilder(
      column: $state.table.userUuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableOrderingComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableOrderingComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableOrderingComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }
}

typedef $$SensorGeolocationsTableInsertCompanionBuilder
    = SensorGeolocationsCompanion Function({
  Value<String> uuid,
  required double latitude,
  required double longitude,
  required double altitude,
  required double bearing,
  required double accuracy,
  required double speed,
  required double calculatedSpeed,
  required double medianSpeed,
  required double distance,
  required String sensoryType,
  Value<String?> clusterId,
  Value<String?> classifiedPeriodUuid,
  Value<String> userId,
  required String provider,
  required bool isNoise,
  required int batteryLevel,
  required DateTime createdOn,
  Value<DateTime?> deletedOn,
  Value<bool?> synced,
  Value<int> rowid,
});
typedef $$SensorGeolocationsTableUpdateCompanionBuilder
    = SensorGeolocationsCompanion Function({
  Value<String> uuid,
  Value<double> latitude,
  Value<double> longitude,
  Value<double> altitude,
  Value<double> bearing,
  Value<double> accuracy,
  Value<double> speed,
  Value<double> calculatedSpeed,
  Value<double> medianSpeed,
  Value<double> distance,
  Value<String> sensoryType,
  Value<String?> clusterId,
  Value<String?> classifiedPeriodUuid,
  Value<String> userId,
  Value<String> provider,
  Value<bool> isNoise,
  Value<int> batteryLevel,
  Value<DateTime> createdOn,
  Value<DateTime?> deletedOn,
  Value<bool?> synced,
  Value<int> rowid,
});

class $$SensorGeolocationsTableTableManager extends RootTableManager<
    _$Database,
    $SensorGeolocationsTable,
    SensorGeolocation,
    $$SensorGeolocationsTableFilterComposer,
    $$SensorGeolocationsTableOrderingComposer,
    $$SensorGeolocationsTableProcessedTableManager,
    $$SensorGeolocationsTableInsertCompanionBuilder,
    $$SensorGeolocationsTableUpdateCompanionBuilder> {
  $$SensorGeolocationsTableTableManager(
      _$Database db, $SensorGeolocationsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$SensorGeolocationsTableFilterComposer(ComposerState(db, table)),
          orderingComposer: $$SensorGeolocationsTableOrderingComposer(
              ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$SensorGeolocationsTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<double> latitude = const Value.absent(),
            Value<double> longitude = const Value.absent(),
            Value<double> altitude = const Value.absent(),
            Value<double> bearing = const Value.absent(),
            Value<double> accuracy = const Value.absent(),
            Value<double> speed = const Value.absent(),
            Value<double> calculatedSpeed = const Value.absent(),
            Value<double> medianSpeed = const Value.absent(),
            Value<double> distance = const Value.absent(),
            Value<String> sensoryType = const Value.absent(),
            Value<String?> clusterId = const Value.absent(),
            Value<String?> classifiedPeriodUuid = const Value.absent(),
            Value<String> userId = const Value.absent(),
            Value<String> provider = const Value.absent(),
            Value<bool> isNoise = const Value.absent(),
            Value<int> batteryLevel = const Value.absent(),
            Value<DateTime> createdOn = const Value.absent(),
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              SensorGeolocationsCompanion(
            uuid: uuid,
            latitude: latitude,
            longitude: longitude,
            altitude: altitude,
            bearing: bearing,
            accuracy: accuracy,
            speed: speed,
            calculatedSpeed: calculatedSpeed,
            medianSpeed: medianSpeed,
            distance: distance,
            sensoryType: sensoryType,
            clusterId: clusterId,
            classifiedPeriodUuid: classifiedPeriodUuid,
            userId: userId,
            provider: provider,
            isNoise: isNoise,
            batteryLevel: batteryLevel,
            createdOn: createdOn,
            deletedOn: deletedOn,
            synced: synced,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            required double latitude,
            required double longitude,
            required double altitude,
            required double bearing,
            required double accuracy,
            required double speed,
            required double calculatedSpeed,
            required double medianSpeed,
            required double distance,
            required String sensoryType,
            Value<String?> clusterId = const Value.absent(),
            Value<String?> classifiedPeriodUuid = const Value.absent(),
            Value<String> userId = const Value.absent(),
            required String provider,
            required bool isNoise,
            required int batteryLevel,
            required DateTime createdOn,
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              SensorGeolocationsCompanion.insert(
            uuid: uuid,
            latitude: latitude,
            longitude: longitude,
            altitude: altitude,
            bearing: bearing,
            accuracy: accuracy,
            speed: speed,
            calculatedSpeed: calculatedSpeed,
            medianSpeed: medianSpeed,
            distance: distance,
            sensoryType: sensoryType,
            clusterId: clusterId,
            classifiedPeriodUuid: classifiedPeriodUuid,
            userId: userId,
            provider: provider,
            isNoise: isNoise,
            batteryLevel: batteryLevel,
            createdOn: createdOn,
            deletedOn: deletedOn,
            synced: synced,
            rowid: rowid,
          ),
        ));
}

class $$SensorGeolocationsTableProcessedTableManager
    extends ProcessedTableManager<
        _$Database,
        $SensorGeolocationsTable,
        SensorGeolocation,
        $$SensorGeolocationsTableFilterComposer,
        $$SensorGeolocationsTableOrderingComposer,
        $$SensorGeolocationsTableProcessedTableManager,
        $$SensorGeolocationsTableInsertCompanionBuilder,
        $$SensorGeolocationsTableUpdateCompanionBuilder> {
  $$SensorGeolocationsTableProcessedTableManager(super.$state);
}

class $$SensorGeolocationsTableFilterComposer
    extends FilterComposer<_$Database, $SensorGeolocationsTable> {
  $$SensorGeolocationsTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get latitude => $state.composableBuilder(
      column: $state.table.latitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get longitude => $state.composableBuilder(
      column: $state.table.longitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get altitude => $state.composableBuilder(
      column: $state.table.altitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get bearing => $state.composableBuilder(
      column: $state.table.bearing,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get accuracy => $state.composableBuilder(
      column: $state.table.accuracy,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get speed => $state.composableBuilder(
      column: $state.table.speed,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get calculatedSpeed => $state.composableBuilder(
      column: $state.table.calculatedSpeed,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get medianSpeed => $state.composableBuilder(
      column: $state.table.medianSpeed,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get distance => $state.composableBuilder(
      column: $state.table.distance,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get sensoryType => $state.composableBuilder(
      column: $state.table.sensoryType,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get userId => $state.composableBuilder(
      column: $state.table.userId,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get provider => $state.composableBuilder(
      column: $state.table.provider,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get isNoise => $state.composableBuilder(
      column: $state.table.isNoise,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get batteryLevel => $state.composableBuilder(
      column: $state.table.batteryLevel,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$ClustersTableFilterComposer get clusterId {
    final $$ClustersTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.clusterId,
        referencedTable: $state.db.clusters,
        getReferencedColumn: (t) => t.uuid,
        builder: (joinBuilder, parentComposers) =>
            $$ClustersTableFilterComposer(ComposerState(
                $state.db, $state.db.clusters, joinBuilder, parentComposers)));
    return composer;
  }

  $$ClassifiedPeriodsTableFilterComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }
}

class $$SensorGeolocationsTableOrderingComposer
    extends OrderingComposer<_$Database, $SensorGeolocationsTable> {
  $$SensorGeolocationsTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get latitude => $state.composableBuilder(
      column: $state.table.latitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get longitude => $state.composableBuilder(
      column: $state.table.longitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get altitude => $state.composableBuilder(
      column: $state.table.altitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get bearing => $state.composableBuilder(
      column: $state.table.bearing,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get accuracy => $state.composableBuilder(
      column: $state.table.accuracy,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get speed => $state.composableBuilder(
      column: $state.table.speed,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get calculatedSpeed => $state.composableBuilder(
      column: $state.table.calculatedSpeed,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get medianSpeed => $state.composableBuilder(
      column: $state.table.medianSpeed,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get distance => $state.composableBuilder(
      column: $state.table.distance,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get sensoryType => $state.composableBuilder(
      column: $state.table.sensoryType,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get userId => $state.composableBuilder(
      column: $state.table.userId,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get provider => $state.composableBuilder(
      column: $state.table.provider,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get isNoise => $state.composableBuilder(
      column: $state.table.isNoise,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get batteryLevel => $state.composableBuilder(
      column: $state.table.batteryLevel,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$ClustersTableOrderingComposer get clusterId {
    final $$ClustersTableOrderingComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.clusterId,
        referencedTable: $state.db.clusters,
        getReferencedColumn: (t) => t.uuid,
        builder: (joinBuilder, parentComposers) =>
            $$ClustersTableOrderingComposer(ComposerState(
                $state.db, $state.db.clusters, joinBuilder, parentComposers)));
    return composer;
  }

  $$ClassifiedPeriodsTableOrderingComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableOrderingComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableOrderingComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }
}

typedef $$ManualGeolocationsTableInsertCompanionBuilder
    = ManualGeolocationsCompanion Function({
  Value<String> uuid,
  required String classifiedPeriodUuid,
  required double latitude,
  required double longitude,
  required DateTime createdOn,
  Value<DateTime?> deletedOn,
  Value<bool?> synced,
  Value<int> rowid,
});
typedef $$ManualGeolocationsTableUpdateCompanionBuilder
    = ManualGeolocationsCompanion Function({
  Value<String> uuid,
  Value<String> classifiedPeriodUuid,
  Value<double> latitude,
  Value<double> longitude,
  Value<DateTime> createdOn,
  Value<DateTime?> deletedOn,
  Value<bool?> synced,
  Value<int> rowid,
});

class $$ManualGeolocationsTableTableManager extends RootTableManager<
    _$Database,
    $ManualGeolocationsTable,
    ManualGeolocation,
    $$ManualGeolocationsTableFilterComposer,
    $$ManualGeolocationsTableOrderingComposer,
    $$ManualGeolocationsTableProcessedTableManager,
    $$ManualGeolocationsTableInsertCompanionBuilder,
    $$ManualGeolocationsTableUpdateCompanionBuilder> {
  $$ManualGeolocationsTableTableManager(
      _$Database db, $ManualGeolocationsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$ManualGeolocationsTableFilterComposer(ComposerState(db, table)),
          orderingComposer: $$ManualGeolocationsTableOrderingComposer(
              ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$ManualGeolocationsTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String> classifiedPeriodUuid = const Value.absent(),
            Value<double> latitude = const Value.absent(),
            Value<double> longitude = const Value.absent(),
            Value<DateTime> createdOn = const Value.absent(),
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ManualGeolocationsCompanion(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            latitude: latitude,
            longitude: longitude,
            createdOn: createdOn,
            deletedOn: deletedOn,
            synced: synced,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            required String classifiedPeriodUuid,
            required double latitude,
            required double longitude,
            required DateTime createdOn,
            Value<DateTime?> deletedOn = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ManualGeolocationsCompanion.insert(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            latitude: latitude,
            longitude: longitude,
            createdOn: createdOn,
            deletedOn: deletedOn,
            synced: synced,
            rowid: rowid,
          ),
        ));
}

class $$ManualGeolocationsTableProcessedTableManager
    extends ProcessedTableManager<
        _$Database,
        $ManualGeolocationsTable,
        ManualGeolocation,
        $$ManualGeolocationsTableFilterComposer,
        $$ManualGeolocationsTableOrderingComposer,
        $$ManualGeolocationsTableProcessedTableManager,
        $$ManualGeolocationsTableInsertCompanionBuilder,
        $$ManualGeolocationsTableUpdateCompanionBuilder> {
  $$ManualGeolocationsTableProcessedTableManager(super.$state);
}

class $$ManualGeolocationsTableFilterComposer
    extends FilterComposer<_$Database, $ManualGeolocationsTable> {
  $$ManualGeolocationsTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get latitude => $state.composableBuilder(
      column: $state.table.latitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get longitude => $state.composableBuilder(
      column: $state.table.longitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableFilterComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }
}

class $$ManualGeolocationsTableOrderingComposer
    extends OrderingComposer<_$Database, $ManualGeolocationsTable> {
  $$ManualGeolocationsTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get latitude => $state.composableBuilder(
      column: $state.table.latitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get longitude => $state.composableBuilder(
      column: $state.table.longitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get createdOn => $state.composableBuilder(
      column: $state.table.createdOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get deletedOn => $state.composableBuilder(
      column: $state.table.deletedOn,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableOrderingComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableOrderingComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableOrderingComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }
}

typedef $$LogsTableInsertCompanionBuilder = LogsCompanion Function({
  Value<int?> id,
  Value<String?> message,
  Value<String?> description,
  Value<String?> type,
  Value<DateTime?> date,
  Value<bool?> synced,
});
typedef $$LogsTableUpdateCompanionBuilder = LogsCompanion Function({
  Value<int?> id,
  Value<String?> message,
  Value<String?> description,
  Value<String?> type,
  Value<DateTime?> date,
  Value<bool?> synced,
});

class $$LogsTableTableManager extends RootTableManager<
    _$Database,
    $LogsTable,
    Log,
    $$LogsTableFilterComposer,
    $$LogsTableOrderingComposer,
    $$LogsTableProcessedTableManager,
    $$LogsTableInsertCompanionBuilder,
    $$LogsTableUpdateCompanionBuilder> {
  $$LogsTableTableManager(_$Database db, $LogsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$LogsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$LogsTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $$LogsTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> message = const Value.absent(),
            Value<String?> description = const Value.absent(),
            Value<String?> type = const Value.absent(),
            Value<DateTime?> date = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
          }) =>
              LogsCompanion(
            id: id,
            message: message,
            description: description,
            type: type,
            date: date,
            synced: synced,
          ),
          getInsertCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> message = const Value.absent(),
            Value<String?> description = const Value.absent(),
            Value<String?> type = const Value.absent(),
            Value<DateTime?> date = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
          }) =>
              LogsCompanion.insert(
            id: id,
            message: message,
            description: description,
            type: type,
            date: date,
            synced: synced,
          ),
        ));
}

class $$LogsTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $LogsTable,
    Log,
    $$LogsTableFilterComposer,
    $$LogsTableOrderingComposer,
    $$LogsTableProcessedTableManager,
    $$LogsTableInsertCompanionBuilder,
    $$LogsTableUpdateCompanionBuilder> {
  $$LogsTableProcessedTableManager(super.$state);
}

class $$LogsTableFilterComposer extends FilterComposer<_$Database, $LogsTable> {
  $$LogsTableFilterComposer(super.$state);
  ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get message => $state.composableBuilder(
      column: $state.table.message,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get description => $state.composableBuilder(
      column: $state.table.description,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get type => $state.composableBuilder(
      column: $state.table.type,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get date => $state.composableBuilder(
      column: $state.table.date,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$LogsTableOrderingComposer
    extends OrderingComposer<_$Database, $LogsTable> {
  $$LogsTableOrderingComposer(super.$state);
  ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get message => $state.composableBuilder(
      column: $state.table.message,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get description => $state.composableBuilder(
      column: $state.table.description,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get type => $state.composableBuilder(
      column: $state.table.type,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get date => $state.composableBuilder(
      column: $state.table.date,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$ReasonsTableInsertCompanionBuilder = ReasonsCompanion Function({
  Value<int?> id,
  Value<String?> name,
  Value<String?> icon,
  Value<String?> color,
});
typedef $$ReasonsTableUpdateCompanionBuilder = ReasonsCompanion Function({
  Value<int?> id,
  Value<String?> name,
  Value<String?> icon,
  Value<String?> color,
});

class $$ReasonsTableTableManager extends RootTableManager<
    _$Database,
    $ReasonsTable,
    Reason,
    $$ReasonsTableFilterComposer,
    $$ReasonsTableOrderingComposer,
    $$ReasonsTableProcessedTableManager,
    $$ReasonsTableInsertCompanionBuilder,
    $$ReasonsTableUpdateCompanionBuilder> {
  $$ReasonsTableTableManager(_$Database db, $ReasonsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$ReasonsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$ReasonsTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $$ReasonsTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<String?> icon = const Value.absent(),
            Value<String?> color = const Value.absent(),
          }) =>
              ReasonsCompanion(
            id: id,
            name: name,
            icon: icon,
            color: color,
          ),
          getInsertCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<String?> icon = const Value.absent(),
            Value<String?> color = const Value.absent(),
          }) =>
              ReasonsCompanion.insert(
            id: id,
            name: name,
            icon: icon,
            color: color,
          ),
        ));
}

class $$ReasonsTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $ReasonsTable,
    Reason,
    $$ReasonsTableFilterComposer,
    $$ReasonsTableOrderingComposer,
    $$ReasonsTableProcessedTableManager,
    $$ReasonsTableInsertCompanionBuilder,
    $$ReasonsTableUpdateCompanionBuilder> {
  $$ReasonsTableProcessedTableManager(super.$state);
}

class $$ReasonsTableFilterComposer
    extends FilterComposer<_$Database, $ReasonsTable> {
  $$ReasonsTableFilterComposer(super.$state);
  ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get icon => $state.composableBuilder(
      column: $state.table.icon,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get color => $state.composableBuilder(
      column: $state.table.color,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ComposableFilter stopsRefs(
      ComposableFilter Function($$StopsTableFilterComposer f) f) {
    final $$StopsTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.id,
        referencedTable: $state.db.stops,
        getReferencedColumn: (t) => t.reasonId,
        builder: (joinBuilder, parentComposers) => $$StopsTableFilterComposer(
            ComposerState(
                $state.db, $state.db.stops, joinBuilder, parentComposers)));
    return f(composer);
  }
}

class $$ReasonsTableOrderingComposer
    extends OrderingComposer<_$Database, $ReasonsTable> {
  $$ReasonsTableOrderingComposer(super.$state);
  ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get icon => $state.composableBuilder(
      column: $state.table.icon,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get color => $state.composableBuilder(
      column: $state.table.color,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$GoogleMapsDatasTableInsertCompanionBuilder = GoogleMapsDatasCompanion
    Function({
  Value<String> uuid,
  Value<String?> googleId,
  Value<String?> address,
  Value<String?> city,
  Value<String?> postcode,
  Value<String?> country,
  Value<String?> name,
  Value<bool?> synced,
  Value<int> rowid,
});
typedef $$GoogleMapsDatasTableUpdateCompanionBuilder = GoogleMapsDatasCompanion
    Function({
  Value<String> uuid,
  Value<String?> googleId,
  Value<String?> address,
  Value<String?> city,
  Value<String?> postcode,
  Value<String?> country,
  Value<String?> name,
  Value<bool?> synced,
  Value<int> rowid,
});

class $$GoogleMapsDatasTableTableManager extends RootTableManager<
    _$Database,
    $GoogleMapsDatasTable,
    GoogleMapsData,
    $$GoogleMapsDatasTableFilterComposer,
    $$GoogleMapsDatasTableOrderingComposer,
    $$GoogleMapsDatasTableProcessedTableManager,
    $$GoogleMapsDatasTableInsertCompanionBuilder,
    $$GoogleMapsDatasTableUpdateCompanionBuilder> {
  $$GoogleMapsDatasTableTableManager(_$Database db, $GoogleMapsDatasTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$GoogleMapsDatasTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$GoogleMapsDatasTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$GoogleMapsDatasTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String?> googleId = const Value.absent(),
            Value<String?> address = const Value.absent(),
            Value<String?> city = const Value.absent(),
            Value<String?> postcode = const Value.absent(),
            Value<String?> country = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              GoogleMapsDatasCompanion(
            uuid: uuid,
            googleId: googleId,
            address: address,
            city: city,
            postcode: postcode,
            country: country,
            name: name,
            synced: synced,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String?> googleId = const Value.absent(),
            Value<String?> address = const Value.absent(),
            Value<String?> city = const Value.absent(),
            Value<String?> postcode = const Value.absent(),
            Value<String?> country = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              GoogleMapsDatasCompanion.insert(
            uuid: uuid,
            googleId: googleId,
            address: address,
            city: city,
            postcode: postcode,
            country: country,
            name: name,
            synced: synced,
            rowid: rowid,
          ),
        ));
}

class $$GoogleMapsDatasTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $GoogleMapsDatasTable,
    GoogleMapsData,
    $$GoogleMapsDatasTableFilterComposer,
    $$GoogleMapsDatasTableOrderingComposer,
    $$GoogleMapsDatasTableProcessedTableManager,
    $$GoogleMapsDatasTableInsertCompanionBuilder,
    $$GoogleMapsDatasTableUpdateCompanionBuilder> {
  $$GoogleMapsDatasTableProcessedTableManager(super.$state);
}

class $$GoogleMapsDatasTableFilterComposer
    extends FilterComposer<_$Database, $GoogleMapsDatasTable> {
  $$GoogleMapsDatasTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get googleId => $state.composableBuilder(
      column: $state.table.googleId,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get address => $state.composableBuilder(
      column: $state.table.address,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get city => $state.composableBuilder(
      column: $state.table.city,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get postcode => $state.composableBuilder(
      column: $state.table.postcode,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get country => $state.composableBuilder(
      column: $state.table.country,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ComposableFilter stopsRefs(
      ComposableFilter Function($$StopsTableFilterComposer f) f) {
    final $$StopsTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.uuid,
        referencedTable: $state.db.stops,
        getReferencedColumn: (t) => t.googleMapsDataUuid,
        builder: (joinBuilder, parentComposers) => $$StopsTableFilterComposer(
            ComposerState(
                $state.db, $state.db.stops, joinBuilder, parentComposers)));
    return f(composer);
  }
}

class $$GoogleMapsDatasTableOrderingComposer
    extends OrderingComposer<_$Database, $GoogleMapsDatasTable> {
  $$GoogleMapsDatasTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get googleId => $state.composableBuilder(
      column: $state.table.googleId,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get address => $state.composableBuilder(
      column: $state.table.address,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get city => $state.composableBuilder(
      column: $state.table.city,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get postcode => $state.composableBuilder(
      column: $state.table.postcode,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get country => $state.composableBuilder(
      column: $state.table.country,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$StopsTableInsertCompanionBuilder = StopsCompanion Function({
  Value<String> uuid,
  required String classifiedPeriodUuid,
  Value<int?> reasonId,
  Value<String?> googleMapsDataUuid,
  Value<bool?> synced,
  Value<int> rowid,
});
typedef $$StopsTableUpdateCompanionBuilder = StopsCompanion Function({
  Value<String> uuid,
  Value<String> classifiedPeriodUuid,
  Value<int?> reasonId,
  Value<String?> googleMapsDataUuid,
  Value<bool?> synced,
  Value<int> rowid,
});

class $$StopsTableTableManager extends RootTableManager<
    _$Database,
    $StopsTable,
    Stop,
    $$StopsTableFilterComposer,
    $$StopsTableOrderingComposer,
    $$StopsTableProcessedTableManager,
    $$StopsTableInsertCompanionBuilder,
    $$StopsTableUpdateCompanionBuilder> {
  $$StopsTableTableManager(_$Database db, $StopsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$StopsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$StopsTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $$StopsTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String> classifiedPeriodUuid = const Value.absent(),
            Value<int?> reasonId = const Value.absent(),
            Value<String?> googleMapsDataUuid = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              StopsCompanion(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            reasonId: reasonId,
            googleMapsDataUuid: googleMapsDataUuid,
            synced: synced,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            required String classifiedPeriodUuid,
            Value<int?> reasonId = const Value.absent(),
            Value<String?> googleMapsDataUuid = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              StopsCompanion.insert(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            reasonId: reasonId,
            googleMapsDataUuid: googleMapsDataUuid,
            synced: synced,
            rowid: rowid,
          ),
        ));
}

class $$StopsTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $StopsTable,
    Stop,
    $$StopsTableFilterComposer,
    $$StopsTableOrderingComposer,
    $$StopsTableProcessedTableManager,
    $$StopsTableInsertCompanionBuilder,
    $$StopsTableUpdateCompanionBuilder> {
  $$StopsTableProcessedTableManager(super.$state);
}

class $$StopsTableFilterComposer
    extends FilterComposer<_$Database, $StopsTable> {
  $$StopsTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableFilterComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }

  $$ReasonsTableFilterComposer get reasonId {
    final $$ReasonsTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.reasonId,
        referencedTable: $state.db.reasons,
        getReferencedColumn: (t) => t.id,
        builder: (joinBuilder, parentComposers) => $$ReasonsTableFilterComposer(
            ComposerState(
                $state.db, $state.db.reasons, joinBuilder, parentComposers)));
    return composer;
  }

  $$GoogleMapsDatasTableFilterComposer get googleMapsDataUuid {
    final $$GoogleMapsDatasTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.googleMapsDataUuid,
            referencedTable: $state.db.googleMapsDatas,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$GoogleMapsDatasTableFilterComposer(ComposerState($state.db,
                    $state.db.googleMapsDatas, joinBuilder, parentComposers)));
    return composer;
  }
}

class $$StopsTableOrderingComposer
    extends OrderingComposer<_$Database, $StopsTable> {
  $$StopsTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableOrderingComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableOrderingComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableOrderingComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }

  $$ReasonsTableOrderingComposer get reasonId {
    final $$ReasonsTableOrderingComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.reasonId,
        referencedTable: $state.db.reasons,
        getReferencedColumn: (t) => t.id,
        builder: (joinBuilder, parentComposers) =>
            $$ReasonsTableOrderingComposer(ComposerState(
                $state.db, $state.db.reasons, joinBuilder, parentComposers)));
    return composer;
  }

  $$GoogleMapsDatasTableOrderingComposer get googleMapsDataUuid {
    final $$GoogleMapsDatasTableOrderingComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.googleMapsDataUuid,
            referencedTable: $state.db.googleMapsDatas,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$GoogleMapsDatasTableOrderingComposer(ComposerState($state.db,
                    $state.db.googleMapsDatas, joinBuilder, parentComposers)));
    return composer;
  }
}

typedef $$VehiclesTableInsertCompanionBuilder = VehiclesCompanion Function({
  Value<int?> id,
  Value<String?> name,
  Value<String?> icon,
  Value<String?> color,
});
typedef $$VehiclesTableUpdateCompanionBuilder = VehiclesCompanion Function({
  Value<int?> id,
  Value<String?> name,
  Value<String?> icon,
  Value<String?> color,
});

class $$VehiclesTableTableManager extends RootTableManager<
    _$Database,
    $VehiclesTable,
    Vehicle,
    $$VehiclesTableFilterComposer,
    $$VehiclesTableOrderingComposer,
    $$VehiclesTableProcessedTableManager,
    $$VehiclesTableInsertCompanionBuilder,
    $$VehiclesTableUpdateCompanionBuilder> {
  $$VehiclesTableTableManager(_$Database db, $VehiclesTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$VehiclesTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$VehiclesTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$VehiclesTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<String?> icon = const Value.absent(),
            Value<String?> color = const Value.absent(),
          }) =>
              VehiclesCompanion(
            id: id,
            name: name,
            icon: icon,
            color: color,
          ),
          getInsertCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<String?> icon = const Value.absent(),
            Value<String?> color = const Value.absent(),
          }) =>
              VehiclesCompanion.insert(
            id: id,
            name: name,
            icon: icon,
            color: color,
          ),
        ));
}

class $$VehiclesTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $VehiclesTable,
    Vehicle,
    $$VehiclesTableFilterComposer,
    $$VehiclesTableOrderingComposer,
    $$VehiclesTableProcessedTableManager,
    $$VehiclesTableInsertCompanionBuilder,
    $$VehiclesTableUpdateCompanionBuilder> {
  $$VehiclesTableProcessedTableManager(super.$state);
}

class $$VehiclesTableFilterComposer
    extends FilterComposer<_$Database, $VehiclesTable> {
  $$VehiclesTableFilterComposer(super.$state);
  ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get icon => $state.composableBuilder(
      column: $state.table.icon,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get color => $state.composableBuilder(
      column: $state.table.color,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ComposableFilter movementsRefs(
      ComposableFilter Function($$MovementsTableFilterComposer f) f) {
    final $$MovementsTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.id,
        referencedTable: $state.db.movements,
        getReferencedColumn: (t) => t.vehicleId,
        builder: (joinBuilder, parentComposers) =>
            $$MovementsTableFilterComposer(ComposerState(
                $state.db, $state.db.movements, joinBuilder, parentComposers)));
    return f(composer);
  }
}

class $$VehiclesTableOrderingComposer
    extends OrderingComposer<_$Database, $VehiclesTable> {
  $$VehiclesTableOrderingComposer(super.$state);
  ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get icon => $state.composableBuilder(
      column: $state.table.icon,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get color => $state.composableBuilder(
      column: $state.table.color,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$MovementsTableInsertCompanionBuilder = MovementsCompanion Function({
  Value<String> uuid,
  required String classifiedPeriodUuid,
  Value<int?> vehicleId,
  Value<int?> transport,
  required int clusterCount,
  Value<bool?> synced,
  Value<int> rowid,
});
typedef $$MovementsTableUpdateCompanionBuilder = MovementsCompanion Function({
  Value<String> uuid,
  Value<String> classifiedPeriodUuid,
  Value<int?> vehicleId,
  Value<int?> transport,
  Value<int> clusterCount,
  Value<bool?> synced,
  Value<int> rowid,
});

class $$MovementsTableTableManager extends RootTableManager<
    _$Database,
    $MovementsTable,
    Movement,
    $$MovementsTableFilterComposer,
    $$MovementsTableOrderingComposer,
    $$MovementsTableProcessedTableManager,
    $$MovementsTableInsertCompanionBuilder,
    $$MovementsTableUpdateCompanionBuilder> {
  $$MovementsTableTableManager(_$Database db, $MovementsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$MovementsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$MovementsTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$MovementsTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String> classifiedPeriodUuid = const Value.absent(),
            Value<int?> vehicleId = const Value.absent(),
            Value<int?> transport = const Value.absent(),
            Value<int> clusterCount = const Value.absent(),
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              MovementsCompanion(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            vehicleId: vehicleId,
            transport: transport,
            clusterCount: clusterCount,
            synced: synced,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            required String classifiedPeriodUuid,
            Value<int?> vehicleId = const Value.absent(),
            Value<int?> transport = const Value.absent(),
            required int clusterCount,
            Value<bool?> synced = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              MovementsCompanion.insert(
            uuid: uuid,
            classifiedPeriodUuid: classifiedPeriodUuid,
            vehicleId: vehicleId,
            transport: transport,
            clusterCount: clusterCount,
            synced: synced,
            rowid: rowid,
          ),
        ));
}

class $$MovementsTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $MovementsTable,
    Movement,
    $$MovementsTableFilterComposer,
    $$MovementsTableOrderingComposer,
    $$MovementsTableProcessedTableManager,
    $$MovementsTableInsertCompanionBuilder,
    $$MovementsTableUpdateCompanionBuilder> {
  $$MovementsTableProcessedTableManager(super.$state);
}

class $$MovementsTableFilterComposer
    extends FilterComposer<_$Database, $MovementsTable> {
  $$MovementsTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get transport => $state.composableBuilder(
      column: $state.table.transport,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get clusterCount => $state.composableBuilder(
      column: $state.table.clusterCount,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableFilterComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableFilterComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }

  $$VehiclesTableFilterComposer get vehicleId {
    final $$VehiclesTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.vehicleId,
        referencedTable: $state.db.vehicles,
        getReferencedColumn: (t) => t.id,
        builder: (joinBuilder, parentComposers) =>
            $$VehiclesTableFilterComposer(ComposerState(
                $state.db, $state.db.vehicles, joinBuilder, parentComposers)));
    return composer;
  }
}

class $$MovementsTableOrderingComposer
    extends OrderingComposer<_$Database, $MovementsTable> {
  $$MovementsTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get transport => $state.composableBuilder(
      column: $state.table.transport,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get clusterCount => $state.composableBuilder(
      column: $state.table.clusterCount,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get synced => $state.composableBuilder(
      column: $state.table.synced,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$ClassifiedPeriodsTableOrderingComposer get classifiedPeriodUuid {
    final $$ClassifiedPeriodsTableOrderingComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.classifiedPeriodUuid,
            referencedTable: $state.db.classifiedPeriods,
            getReferencedColumn: (t) => t.uuid,
            builder: (joinBuilder, parentComposers) =>
                $$ClassifiedPeriodsTableOrderingComposer(ComposerState(
                    $state.db,
                    $state.db.classifiedPeriods,
                    joinBuilder,
                    parentComposers)));
    return composer;
  }

  $$VehiclesTableOrderingComposer get vehicleId {
    final $$VehiclesTableOrderingComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.vehicleId,
        referencedTable: $state.db.vehicles,
        getReferencedColumn: (t) => t.id,
        builder: (joinBuilder, parentComposers) =>
            $$VehiclesTableOrderingComposer(ComposerState(
                $state.db, $state.db.vehicles, joinBuilder, parentComposers)));
    return composer;
  }
}

typedef $$DevicesTableInsertCompanionBuilder = DevicesCompanion Function({
  Value<int?> id,
  Value<String?> device,
  Value<String?> version,
  Value<String?> product,
  Value<String?> model,
  Value<String?> brand,
  Value<String?> androidId,
  Value<String?> secureId,
  Value<String?> sdk,
  Value<double?> width,
  Value<double?> height,
  Value<double?> widthLogical,
  Value<double?> heightLogical,
  Value<bool> sensorLock,
  Value<bool> logLock,
});
typedef $$DevicesTableUpdateCompanionBuilder = DevicesCompanion Function({
  Value<int?> id,
  Value<String?> device,
  Value<String?> version,
  Value<String?> product,
  Value<String?> model,
  Value<String?> brand,
  Value<String?> androidId,
  Value<String?> secureId,
  Value<String?> sdk,
  Value<double?> width,
  Value<double?> height,
  Value<double?> widthLogical,
  Value<double?> heightLogical,
  Value<bool> sensorLock,
  Value<bool> logLock,
});

class $$DevicesTableTableManager extends RootTableManager<
    _$Database,
    $DevicesTable,
    Device,
    $$DevicesTableFilterComposer,
    $$DevicesTableOrderingComposer,
    $$DevicesTableProcessedTableManager,
    $$DevicesTableInsertCompanionBuilder,
    $$DevicesTableUpdateCompanionBuilder> {
  $$DevicesTableTableManager(_$Database db, $DevicesTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$DevicesTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$DevicesTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $$DevicesTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> device = const Value.absent(),
            Value<String?> version = const Value.absent(),
            Value<String?> product = const Value.absent(),
            Value<String?> model = const Value.absent(),
            Value<String?> brand = const Value.absent(),
            Value<String?> androidId = const Value.absent(),
            Value<String?> secureId = const Value.absent(),
            Value<String?> sdk = const Value.absent(),
            Value<double?> width = const Value.absent(),
            Value<double?> height = const Value.absent(),
            Value<double?> widthLogical = const Value.absent(),
            Value<double?> heightLogical = const Value.absent(),
            Value<bool> sensorLock = const Value.absent(),
            Value<bool> logLock = const Value.absent(),
          }) =>
              DevicesCompanion(
            id: id,
            device: device,
            version: version,
            product: product,
            model: model,
            brand: brand,
            androidId: androidId,
            secureId: secureId,
            sdk: sdk,
            width: width,
            height: height,
            widthLogical: widthLogical,
            heightLogical: heightLogical,
            sensorLock: sensorLock,
            logLock: logLock,
          ),
          getInsertCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String?> device = const Value.absent(),
            Value<String?> version = const Value.absent(),
            Value<String?> product = const Value.absent(),
            Value<String?> model = const Value.absent(),
            Value<String?> brand = const Value.absent(),
            Value<String?> androidId = const Value.absent(),
            Value<String?> secureId = const Value.absent(),
            Value<String?> sdk = const Value.absent(),
            Value<double?> width = const Value.absent(),
            Value<double?> height = const Value.absent(),
            Value<double?> widthLogical = const Value.absent(),
            Value<double?> heightLogical = const Value.absent(),
            Value<bool> sensorLock = const Value.absent(),
            Value<bool> logLock = const Value.absent(),
          }) =>
              DevicesCompanion.insert(
            id: id,
            device: device,
            version: version,
            product: product,
            model: model,
            brand: brand,
            androidId: androidId,
            secureId: secureId,
            sdk: sdk,
            width: width,
            height: height,
            widthLogical: widthLogical,
            heightLogical: heightLogical,
            sensorLock: sensorLock,
            logLock: logLock,
          ),
        ));
}

class $$DevicesTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $DevicesTable,
    Device,
    $$DevicesTableFilterComposer,
    $$DevicesTableOrderingComposer,
    $$DevicesTableProcessedTableManager,
    $$DevicesTableInsertCompanionBuilder,
    $$DevicesTableUpdateCompanionBuilder> {
  $$DevicesTableProcessedTableManager(super.$state);
}

class $$DevicesTableFilterComposer
    extends FilterComposer<_$Database, $DevicesTable> {
  $$DevicesTableFilterComposer(super.$state);
  ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get device => $state.composableBuilder(
      column: $state.table.device,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get version => $state.composableBuilder(
      column: $state.table.version,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get product => $state.composableBuilder(
      column: $state.table.product,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get model => $state.composableBuilder(
      column: $state.table.model,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get brand => $state.composableBuilder(
      column: $state.table.brand,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get androidId => $state.composableBuilder(
      column: $state.table.androidId,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get secureId => $state.composableBuilder(
      column: $state.table.secureId,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get sdk => $state.composableBuilder(
      column: $state.table.sdk,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get width => $state.composableBuilder(
      column: $state.table.width,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get height => $state.composableBuilder(
      column: $state.table.height,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get widthLogical => $state.composableBuilder(
      column: $state.table.widthLogical,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get heightLogical => $state.composableBuilder(
      column: $state.table.heightLogical,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get sensorLock => $state.composableBuilder(
      column: $state.table.sensorLock,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get logLock => $state.composableBuilder(
      column: $state.table.logLock,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$DevicesTableOrderingComposer
    extends OrderingComposer<_$Database, $DevicesTable> {
  $$DevicesTableOrderingComposer(super.$state);
  ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get device => $state.composableBuilder(
      column: $state.table.device,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get version => $state.composableBuilder(
      column: $state.table.version,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get product => $state.composableBuilder(
      column: $state.table.product,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get model => $state.composableBuilder(
      column: $state.table.model,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get brand => $state.composableBuilder(
      column: $state.table.brand,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get androidId => $state.composableBuilder(
      column: $state.table.androidId,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get secureId => $state.composableBuilder(
      column: $state.table.secureId,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get sdk => $state.composableBuilder(
      column: $state.table.sdk,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get width => $state.composableBuilder(
      column: $state.table.width,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get height => $state.composableBuilder(
      column: $state.table.height,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get widthLogical => $state.composableBuilder(
      column: $state.table.widthLogical,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get heightLogical => $state.composableBuilder(
      column: $state.table.heightLogical,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get sensorLock => $state.composableBuilder(
      column: $state.table.sensorLock,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get logLock => $state.composableBuilder(
      column: $state.table.logLock,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$TokensTableInsertCompanionBuilder = TokensCompanion Function({
  Value<int?> id,
  required String authToken,
});
typedef $$TokensTableUpdateCompanionBuilder = TokensCompanion Function({
  Value<int?> id,
  Value<String> authToken,
});

class $$TokensTableTableManager extends RootTableManager<
    _$Database,
    $TokensTable,
    Token,
    $$TokensTableFilterComposer,
    $$TokensTableOrderingComposer,
    $$TokensTableProcessedTableManager,
    $$TokensTableInsertCompanionBuilder,
    $$TokensTableUpdateCompanionBuilder> {
  $$TokensTableTableManager(_$Database db, $TokensTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$TokensTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$TokensTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $$TokensTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            Value<String> authToken = const Value.absent(),
          }) =>
              TokensCompanion(
            id: id,
            authToken: authToken,
          ),
          getInsertCompanionBuilder: ({
            Value<int?> id = const Value.absent(),
            required String authToken,
          }) =>
              TokensCompanion.insert(
            id: id,
            authToken: authToken,
          ),
        ));
}

class $$TokensTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $TokensTable,
    Token,
    $$TokensTableFilterComposer,
    $$TokensTableOrderingComposer,
    $$TokensTableProcessedTableManager,
    $$TokensTableInsertCompanionBuilder,
    $$TokensTableUpdateCompanionBuilder> {
  $$TokensTableProcessedTableManager(super.$state);
}

class $$TokensTableFilterComposer
    extends FilterComposer<_$Database, $TokensTable> {
  $$TokensTableFilterComposer(super.$state);
  ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get authToken => $state.composableBuilder(
      column: $state.table.authToken,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$TokensTableOrderingComposer
    extends OrderingComposer<_$Database, $TokensTable> {
  $$TokensTableOrderingComposer(super.$state);
  ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get authToken => $state.composableBuilder(
      column: $state.table.authToken,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

class _$DatabaseManager {
  final _$Database _db;
  _$DatabaseManager(this._db);
  $$TrackedDaysTableTableManager get trackedDays =>
      $$TrackedDaysTableTableManager(_db, _db.trackedDays);
  $$ClassifiedPeriodsTableTableManager get classifiedPeriods =>
      $$ClassifiedPeriodsTableTableManager(_db, _db.classifiedPeriods);
  $$ClustersTableTableManager get clusters =>
      $$ClustersTableTableManager(_db, _db.clusters);
  $$SensorGeolocationsTableTableManager get sensorGeolocations =>
      $$SensorGeolocationsTableTableManager(_db, _db.sensorGeolocations);
  $$ManualGeolocationsTableTableManager get manualGeolocations =>
      $$ManualGeolocationsTableTableManager(_db, _db.manualGeolocations);
  $$LogsTableTableManager get logs => $$LogsTableTableManager(_db, _db.logs);
  $$ReasonsTableTableManager get reasons =>
      $$ReasonsTableTableManager(_db, _db.reasons);
  $$GoogleMapsDatasTableTableManager get googleMapsDatas =>
      $$GoogleMapsDatasTableTableManager(_db, _db.googleMapsDatas);
  $$StopsTableTableManager get stops =>
      $$StopsTableTableManager(_db, _db.stops);
  $$VehiclesTableTableManager get vehicles =>
      $$VehiclesTableTableManager(_db, _db.vehicles);
  $$MovementsTableTableManager get movements =>
      $$MovementsTableTableManager(_db, _db.movements);
  $$DevicesTableTableManager get devices =>
      $$DevicesTableTableManager(_db, _db.devices);
  $$TokensTableTableManager get tokens =>
      $$TokensTableTableManager(_db, _db.tokens);
}
