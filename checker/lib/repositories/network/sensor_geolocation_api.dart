import '../database/database.dart';
import 'base_api.dart';
import 'dtos/parsed_response.dart';

class SensorGeolocationApi extends BaseApi {
  SensorGeolocationApi(Database database) : super('sensorGeolocation/', database);

  Future<ParsedResponse<List<SensorGeolocation>>> syncSensorGeolocation(List<SensorGeolocation> sensorGeolocations) async {
    final payload = sensorGeolocations.map((sensorGeolocation) => sensorGeolocation.toJson()).toList();
    return getParsedResponse<List<SensorGeolocation>, SensorGeolocation>(
      'bulkInsert',
      SensorGeolocation.fromJson,
      payload: payload,
    );
  }

  Future<ParsedResponse<List<SensorGeolocation>>> updateSensorLocations(List<SensorGeolocation> sensorGeolocations) async {
    final payload = sensorGeolocations.map((sensorGeolocation) => sensorGeolocation.toJson()).toList();
    return getParsedResponse<List<SensorGeolocation>, SensorGeolocation>(
      'bulkUpdate',
      SensorGeolocation.fromJson,
      payload: payload,
    );
  }

  Future<ParsedResponse<List<SensorGeolocation>>> getLocations() async {
    return getParsedResponse<List<SensorGeolocation>, SensorGeolocation>(
      'getLocations',
      SensorGeolocation.fromJson,
    );
  }
}
