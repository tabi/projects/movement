import '../database/database.dart';
import 'base_api.dart';
import 'dtos/parsed_response.dart';

class ClassifiedPeriodApi extends BaseApi {
  ClassifiedPeriodApi(Database database) : super('classifiedPeriod/', database);

  Future<ParsedResponse<ClassifiedPeriod?>> sync(ClassifiedPeriod classifiedPeriod) async => this.getParsedResponse<ClassifiedPeriod, ClassifiedPeriod>(
        'upsert',
        ClassifiedPeriod.fromJson,
        payload: classifiedPeriod,
      );
}
