import '../database/database.dart';
import 'base_api.dart';
import 'dtos/parsed_response.dart';

class ClusterApi extends BaseApi {
  ClusterApi(Database database) : super('cluster/', database);

  Future<ParsedResponse<List<Cluster>>> syncClusters(List<Cluster> clusters) async {
    final payload = clusters.map((cluster) => cluster.toJson()).toList();
    return getParsedResponse<List<Cluster>, Cluster>(
      'bulkInsert',
      Cluster.fromJson,
      payload: payload,
    );
  }

  Future<ParsedResponse<List<Cluster>>> syncAnonymousClusters(List<Cluster> clusters) async {
    final payload = clusters.map((cluster) => cluster.toJson()).toList();
    return getParsedResponse<List<Cluster>, Cluster>(
      'bulkInsertAnonymous',
      Cluster.fromJson,
      payload: payload,
    );
  }
}
