// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'probable_transport_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProbableTransportDTO _$ProbableTransportDTOFromJson(
        Map<String, dynamic> json) =>
    ProbableTransportDTO(
      transport: $enumDecode(_$TransportEnumMap, json['transport']),
      probability: (json['probability'] as num).toDouble(),
    );

Map<String, dynamic> _$ProbableTransportDTOToJson(
        ProbableTransportDTO instance) =>
    <String, dynamic>{
      'transport': _$TransportEnumMap[instance.transport]!,
      'probability': instance.probability,
    };

const _$TransportEnumMap = {
  Transport.Walking: 'Walking',
  Transport.Bicycle: 'Bicycle',
  Transport.BicycleElectric: 'BicycleElectric',
  Transport.Bus: 'Bus',
  Transport.Tram: 'Tram',
  Transport.Subway: 'Subway',
  Transport.Train: 'Train',
  Transport.Car: 'Car',
  Transport.CarPassenger: 'CarPassenger',
  Transport.Scooter: 'Scooter',
  Transport.Motor: 'Motor',
  Transport.PickupTruck: 'PickupTruck',
  Transport.Onderway: 'Onderway',
  Transport.Unknown: 'Unknown',
};
