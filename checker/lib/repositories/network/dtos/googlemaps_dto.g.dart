// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'googlemaps_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GoogleMapsDTO _$GoogleMapsDTOFromJson(Map<String, dynamic> json) =>
    GoogleMapsDTO(
      placeId: json['placeId'] as String,
      lat: (json['lat'] as num?)?.toDouble(),
      lon: (json['lon'] as num?)?.toDouble(),
      displayName: json['displayName'] as String?,
      address: AddressDTO.fromJson(json['address'] as String),
    );

Map<String, dynamic> _$GoogleMapsDTOToJson(GoogleMapsDTO instance) =>
    <String, dynamic>{
      'placeId': instance.placeId,
      'lat': instance.lat,
      'lon': instance.lon,
      'displayName': instance.displayName,
      'address': instance.address,
    };
