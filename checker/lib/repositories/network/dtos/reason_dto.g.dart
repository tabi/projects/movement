// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reason_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReasonDTO _$ReasonDTOFromJson(Map<String, dynamic> json) => ReasonDTO(
      id: (json['id'] as num).toInt(),
      name: json['name'] as String,
      icon: json['icon'] as String,
      color: json['color'] as String?,
    );

Map<String, dynamic> _$ReasonDTOToJson(ReasonDTO instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'icon': instance.icon,
      'color': instance.color,
    };
