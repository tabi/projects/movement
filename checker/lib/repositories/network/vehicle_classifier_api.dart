import 'package:checker/repositories/network/dtos/vehicle_cluster_dto.dart';

import '../database/database.dart';
import 'base_api.dart';
import 'dtos/parsed_response.dart';
import 'dtos/probable_transport_dto.dart';

class VehicleClassifierApi extends BaseApi {
  VehicleClassifierApi(Database database) : super('vehicleClassifier/', database);

  Future<ParsedResponse<ProbableTransportDTO>> classifyWalking(VehicleClusterDto cluster) async =>
      getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyWalking', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyBicycle(VehicleClusterDto cluster) async =>
      getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyBicycle', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyCar(VehicleClusterDto cluster) async =>
      getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyCar', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyTram(VehicleClusterDto cluster) async =>
      getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyTram', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifyTrain(VehicleClusterDto cluster) async =>
      getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifyTrain', ProbableTransportDTO.fromMap, payload: cluster);

  Future<ParsedResponse<ProbableTransportDTO>> classifySubway(VehicleClusterDto cluster) async =>
      getParsedResponse<ProbableTransportDTO, ProbableTransportDTO>('classifySubway', ProbableTransportDTO.fromMap, payload: cluster);
}
