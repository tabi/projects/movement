import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

import '../repositories/database/database.dart';
import '../repositories/network/dtos/location_map_dto.dart';

class LocationMap extends StatelessWidget {
  const LocationMap(this.markers, {super.key});

  final List<Marker> markers;

  @override
  Widget build(BuildContext context) {
    return _buildLocationList(context, markers);
  }

  Widget _buildLocationList(BuildContext context, List<Marker> markers) {
    if (markers.isEmpty) {
      return Container();
    }

    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.45,
        width: MediaQuery.of(context).size.width * 0.22,
        child: Stack(children: [
          FlutterMap(
            options: MapOptions(
              onTap: (tapPosition, point) => print(point),
              center: LatLng(markers.last.point.latitude, markers.last.point.longitude),
              zoom: 13.0,
            ),
            children: [
              TileLayer(urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", subdomains: ['a', 'b', 'c']),
              MarkerLayer(
                markers: markers,
              ),
              const PolylineLayer(polylines: []),
            ],
          ),
        ]));
  }
}
