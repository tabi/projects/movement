import 'package:checker/repositories/movement_repository.dart';
import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

import '../../repositories/classified_period_repository.dart';
import '../../repositories/database/database.dart';
import '../../repositories/network/dtos/enums/transport_enum.dart';
import '../../repositories/network/dtos/movement_dto.dart';
import '../../repositories/network/dtos/probable_transport_dto.dart';
import '../../repositories/sensor_repository.dart';
import 'cluster_classifier.dart';

import 'dart:math';

class MovementClassifier {
  final SensorRepository _sensorRepository;
  final ClassifiedPeriodRepository _classifierRepository;
  final MovementRepository _movementRepository;
  final ClusterClassifier _clusterClassifier;
  final Database _database;

  MovementClassifier(this._sensorRepository, this._classifierRepository, this._clusterClassifier, this._database, this._movementRepository);

  Future<void> processSensorGeolocation({
    required double latitude,
    required double longitude,
    required int createdOn,
    required double accuracy,
    required double altitude,
    required double bearing,
    required double speed,
    required String sensorType,
    required String provider,
    required int batteryLevel,
  }) async {
    var calculatedSpeed = 0.0;
    var lastSensorLocation = await _database.sensorGeolocationDao.getLastValidSensorGeolocation();
    if (lastSensorLocation != null) {
      var distance = _calculateDistance(lastSensorLocation.latitude, lastSensorLocation.longitude, latitude, longitude);
      calculatedSpeed = _calculateSpeed(lastSensorLocation.createdOn, DateTime.fromMillisecondsSinceEpoch(createdOn), distance);
    }

    var sensorGeolocation = SensorGeolocationsCompanion.insert(
        uuid: Value(const Uuid().v4()),
        latitude: latitude,
        longitude: longitude,
        accuracy: accuracy,
        altitude: altitude,
        bearing: bearing,
        speed: speed,
        provider: provider,
        isNoise: false,
        createdOn: DateTime.now(),
        synced: Value(false),
        sensoryType: "",
        userId: Value(""),
        batteryLevel: batteryLevel,
        calculatedSpeed: calculatedSpeed,
        medianSpeed: 0.0,
        distance: 0.0);

    await _database.sensorGeolocationDao.addSensorGeolocation(sensorGeolocation);

    await _clusterClassifier.processGeolocation();

    var unprocessedClusters = await _database.clusterDao.getClustersWithNoMovement();
    var lastMovement = await _database.classifiedPeriodDtoDao.getLastClassifiedPeriodDto();
    processClusters(unprocessedClusters, lastMovement as MovementDto?);
  }

  Future<void> processLocation(SensorGeolocation location) async {
    await _database.sensorGeolocationDao.addLocation(location);

    await _clusterClassifier.processGeolocation();

    // var unprocessedClusters = await _database.clusterDao.getClustersWithNoMovement();
    // var lastMovement = await _database.classifiedPeriodDtoDao.getLastClassifiedPeriodDto();
    // await processClusters(unprocessedClusters, lastMovement as MovementDto?);
  }

  Future processClusters(List<Cluster> clusters, MovementDto? lastMovement) async {
    var clusterList = List<Cluster>.empty(growable: true);

    //Check if there are enough clusters, atleast 2
    if (clusters.length < 2) {
      return;
    }

    for (var cluster in clusters) {
      //If it's the first cluster, just add it tot the list
      if (clusterList.isEmpty) {
        var transport = getHighestProbabilityByCluster(cluster);
        cluster = cluster.copyWith(transport: Value(transport.index));

        clusterList.add(cluster);
        continue;
      }

      var previousCluster = clusterList.length > 2 ? clusterList[clusterList.length - 2] : clusterList[clusterList.length - 1];

      //If it's the the vehicle is the same as the previous cluster, just add it to the list.
      if (getHighestProbability(clusterList.last, previousCluster) == getHighestProbability(cluster, clusterList.last)) {
        var transport = getHighestProbability(cluster, clusterList.last);
        cluster = cluster.copyWith(transport: Value(transport.index));
        clusterList.add(cluster);
      } else {
        var transport = getHighestProbability(clusterList.last, clusterList[clusterList.length - 1]);

        cluster = cluster.copyWith(transport: Value(transport.index));

        //Check if previous movement was walking and if the current cluster is walking
        //if so, merge the movements together
        if (lastMovement != null &&
            lastMovement.transport == transport &&
            lastMovement.transport == Transport.Walking &&
            lastMovement.classifiedPeriod.endDate.difference(lastMovement.classifiedPeriod.startDate).inMinutes < 10) {
          var locations = <SensorGeolocation>[];

          for (final cluster in clusterList) {
            final clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(cluster.uuid);
            locations.addAll(clusterLocations);
          }

          locations.addAll(lastMovement.sensorGeolocations);

          var classifiedPeriod = ClassifiedPeriod(
              uuid: const Uuid().v4(),
              startDate: lastMovement.classifiedPeriod.startDate,
              endDate: clusterList.last.endTime,
              confirmed: false,
              createdOn: DateTime.now(),
              synced: false);

          var movementDto = MovementDto(
              movementUuid: const Uuid().v4(),
              transport: transport,
              sensorGeolocations: locations,
              manualGeolocations: lastMovement.manualGeolocations,
              vehicle: null,
              classifiedPeriod: classifiedPeriod,
              clusterCount: clusterList.length + lastMovement.clusterCount,
              clusters: clusterList);

          await _movementRepository.updateDeepMovement(movementDto, lastMovement);
        } else {
          if (clusterList.length == 1 && clusterList.first.endTime.difference(clusterList.first.startTime).inMinutes < 5) {
            transport = Transport.Walking;
            cluster = cluster.copyWith(transport: Value(transport.index));
          }

          //Merge last movement with the current clusters
          if (lastMovement != null && lastMovement.transport == Transport.Walking && transport == Transport.Walking) {
            var locations = <SensorGeolocation>[];
            var allClusters = <Cluster>[];

            for (final cluster in lastMovement.clusters) {
              final clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(cluster.uuid);
              locations.addAll(clusterLocations);
            }

            for (final cluster in clusterList) {
              final clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(cluster.uuid);
              locations.addAll(clusterLocations);
            }

            allClusters.addAll(lastMovement.clusters);
            allClusters.addAll(clusterList);

            var newMovement = lastMovement.copyWith(
                clusterCount: clusterList.length + lastMovement.clusterCount,
                sensorGeolocations: locations,
                classifiedPeriod: lastMovement.classifiedPeriod.copyWith(endDate: clusterList.last.endTime, uuid: const Uuid().v4()),
                transport: transport,
                clusters: allClusters);

            await _movementRepository.updateDeepMovement(newMovement, lastMovement);
            return;
          } else {
            var locations = <SensorGeolocation>[];

            for (final cluster in clusterList) {
              final clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(cluster.uuid);
              locations.addAll(clusterLocations);
            }

            var classifiedPeriod = ClassifiedPeriod(
                uuid: const Uuid().v4(),
                startDate: clusterList.first.startTime,
                endDate: clusterList.last.endTime,
                confirmed: false,
                createdOn: DateTime.now(),
                synced: false);

            var movementDto = MovementDto(
                movementUuid: const Uuid().v4(),
                transport: transport,
                sensorGeolocations: locations,
                manualGeolocations: [],
                vehicle: null,
                classifiedPeriod: classifiedPeriod,
                clusterCount: clusterList.length,
                clusters: clusterList);

            await _movementRepository.addDeepMovement(movementDto);
            return;
          }
        }

        clusterList.clear();
        clusterList.add(cluster);
      }
    }

    if (clusterList.isNotEmpty && lastMovement != null) {
      var lastMovementDifference = lastMovement.classifiedPeriod.endDate.difference(clusterList.first.startTime);
      var transport = getHighestProbability(clusterList.last, clusterList[clusterList.length - 1]);

      if (lastMovement.transport == transport && lastMovement.transport == Transport.Walking && lastMovementDifference.inMinutes < 10) {
        var locations = <SensorGeolocation>[];

        for (final cluster in clusterList) {
          final clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(cluster.uuid);
          locations.addAll(clusterLocations);
        }

        var newMovement = lastMovement.copyWith(
            clusterCount: clusterList.length,
            sensorGeolocations: locations,
            classifiedPeriod: lastMovement.classifiedPeriod.copyWith(endDate: clusterList.last.endTime, uuid: const Uuid().v4()),
            transport: transport,
            clusters: clusterList);

        await _movementRepository.updateDeepMovement(newMovement, lastMovement);
        return;
      } else {
        var locations = <SensorGeolocation>[];

        for (final cluster in clusterList) {
          final clusterLocations = await _database.sensorGeolocationDao.getSensorGeolocationsFromCluster(cluster.uuid);
          locations.addAll(clusterLocations);
        }

        var classifiedPeriod = ClassifiedPeriod(
            uuid: const Uuid().v4(),
            startDate: clusterList.first.startTime,
            endDate: clusterList.last.endTime,
            confirmed: false,
            createdOn: DateTime.now(),
            synced: true);

        var movementDto = MovementDto(
            movementUuid: const Uuid().v4(),
            transport: transport,
            sensorGeolocations: locations,
            manualGeolocations: [],
            vehicle: null,
            classifiedPeriod: classifiedPeriod,
            clusterCount: clusterList.length,
            clusters: clusterList);

        await _movementRepository.addDeepMovement(movementDto);
        return;
      }
    }
  }

  Transport getHighestProbability(Cluster cluster, Cluster previousCluster) {
    if (cluster.carProbability == null) {
      return Transport.Walking;
    }

    var list = List<ProbableTransportDTO>.empty(growable: true);
    var highestProbality = 0.0;
    final List<ProbableTransportDTO> probableTransports = [
      ProbableTransportDTO(probability: cluster.trainProbability!, transport: Transport.Train),
      ProbableTransportDTO(probability: cluster.tramProbability!, transport: Transport.Tram),
      ProbableTransportDTO(probability: cluster.subwayProbability!, transport: Transport.Subway),
      ProbableTransportDTO(probability: cluster.walkingProbability!, transport: Transport.Walking),
      ProbableTransportDTO(probability: cluster.carProbability!, transport: Transport.Car),
      ProbableTransportDTO(probability: cluster.bicycleProbability!, transport: Transport.Bicycle),
    ];

    for (var probableTransport in probableTransports) {
      if (probableTransport.probability > highestProbality) {
        highestProbality = probableTransport.probability;
        list.clear();
        list.add(probableTransport);
        continue;
      }

      if (probableTransport.probability == highestProbality) {
        list.add(probableTransport);
      }
    }

    if (list.length == 1) {
      return list.first.transport;
    }

    var biasedTransportIndex = list.indexWhere((element) => element.transport == previousCluster.transport);

    if (biasedTransportIndex != -1) {
      return list[biasedTransportIndex].transport;
    }

    return list.first.transport;
  }

  Transport getHighestProbabilityByCluster(Cluster cluster) {
    if (cluster.carProbability == null) {
      return Transport.Walking;
    }

    var highestProbability = 0.0;
    ProbableTransportDTO? highestTransport = null;

    final List<ProbableTransportDTO> probableTransports = [
      ProbableTransportDTO(probability: cluster.trainProbability!, transport: Transport.Train),
      ProbableTransportDTO(probability: cluster.tramProbability!, transport: Transport.Tram),
      ProbableTransportDTO(probability: cluster.subwayProbability!, transport: Transport.Subway),
      ProbableTransportDTO(probability: cluster.walkingProbability!, transport: Transport.Walking),
      ProbableTransportDTO(probability: cluster.carProbability!, transport: Transport.Car),
      ProbableTransportDTO(probability: cluster.bicycleProbability!, transport: Transport.Bicycle),
    ];

    for (final probableTransport in probableTransports) {
      if (probableTransport.probability > highestProbability) {
        highestProbability = probableTransport.probability;
        highestTransport = probableTransport;
      }
    }

    return highestTransport!.transport;
  }

  double _calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * asin(sqrt(a)) * 1000;
  }

  double _calculateSpeed(DateTime startTime, DateTime endTime, double distance) {
    double timeDifferenceInSeconds = (endTime.millisecondsSinceEpoch - startTime.millisecondsSinceEpoch) / 1000;
    double speedInMetersPerSecond = distance / timeDifferenceInSeconds;
    double speedInKilometersPerHour = speedInMetersPerSecond.isNaN || speedInMetersPerSecond.isInfinite ? 0 : speedInMetersPerSecond * 3.6;

    return speedInKilometersPerHour;
  }
}
