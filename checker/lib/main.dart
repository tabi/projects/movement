import 'package:checker/providers.dart';
import 'package:checker/repositories/network/dtos/cluster_dto.dart';
import 'package:checker/repositories/network/dtos/location_map_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';
import 'package:path/path.dart';
import 'package:window_manager/window_manager.dart';

import 'repositories/database/database.dart';
import 'repositories/network/dtos/enums/transport_enum.dart';
import 'widgets/LineChartSample2.dart';
import 'widgets/location_map.dart';

void main() async {
  runApp(const ProviderScope(child: MyApp()));

  WidgetsFlutterBinding.ensureInitialized();
// Must add this line.
  await windowManager.ensureInitialized();

// Use it only after calling `hiddenWindowAtLaunch`
  windowManager.waitUntilReadyToShow().then((_) async {
// Hide window title bar
    await windowManager.setFullScreen(true);
    await windowManager.center();
    await windowManager.show();
    await windowManager.setSkipTaskbar(false);
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends ConsumerWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var notifier = ref.read(classifierNotifierProvider.notifier);

    //notifier.calculateAndSaveLocations();
    //notifier.calculateAndSaveMedianLocations();

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        ),
        body: SingleChildScrollView(
          child: SizedBox(
            child: Center(
              child: Column(children: [
                //Charts for showing the speed
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _buildMedianCalculatedSpeedNormalChart(context),
                    _buildCalculatedSpeedNormalChart(context),
                  ],
                ),
                //Map to show point below and above 8 km/h to map potential clusters
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[_buildRawMap(context), _buildMovementMap(context), _buildNoMovementMap(context)],
                ),
                //map to points blow and above 8 km/h in median speed to map potential clusters
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[_buildRawMap(context), _buildMedianMovementMap(context), _buildMedianNoMovementMap(context)],
                ),
                //TODO:: map with polylines to show movements clusters
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[_buildRawMap(context), _buildMedianMovementMap(context), _buildMedianNoMovementMap(context)],
                ),
                //TODO:: show map with movementclusters with the potential vehicle probabilities
                Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[_buildClusterDetails(context)])
              ]),
            ),
          ),
        ));
  }

  Widget _buildRawMap(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);

      return Column(children: [const Text("Raw normal"), const SizedBox(height: 10), LocationMap(_getRawMarkers(state.locations))]);
    });
  }

  Widget _buildMovementMap(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);

      return Column(children: [const Text("Movement normal"), const SizedBox(height: 10), LocationMap(_getMovementMarkers(state.locations))]);
    });
  }

  Widget _buildNoMovementMap(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);

      return Column(children: [const Text("No Movement normal"), const SizedBox(height: 10), LocationMap(_getNoMovementMarkers(state.locations))]);
    });
  }

  Widget _buildMedianMovementMap(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);

      return Column(children: [const Text("Movement Median normal"), const SizedBox(height: 10), LocationMap(_getMedianMovementMarkers(state.locations))]);
    });
  }

  Widget _buildMedianNoMovementMap(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);

      return Column(children: [const Text("No Median Movement normal"), const SizedBox(height: 10), LocationMap(_getMedianNoMovementMarkers(state.locations))]);
    });
  }

  Widget _buildCalculatedSpeedNormalChart(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);

      return SizedBox(
          width: MediaQuery.of(context).size.width * 0.25,
          height: MediaQuery.of(context).size.height * 0.3,
          child: Column(
            children: [
              const Text("Calculated speed normal"),
              const SizedBox(height: 50),
              SimpleTimeSeriesChart.createLineCart(state.locations
                  .map((e) => TimeSeriesPing(e.createdOn, e.calculatedSpeed.isNaN || e.calculatedSpeed.isInfinite ? 0 : e.calculatedSpeed.toInt()))
                  .toList()),
            ],
          ));
    });
  }

  Widget _buildMedianCalculatedSpeedNormalChart(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);

      return SizedBox(
          width: MediaQuery.of(context).size.width * 0.25,
          height: MediaQuery.of(context).size.height * 0.3,
          child: Column(
            children: [
              const Text("Calculated speed normal"),
              const SizedBox(height: 50),
              SimpleTimeSeriesChart.createLineCart(state.locations
                  .map((e) => TimeSeriesPing(e.createdOn, e.medianSpeed.isNaN || e.medianSpeed.isInfinite ? 0 : e.medianSpeed.toInt()))
                  .toList()),
            ],
          ));
    });
  }

  Widget _buildClusterDetails(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final state = ref.watch(classifierNotifierProvider);
      final notifier = ref.watch(classifierNotifierProvider.notifier);

      try {
        List<Widget> list = state.clustersDto
            .map((c) => Row(children: [
                  Text(
                      "StartTijd: ${c.startTime}, Endtijd: ${c.endTime}, Points: ${c.sensorGeolocations.length}, AverageSpeed: ${c.averageSpeed}, AverageAccuracy: ${c.averageAccuracy.round()}, CarProb: ${c.carProbability}, TramProb: ${c.tramProbability}}, TrainProb: ${c.trainProbability}, MetroProb: ${c.subwayProbability}"),
                ]))
            .cast<Widget>()
            .toList();
        return SizedBox(
            child: Column(
          children: list,
        ));
      } catch (ex) {
        print(ex);
        return Container();
      }
    });
  }

  List<Marker> _getRawMarkers(List<SensorGeolocation> locations) {
    var markers = <Marker>[];

    for (var i = 0; i < locations.length; i++) {
      var dto = locations[i];

      var marker = Marker(
          width: 5.0,
          height: 5.0,
          point: LatLng(locations[i].latitude, locations[i].longitude),
          child: InkWell(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
            ),
            onTap: () {
              print(dto);
            },
          ));

      markers.add(marker);
    }

    return markers;
  }

  List<Marker> _getNoMovementMarkers(List<SensorGeolocation> locations) {
    var markers = <Marker>[];

    for (var i = 0; i < locations.length; i++) {
      if (locations[i].calculatedSpeed > 8) {
        continue;
      }

      var marker = Marker(
          width: 5.0,
          height: 5.0,
          point: LatLng(locations[i].latitude, locations[i].longitude),
          child: InkWell(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
            ),
            onTap: () {
              print(locations[i]);
            },
          ));

      markers.add(marker);
    }

    return markers;
  }

  List<Marker> _getMovementMarkers(List<SensorGeolocation> locations) {
    var markers = <Marker>[];

    for (var i = 0; i < locations.length; i++) {
      if (locations[i].calculatedSpeed <= 8) {
        continue;
      }

      var marker = Marker(
          width: 5.0,
          height: 5.0,
          point: LatLng(locations[i].latitude, locations[i].longitude),
          child: InkWell(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
            ),
            onTap: () {
              print(locations[i]);
            },
          ));

      markers.add(marker);
    }

    return markers;
  }

  List<Marker> _getMedianNoMovementMarkers(List<SensorGeolocation> locations) {
    var markers = <Marker>[];

    for (var i = 0; i < locations.length; i++) {
      if (locations[i].medianSpeed > 8) {
        continue;
      }

      var marker = Marker(
          width: 5.0,
          height: 5.0,
          point: LatLng(locations[i].latitude, locations[i].longitude),
          child: InkWell(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
            ),
            onTap: () {
              print(locations[i]);
            },
          ));

      markers.add(marker);
    }

    return markers;
  }

  List<Marker> _getMedianMovementMarkers(List<SensorGeolocation> locations) {
    var markers = <Marker>[];

    for (var i = 0; i < locations.length; i++) {
      if (locations[i].medianSpeed <= 8) {
        continue;
      }

      var marker = Marker(
          width: 5.0,
          height: 5.0,
          point: LatLng(locations[i].latitude, locations[i].longitude),
          child: InkWell(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
            ),
            onTap: () {
              print(locations[i]);
            },
          ));

      markers.add(marker);
    }

    return markers;
  }
}
