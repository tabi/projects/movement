import 'package:checker/notifiers/classifier_notifier.dart';
import 'package:checker/repositories/movement_repository.dart';
import 'package:checker/repositories/network/cluster_api.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'repositories/classified_period_repository.dart';
import 'repositories/database/database.dart';
import 'repositories/network/classified_period_api.dart';
import 'repositories/network/sensor_geolocation_api.dart';
import 'repositories/network/vehicle_classifier_api.dart';
import 'repositories/sensor_repository.dart';
import 'services/movement_classifier/cluster_classifier.dart';
import 'services/movement_classifier/movement_classifier.dart';

//Notifiers
final classifierNotifierProvider = ChangeNotifierProvider(
    (ref) => ClassifierNotifier(ref.watch(movementClassifierServiceProvider), ref.watch(sensorGeolocationApi), ref.watch(clusterApi), ref.watch(database)));

//Classifier
final clusterClassifierProvider = Provider<ClusterClassifier>(
  (ref) => ClusterClassifier(ref.watch(database), ref.watch(vehicleClassifierApi)),
);

final movementClassifierServiceProvider = Provider<MovementClassifier>(
  (ref) => MovementClassifier(ref.watch(sensorRepository), ref.watch(classifiedPeriodRepository), ref.watch(clusterClassifierProvider), ref.watch(database),
      ref.watch(movementRepository)),
);

//Databases
final database = Provider<Database>(
  (ref) => Database(),
);

//Network
final sensorGeolocationApi = Provider<SensorGeolocationApi>((ref) => SensorGeolocationApi(ref.watch(database)));
final clusterApi = Provider<ClusterApi>((ref) => ClusterApi(ref.watch(database)));
final classifiedPeriodApi = Provider<ClassifiedPeriodApi>((ref) => ClassifiedPeriodApi(ref.watch(database)));
final vehicleClassifierApi = Provider<VehicleClassifierApi>((ref) => VehicleClassifierApi(ref.watch(database)));

//Repositories
final sensorRepository = Provider<SensorRepository>(
  (ref) => SensorRepository(ref.watch(database), ref.watch(sensorGeolocationApi)),
);

final classifiedPeriodRepository = Provider<ClassifiedPeriodRepository>(
  (ref) => ClassifiedPeriodRepository(ref.watch(database), ref.watch(classifiedPeriodApi)),
);

final movementRepository = Provider<MovementRepository>(
  (ref) => MovementRepository(ref.watch(database)),
);
