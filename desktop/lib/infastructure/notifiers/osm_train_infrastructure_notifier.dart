import 'package:desktop/infastructure/repositories/network/osm_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';

import '../../presentation/theme/icon_mapper.dart';
import '../repositories/dtos/location_map_dto.dart';
import '../repositories/dtos/osm_coordinates_dto.dart';
import 'generic_notifier.dart';

class OSMTrainInfrastructureNotifier extends StateNotifier<NotifierState> {
  final OSMApi _osmApi;

  OSMTrainInfrastructureNotifier(this._osmApi) : super(const Initial()) {
    getDetails();
  }

  getDetails() async {
    state = const Loading();

    final response = await _osmApi.getTrainInfastructure();
    var markers = getRawLocationMapDTO(response.payload!);

    state = Loaded(markers);
  }

  LocationMapDTO getRawLocationMapDTO(OSMCoordinatesDTO data) {
    var locationMapDTO = LocationMapDTO(_getRawMarkers(data), []);

    return locationMapDTO;
  }

  List<Marker> _getRawMarkers(OSMCoordinatesDTO data) {
    var markers = <Marker>[];

    for (var i = 0; i < data.coordinates.length; i += 100) {
      var dto = data.coordinates[i];

      var marker = Marker(
          width: 5.0,
          height: 5.0,
          point: LatLng(dto.latitude, dto.longitude),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: Colors.blue,
            ),
            alignment: Alignment.center,
          ));

      markers.add(marker);
    }

    // for (var station in data.stations) {
    //   var marker = Marker(
    //       width: 30.0,
    //       height: 30.0,
    //       point: LatLng(station.latitude, station.longitude),
    //       builder: (ctx) => Container(
    //             decoration: BoxDecoration(
    //               borderRadius: BorderRadius.circular(40),
    //               color: Colors.white,
    //             ),
    //             alignment: Alignment.center,
    //             child: FaIconMapper.getFaIcon(null),
    //           ));

    //   markers.add(marker);
    // }

    return markers;
  }
}
