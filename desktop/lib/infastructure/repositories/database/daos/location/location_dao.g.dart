// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location_dao.dart';

// ignore_for_file: type=lint
mixin _$LocationsDaoMixin on DatabaseAccessor<Database> {
  $LocationsTable get locations => attachedDatabase.locations;
}
