// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tracked_movement_dao.dart';

// ignore_for_file: type=lint
mixin _$TrackedMovementsDaoMixin on DatabaseAccessor<Database> {
  $TrackedMovementsTable get trackedMovements =>
      attachedDatabase.trackedMovements;
}
