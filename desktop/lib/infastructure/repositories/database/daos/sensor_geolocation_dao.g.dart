// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensor_geolocation_dao.dart';

// ignore_for_file: type=lint
mixin _$SensorGeolocationDaoMixin on DatabaseAccessor<Database> {
  $SensorGeolocationsTable get sensorGeolocations =>
      attachedDatabase.sensorGeolocations;
}
