// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tracked_day_dao.dart';

// ignore_for_file: type=lint
mixin _$TrackedDaysDaoMixin on DatabaseAccessor<Database> {
  $TrackedDaysTable get trackedDays => attachedDatabase.trackedDays;
  $TrackedLocationsTable get trackedLocations =>
      attachedDatabase.trackedLocations;
}
