// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tracked_location_dao.dart';

// ignore_for_file: type=lint
mixin _$TrackedLocationsDaoMixin on DatabaseAccessor<Database> {
  $TrackedLocationsTable get trackedLocations =>
      attachedDatabase.trackedLocations;
  $TrackedDaysTable get trackedDays => attachedDatabase.trackedDays;
}
