// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'classified_period_dto_dao.dart';

// ignore_for_file: type=lint
mixin _$ClassifiedPeriodDtoDaoMixin on DatabaseAccessor<Database> {
  $TrackedDaysTable get trackedDays => attachedDatabase.trackedDays;
  $ClassifiedPeriodsTable get classifiedPeriods =>
      attachedDatabase.classifiedPeriods;
  $VehiclesTable get vehicles => attachedDatabase.vehicles;
  $MovementsTable get movements => attachedDatabase.movements;
  $ReasonsTable get reasons => attachedDatabase.reasons;
  $GoogleMapsDatasTable get googleMapsDatas => attachedDatabase.googleMapsDatas;
  $StopsTable get stops => attachedDatabase.stops;
  $ManualGeolocationsTable get manualGeolocations =>
      attachedDatabase.manualGeolocations;
  $SensorGeolocationsTable get sensorGeolocations =>
      attachedDatabase.sensorGeolocations;
}
