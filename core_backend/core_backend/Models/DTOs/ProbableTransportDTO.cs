﻿namespace core_backend.Models.DTOs
{
    public class ProbableTransportDTO
    {
        public long Id { get; set; }
        public double Probalitity { get; set; }
        public string Transport { get; set; }
    }
}
