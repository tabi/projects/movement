﻿using core_backend.Models.DTOs;

namespace core_backend.Models
{
    public class Cluster
    {
        public long Id { get; set; }
        public List<SensorGeolocation> Locations { get; set; }
        public List<string> PotentialTransport { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double AverageSpeed { get; set; }
        public long AmountOfTime { get; set; }
        public double MaxSpeed { get; set; }
        public double AverageAccuracy { get; set; }
        public string UserId { get; set; }
        public List<ClassifiedVehicleClusterDTO> classifiedVehicles { get; set; }

    }
}
