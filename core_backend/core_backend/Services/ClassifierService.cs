﻿using core_backend.Data;
using core_backend.Database;
using core_backend.Models;
using core_backend.Models.DTOs;
using Microsoft.EntityFrameworkCore;

namespace core_backend.Services
{
    public class ClassifierService
    {
        private readonly PostgressDatabase _database;

        public ClassifierService(PostgressDatabase database)
        {
            _database = database;
        }

        public List<SensorGeolocation> PreProcessDay(List<SensorGeolocation> data)
        {
            for (var i = 1; i < data.Count; i ++)
            {
                var distance = CalculateDistance(data[i - 1].Latitude, data[i - 1].Longitude, data[i].Latitude, data[i].Longitude);
                var calculatedSpeed = CalculateSpeed(data[i - 1].CreatedOn, data[i].CreatedOn, distance);
                if (calculatedSpeed == int.MaxValue || calculatedSpeed == int.MinValue)
                {
                    calculatedSpeed = 5;
                }
                data[i].CalculatedSpeed = calculatedSpeed;
            }

            return data;
        }

        public async Task<List<Cluster>> CreateCluster(List<SensorGeolocation> data)
        {
            var clusters = new List<Cluster>();
            var moving = false;
            var counter = 0;

            for (var i = 0; i < data.Count - 5; i += 5)
            {
                var medianSpeed = GetMedian(data.GetRange(i, i + 5));
                for (var j = 0; j < 5; j++)
                {
                    var isMoving = IsMoving(medianSpeed);
                    var stoppedMoving = StoppedMoving(isMoving, moving);

                    if(stoppedMoving)
                    {
                        var cluster = await DistillCluster(data.GetRange(counter, i + j));
                        clusters.Add(cluster);
                    }

                    if(StartedMoving(isMoving, moving))
                    {
                        counter = i + j;
                    }

                    moving = isMoving;
                }
            }

            return clusters;
        }

        public async Task<Cluster> DistillCluster(List<SensorGeolocation> data)
        {
            if(data.Count == 0)
            {
                return new Cluster();
            }

            data = data.OrderBy(l => l.CreatedOn).ToList();

            var averageSpeed = data.Select(d => d.CalculatedSpeed).Average();
            var averageAccuracy = data.Select(d => d.Accuracy).Average();
            var maxSpeed = data.Select(d => d.CalculatedSpeed).Max();
            var amountOfTime = (data.Last().CreatedOn - data.First().CreatedOn) / 1000;

            var cluster = new Cluster
            {
                StartTime = UnixMillisecondsToDateTime(data.First().CreatedOn),
                EndTime = UnixMillisecondsToDateTime(data.Last().CreatedOn),
                AmountOfTime = amountOfTime,
                AverageAccuracy = averageAccuracy,
                AverageSpeed = averageSpeed,
                MaxSpeed = maxSpeed,
                Locations = data,
                UserId = data.First().UserId
            };

            if(cluster.AverageSpeed > 9 || cluster.AverageAccuracy > 400)
            {
                var classifiedVehicle = await ClassifyVehiclesByCluster(cluster);
                cluster.classifiedVehicles = classifiedVehicle;
            }

            return cluster;
        }

        public async Task<List<ClassifiedVehicleClusterDTO>> ClassifyVehiclesByCluster(Cluster cluster)
        {
            var bus = await ClassifyBus(cluster);
            var train = await ClassifyTrain(cluster);
            var tram = await ClassifyTram(cluster);
            var metro = await ClassifyMetro(cluster);

            var classifiedVehicles = new List<ClassifiedVehicleClusterDTO> { bus, train, tram, metro };

            return classifiedVehicles;
        }

        public DateTime UnixMillisecondsToDateTime(long milliseconds)
        {
            DateTime unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return unixEpoch.AddMilliseconds(milliseconds);
        }

        public int GetMedian(List<SensorGeolocation> locations)
        {
            var i = (int)Math.Ceiling((double)(locations.Count() - 1) / 2);
            if (i >= 0)
            {
                locations.OrderBy(l => l.CalculatedSpeed);
                return Convert.ToInt32(locations[i].CalculatedSpeed);
            }

            return 0;
        }

        public bool StoppedMoving(bool isMoving, bool moving)
        {
            return !isMoving && moving;
        }

        public bool StartedMoving(bool isMoving, bool moving)
        {
            return isMoving && !moving;
        }

        public bool IsMoving(int calculatedSpeed)
        {
            return calculatedSpeed > 3;
        }

        double CalculateDistance(double lat1, double lon1, double lat2, double lon2)
        {
            const double p = 0.017453292519943295;
            var c = Math.Cos;
            var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;

            return 12742 * Math.Asin(Math.Sqrt(a)) * 1000;
        }

        double CalculateSpeed(long startTime, long endTime, double distance)
        {
            var timeDifferenceInSeconds = (endTime - startTime) / 1000.0;
            var speed = distance / timeDifferenceInSeconds;

            return double.IsNaN(speed) ? 0 : speed * 3.6;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyTrain(VehicleClusterDTO vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var trainStops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                var coordinates = await _database.TrainRailCoordinates.FromSqlRaw($"select * from TrainRailCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.1").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                if (stations.Any())
                {
                    trainStops.Add(new ExtraLocationsDataDTO
                    {
                        Latitude = cluster.Latitude,
                        Longitude = cluster.Longitude,
                        Station = stations.First().Name,
                        TrainRailCloseBy = coordinates.Count > 0,
                        CreatedOn = cluster.CreatedOn
                    });
                }

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }
            }

            var trainMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Train",
                        Probalitity = trainMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyMetro(VehicleClusterDTO vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var subwayStops = new List<ExtraLocationsDataDTO>();


            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.SubwayRailCoordinates.FromSqlRaw($"select * from SubwayRailCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }

            }

            var subwayMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;
            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Subway",
                        Probalitity = subwayMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyTram(VehicleClusterDTO vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var tramStops = new List<ExtraLocationsDataDTO>();


            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.TramRailCoordinates.FromSqlRaw($"select * from TramRailCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }
            }

            var tramMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Tram",
                        Probalitity = tramMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyBus(VehicleClusterDTO vehicleClusterDTO)
        {
            double coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var busStops = new List<ExtraLocationsDataDTO>();


            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.BusLineCoordinates.FromSqlRaw($"select * from BusLineCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }
            }

            var busMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Bus",
                        Probalitity = busMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyCar(VehicleClusterDTO vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var carStops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i += 10)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.CarRoadCoordinates.FromSqlRaw($"select * from CarRoadCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                coordinateCounter += coordinates.Count;
            }

            var match = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Car",
                        Probalitity = match
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyBicycle(VehicleClusterDTO vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var stops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i += 10)
            {
                var cluster = vehicleClusterDTO.Locations[i];
                var coordinates = await _database.BicycleRoadsCoordinates.FromSqlRaw($"select * from BicycleRoadsCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                coordinateCounter += coordinates.Count;
            }

            var match = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Bicycle",
                        Probalitity = match
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyWalking(VehicleClusterDTO vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var stops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i += 10)
            {
                var cluster = vehicleClusterDTO.Locations[i];
                var coordinates = await _database.WalkingPathCoordinates.FromSqlRaw($"select * from WalkingPathCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                coordinateCounter += coordinates.Count;
            }

            var match = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Walking",
                        Probalitity = match
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }


        public async Task<ClassifiedVehicleClusterDTO> ClassifyTrain(Cluster vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var trainStops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                var coordinates = await _database.TrainRailCoordinates.FromSqlRaw($"select * from TrainRailCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.1").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                if (stations.Any())
                {
                    trainStops.Add(new ExtraLocationsDataDTO
                    {
                        Latitude = cluster.Latitude,
                        Longitude = cluster.Longitude,
                        Station = stations.First().Name,
                        TrainRailCloseBy = coordinates.Count > 0,
                        CreatedOn = cluster.CreatedOn
                    });
                }

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }
            }

            var trainMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Train",
                        Probalitity = trainMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyMetro(Cluster vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var subwayStops = new List<ExtraLocationsDataDTO>();


            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.SubwayRailCoordinates.FromSqlRaw($"select * from SubwayRailCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }

            }

            var subwayMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;
            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Subway",
                        Probalitity = subwayMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyTram(Cluster vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var tramStops = new List<ExtraLocationsDataDTO>();


            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.TramRailCoordinates.FromSqlRaw($"select * from TramRailCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }
            }

            var tramMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Tram",
                        Probalitity = tramMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyBus(Cluster vehicleClusterDTO)
        {
            double coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var busStops = new List<ExtraLocationsDataDTO>();


            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i++)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.BusLineCoordinates.FromSqlRaw($"select * from BusLineCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                if (coordinates.Count > 0)
                {
                    coordinateCounter++;
                }
            }

            var busMatch = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Bus",
                        Probalitity = busMatch
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyCar(Cluster vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var carStops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i += 10)
            {
                var cluster = vehicleClusterDTO.Locations[i];

                if (cluster.Accuracy > 500)
                {
                    continue;
                }

                var coordinates = await _database.CarRoadCoordinates.FromSqlRaw($"select * from CarRoadCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                coordinateCounter += coordinates.Count;
            }

            var match = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Car",
                        Probalitity = match
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyBicycle(Cluster vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var stops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i += 10)
            {
                var cluster = vehicleClusterDTO.Locations[i];
                var coordinates = await _database.BicycleRoadsCoordinates.FromSqlRaw($"select * from BicycleRoadsCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                coordinateCounter += coordinates.Count;
            }

            var match = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Bicycle",
                        Probalitity = match
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }

        public async Task<ClassifiedVehicleClusterDTO> ClassifyWalking(Cluster vehicleClusterDTO)
        {
            var coordinateCounter = 0;
            var extraLocationsDataList = new List<ExtraLocationsDataDTO>();
            var stops = new List<ExtraLocationsDataDTO>();

            for (var i = 0; i < vehicleClusterDTO.Locations.Count; i += 10)
            {
                var cluster = vehicleClusterDTO.Locations[i];
                var coordinates = await _database.WalkingPathCoordinates.FromSqlRaw($"select * from WalkingPathCoordinates where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < {cluster.Accuracy / 100}").ToListAsync();
                //var stations = await _database.TrainStops.FromSqlRaw($"select * from TrainStops where dbo.udf_Haversine(latitude, longitude, {cluster.Latitude}, {cluster.Longitude}) < 0.01").ToListAsync();

                extraLocationsDataList.Add(new ExtraLocationsDataDTO
                {
                    Latitude = cluster.Latitude,
                    Longitude = cluster.Longitude,
                    Station = "",
                    TrainRailCloseBy = coordinates.Count > 0
                });

                //if (stations.Any())
                //{
                //    trainStops.Add(new ExtraLocationsDataDTO
                //    {
                //        Latitude = cluster.Latitude,
                //        Longitude = cluster.Longitude,
                //        Station = stations.First().Name,
                //        TrainRailCloseBy = coordinates.Count > 0,
                //        CreatedOn = cluster.CreatedOn
                //    });
                //}

                coordinateCounter += coordinates.Count;
            }

            var match = (((double)coordinateCounter / (double)vehicleClusterDTO.Locations.Count)) * 100;

            var classifiedVehicle = new ClassifiedVehicleClusterDTO
            {
                ProbableTransports = new List<ProbableTransportDTO> {
                    new ProbableTransportDTO
                    {
                        Transport = "Walking",
                        Probalitity = match
                    }
                },
                ExtraLocationsDataDTOs = extraLocationsDataList,
                //TrainStopsDTOs = trainStops.DistinctBy(s => s.Station).ToList()
            };

            return classifiedVehicle;
        }
    }
}
