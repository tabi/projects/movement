﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace core_backend.Migrations
{
    public partial class clusters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "CalculatedSpeed",
                table: "SensorGeolocations",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "ClusterId",
                table: "SensorGeolocations",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Clusters",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PotentialTransport = table.Column<List<string>>(type: "text[]", nullable: false),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    AverageSpeed = table.Column<double>(type: "double precision", nullable: false),
                    AmountOfTime = table.Column<long>(type: "bigint", nullable: false),
                    MaxSpeed = table.Column<double>(type: "double precision", nullable: false),
                    AverageAccuracy = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clusters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClassifiedVehicleClusterDTO",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClusterId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassifiedVehicleClusterDTO", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassifiedVehicleClusterDTO_Clusters_ClusterId",
                        column: x => x.ClusterId,
                        principalTable: "Clusters",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ExtraLocations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Latitude = table.Column<double>(type: "double precision", nullable: false),
                    Longitude = table.Column<double>(type: "double precision", nullable: false),
                    Station = table.Column<string>(type: "text", nullable: false),
                    TrainRailCloseBy = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedOn = table.Column<long>(type: "bigint", nullable: false),
                    ClassifiedVehicleClusterDTOId = table.Column<long>(type: "bigint", nullable: true),
                    ClassifiedVehicleClusterDTOId1 = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtraLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExtraLocations_ClassifiedVehicleClusterDTO_ClassifiedVehic~1",
                        column: x => x.ClassifiedVehicleClusterDTOId1,
                        principalTable: "ClassifiedVehicleClusterDTO",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ExtraLocations_ClassifiedVehicleClusterDTO_ClassifiedVehicl~",
                        column: x => x.ClassifiedVehicleClusterDTOId,
                        principalTable: "ClassifiedVehicleClusterDTO",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProbableTransports",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Probalitity = table.Column<double>(type: "double precision", nullable: false),
                    Transport = table.Column<string>(type: "text", nullable: false),
                    ClassifiedVehicleClusterDTOId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProbableTransports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProbableTransports_ClassifiedVehicleClusterDTO_ClassifiedVe~",
                        column: x => x.ClassifiedVehicleClusterDTOId,
                        principalTable: "ClassifiedVehicleClusterDTO",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_SensorGeolocations_ClusterId",
                table: "SensorGeolocations",
                column: "ClusterId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassifiedVehicleClusterDTO_ClusterId",
                table: "ClassifiedVehicleClusterDTO",
                column: "ClusterId");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraLocations_ClassifiedVehicleClusterDTOId",
                table: "ExtraLocations",
                column: "ClassifiedVehicleClusterDTOId");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraLocations_ClassifiedVehicleClusterDTOId1",
                table: "ExtraLocations",
                column: "ClassifiedVehicleClusterDTOId1");

            migrationBuilder.CreateIndex(
                name: "IX_ProbableTransports_ClassifiedVehicleClusterDTOId",
                table: "ProbableTransports",
                column: "ClassifiedVehicleClusterDTOId");

            migrationBuilder.AddForeignKey(
                name: "FK_SensorGeolocations_Clusters_ClusterId",
                table: "SensorGeolocations",
                column: "ClusterId",
                principalTable: "Clusters",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SensorGeolocations_Clusters_ClusterId",
                table: "SensorGeolocations");

            migrationBuilder.DropTable(
                name: "ExtraLocations");

            migrationBuilder.DropTable(
                name: "ProbableTransports");

            migrationBuilder.DropTable(
                name: "ClassifiedVehicleClusterDTO");

            migrationBuilder.DropTable(
                name: "Clusters");

            migrationBuilder.DropIndex(
                name: "IX_SensorGeolocations_ClusterId",
                table: "SensorGeolocations");

            migrationBuilder.DropColumn(
                name: "CalculatedSpeed",
                table: "SensorGeolocations");

            migrationBuilder.DropColumn(
                name: "ClusterId",
                table: "SensorGeolocations");
        }
    }
}
