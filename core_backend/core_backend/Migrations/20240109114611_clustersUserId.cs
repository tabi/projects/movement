﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace core_backend.Migrations
{
    public partial class clustersUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Clusters",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Clusters");
        }
    }
}
