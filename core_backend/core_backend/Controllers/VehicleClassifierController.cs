﻿using core_backend.Data;
using core_backend.Models.DTOs;
using core_backend.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace core_backend.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class VehicleClassifierController : ControllerBase
    {
        private readonly ApplicationDbContext _database;
        private readonly ClassifierService _classifierService;
        public VehicleClassifierController(ApplicationDbContext database, ClassifierService classifierService)
        {
            _classifierService = classifierService;
            _database = database;
        }

        [HttpPost]
        public async Task<IActionResult> ClassifyWalkingCluster([FromBody] VehicleClusterDTO vehicleClusterDTO)
        {
            var classifiedCluster = await _classifierService.ClassifyWalking(vehicleClusterDTO);

            return Ok(classifiedCluster);
        }

        [HttpPost]
        public async Task<IActionResult> ClassifyBicycleCluster([FromBody] VehicleClusterDTO vehicleClusterDTO)
        {
            var classifiedCluster = await _classifierService.ClassifyBicycle(vehicleClusterDTO);

            return Ok(classifiedCluster);
        }

        [HttpPost]
        public async Task<IActionResult> ClassifyTramCluster([FromBody] VehicleClusterDTO vehicleClusterDTO)
        {
            var classifiedCluster = await _classifierService.ClassifyTram(vehicleClusterDTO);

            return Ok(classifiedCluster);
        }

        [HttpPost]
        public async Task<IActionResult> ClassifyMetroCluster([FromBody] VehicleClusterDTO vehicleClusterDTO)
        {
            var classifiedCluster = await _classifierService.ClassifyMetro(vehicleClusterDTO);

            return Ok(classifiedCluster);
        }

        [HttpPost]
        public async Task<IActionResult> ClassifyTrainCluster([FromBody] VehicleClusterDTO vehicleClusterDTO)
        {
            var classifiedCluster = await _classifierService.ClassifyTrain(vehicleClusterDTO);

            return Ok(classifiedCluster);
        }

        [HttpPost]
        public async Task<IActionResult> ClassifyBusCluster([FromBody] VehicleClusterDTO vehicleClusterDTO)
        {
            var classifiedCluster = await _classifierService.ClassifyBus(vehicleClusterDTO);

            return Ok(classifiedCluster);
        }

        [HttpPost]
        public async Task<IActionResult> ClassifyCarCluster([FromBody] VehicleClusterDTO vehicleClusterDTO)
        {
            var classifiedCluster = await _classifierService.ClassifyCar(vehicleClusterDTO);

            return Ok(classifiedCluster);
        }

        
    }
}
