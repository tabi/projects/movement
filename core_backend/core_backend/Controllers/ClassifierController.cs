﻿using core_backend.Database;
using core_backend.Models;
using core_backend.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace core_backend.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ClassifierController : ControllerBase
    {
        private readonly ClassifierService _classifierService;
        private readonly PostgressDatabase _database;

        public ClassifierController(PostgressDatabase database, ClassifierService classifierService)
        {
            _database = database;
            _classifierService = classifierService;
        }

        [HttpGet]
       public async Task<IActionResult> ClassifyOldData()
        {
            var users = _database.CSVSensorGeolocations.Select(sl => sl.UserId).Distinct().ToList();

            foreach(var user in users)
            {
                var sensorGeolocations = await _database.CSVSensorGeolocations.Where(sl => sl.UserId == user).Select(l => new SensorGeolocation
                {
                    Accuracy = float.Parse(l.Accuracy),
                    UserId = l.UserId,
                    Altitude = float.Parse(l.Altitude),
                    BatteryLevel = long.Parse(l.BatteryLevel),
                    Bearing = float.Parse(l.Bearing),
                    CalculatedSpeed = 0,
                    CreatedOn = l.CreatedOn,
                    DeletedOn = l.DeletedOn,
                    Latitude = (float)l.Latitude,
                    Longitude = (float)l.Longitude,
                 
                }).ToListAsync();

                var locations = _classifierService.PreProcessDay(sensorGeolocations);
                var clusters = await _classifierService.CreateCluster(locations);

                //TODO:: save clusters to the database.
                await _database.Clusters.AddRangeAsync(clusters);
                await _database.SaveChangesAsync();

                //TODO:: convert clusters to movements.
            }

            

            return Ok();
        }
    }
}
